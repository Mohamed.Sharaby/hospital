<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Bill
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bill newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bill newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bill query()
 */
	class Bill extends \Eloquent {}
}

namespace App{
/**
 * App\Department
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department query()
 */
	class Department extends \Eloquent {}
}

namespace App{
/**
 * App\Product
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product query()
 */
	class Product extends \Eloquent {}
}

namespace App{
/**
 * App\Specialization
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Specialization newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Specialization newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Specialization query()
 */
	class Specialization extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \App\Department $department
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Specialization $specialize
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 */
	class User extends \Eloquent {}
}

