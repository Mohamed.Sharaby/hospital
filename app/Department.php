<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name','en_name'];

protected $appends = ['value'];
    public function getValueAttribute()
    {
        if (app()->getLocale() == 'ar'){
            return $this->name;
        }
        return $this->en_name;
    }

    public function users(){
        return $this->hasMany(User::class);
    }


}
