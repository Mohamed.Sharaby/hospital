<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
    protected $fillable = ['order_id','technical_id','status','refuse_reason','description','user_id','date'];

    public function technical(){
        return $this->belongsTo(User::class,'technical_id');
    }

    public function exchange_details(){
        return $this->hasMany(Exchange_details::class);
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
