<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exchange_details extends Model
{
    protected $fillable = ['exchange_id','product_id','qty'];

    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }


}
