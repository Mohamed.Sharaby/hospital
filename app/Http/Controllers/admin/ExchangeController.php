<?php

namespace App\Http\Controllers\admin;

use App\Exchange;
use App\Exchange_details;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Device;
use App\Libraries\firebase;
use App\Notification;


class ExchangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
//        if(auth()->user()->role !='technical'){
            $exchanges = Exchange::all()->reverse();
            return view('admin.exchanges.index',compact('exchanges'));
//        }else{
//            return abort(401);
//        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->role == 'super' || auth()->user()->role == 'technical'){
            $products = Product::all();
            $technicals = User::whereRole('technical')->whereIsActive(1)->get();
            return view('admin.exchanges.create',compact('products','technicals'));
        }
        else{
            return abort(401);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function getFastExchange($order_id){
        if(auth()->user()->role == 'super' || auth()->user()->role == 'technical'){
            $products = Product::all();
            $technicals = User::whereRole('technical')->whereIsActive(1)->get();
            return view('admin.exchanges.fast_create',compact('products','technicals','order_id'));
        }
        else{
            return abort(401);
        }

    }

    public function store(Request $request)
    {
//        return $request->all();
        $data = [
            'order_id'=>$request->order_id,
            'technical_id' =>$request->technical_id,
            'date'   =>$request->date,
            'description'   =>$request->description,
            'products'   =>$request->products,
            'qtys'   =>$request->products,

        ];

        $rules = [
            'order_id'=>'required|string|exists:orders,id',
            'technical_id'=>'required|string',
            'date'=>"required|date|after_or_equal:today",
            'description'=>"required|string",
            'products'=>"required|array|min:1",
            'qtys'=>"required|array|min:1",
        ];

        $messages = [
            'bill_number.required'=>__('system.bill_number_required'),
            'date.required'=>__('system.bill_date_required'),
            'date.date'=>__('system.enter_valid_mail'),
            'products.required'=>__('system.please_choose_products'),
            'products.min'=>__('system.please_choose_products_2'),
        ];

        $valResult = Validator::make($data,$rules,$messages);
        $data = $request->all();
        $data['user_id'] = auth()->id();
        $data['status'] = 'wait';


        if($valResult->passes()){
//            $order = Order::find($request->order_id);
//            if($order->has('exchange')){
//                session()->flash('error','رقم الطلب لديه أمر صرف بالفعل');
//                return redirect()->back();
//            }

            $products = $request->products;
            $qtys = $request->qtys;

            $exchange = Exchange::create($data);

            for($i = 0; $i< count($products); $i++ ){
                $exchange_details = new Exchange_details();
                $exchange_details->exchange_id = $exchange->id;
                $exchange_details->product_id = $products[$i];
                $exchange_details->qty = $qtys[$i];
                $exchange_details->save();
            }


            //-------------- SEND NOTIFICATIONS USING FIREBASE TOKENS...........
            $title = __('system.exchange');
            $body = __('system.ex_created_by') . auth()->user()->name ;
            $icon =  getimg(auth()->user()->image);
            $image = getimg(auth()->user()->image);
            $click_action =route('notifications.index');
            $username = auth()->user()->name;

            //Get devices ....
            $ids = User::where('role','super')->orWhere('role','warehouse_admin')->where('id','!=',auth()->id())->get()->pluck('id');
//
            $devices = Device::whereIn('user_id',$ids)->get()->pluck('device');

//      ann array to collect notification data to store in DB...
            foreach ($ids as $id)
            {
                $row = [
                    'user_id'=>$id,
                    'title' =>$title,
                    'body'=>$body,
                    'icon'=>$icon,
                    'image'=>$image,
                    'click_action'=>$click_action
                ];
                $notification_rows[]= $row;
            }
            Notification::insert($notification_rows);
            $firebase = new firebase();
            $firebase->sendNotify($devices,$title,$body,$icon,$image,$click_action,$username);
//--------------- END NOTIFICATIONS ---------------------------------

            session()->flash('success',__('system.ex_done_succ'));
            return redirect()->back();
        }else{
            $errors = $valResult->messages();
            return redirect()->back()->withInput()
                ->withErrors($errors);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(auth()->user()->role != 'dept_admin' || auth()->user()->role !='coordinator') {
            $exchange = Exchange::find($id);
            if ($exchange) {
                return view('admin.exchanges.details', compact('exchange'));
            } else {
                return abort(404);
            }
        }
        else{
            return abort(401);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function refuseWithReason(Request $request){

       $exchange = Exchange::find($request->id);
       if($exchange){
            $exchange->status = 'refused';
            $exchange->refuse_reason = $request->reason;
            $exchange->save();


//-------------- SEND NOTIFICATIONS USING FIREBASE TOKENS...........
           $title = __('system.refuse_ex');
           $body = __('system.ex_no_refuse') . $exchange->id .__('system.by'). auth()->user()->name .__('system.and_refuse_reason') . $request->reason;
           $icon =  getimg(auth()->user()->image);
           $image = getimg(auth()->user()->image);
           $click_action =route('notifications.index');
           $username = auth()->user()->name;



           //Get devices ....
           $ids = User::where('role','super')->orWhere('role','warehouse_admin')
               ->where('id','!=',auth()->id())->get()->pluck('id');
           $devices = Device::whereIn('user_id',$ids)->get()->pluck('device');

//      ann array to collect notification data to store in DB...
           foreach ($ids as $id)
           {
               $row = [
                   'user_id'=>$id,
                   'title' =>$title,
                   'body'=>$body,
                   'icon'=>$icon,
                   'image'=>$image,
                   'click_action'=>$click_action
               ];
               $notification_rows[]= $row;
           }

           $technical_row = [
               'user_id'=>$exchange->technical_id,
               'title' =>$title,
               'body'=>__('system.order_no_refuse'). $exchange->id .__('system.by').auth()->user()->name .__('system.and_refuse_reason').$request->reason,
               'icon'=>$icon,
               'image'=>$image,
               'click_action'=>$click_action
           ];


           Notification::insert($notification_rows);
           Notification::insert($technical_row);

           $technical_devices = Device::where('user_id',$exchange->technical_id)->get()->pluck('device');

           $firebase = new firebase();
           $firebase->sendNotify($devices,$title,$body,$icon,$image,$click_action,$username);
           $firebase->sendNotify($technical_devices,$title,__('system.order_no_refuse'). $exchange->id .__('system.by').auth()->user()->name .__('system.and_refuse_reason').$request->reason,$icon,$image,$click_action,$username);
//--------------- END NOTIFICATIONS ---------------------------------

            return response()->json([
                'status'=>true,
                'title'=>__('system.success'),
                'message'=>__('system.refused_succ'),
            ]);
       }else{

           return response()->json([
               'status'=>true,
               'title'=>__('system.error'),
               'message'=>__('system.something_went_wrong'),
           ]);
       }
    }


    public function getAjaxProductQty(Request $request){
        $product = Product::find($request->id);
        return response()->json([
            'data'=>$product->qty
        ]);
    }

    public function accept(Request $request){

        $exchange = Exchange::find($request->exchange_id);
        $exchange->status = 'accepted';
        if($exchange->save()){
            $ids = $request->rowsId;
            $qtys = $request->qtys;
            $productsIds = $request->productsIds;

            for($i = 0; $i< count($ids); $i++ ){
                $details = Exchange_details::find($ids[$i]);
                $details->qty = $qtys[$i];
                $details->save();
                $product = Product::find($productsIds[$i]);
                $product->decrement('qty',$qtys[$i]);
            }


//-------------- SEND NOTIFICATIONS USING FIREBASE TOKENS...........
            $title = __('system.ex_accept');
            $body = __('system.ex_no_accept') . $exchange->id .__('system.by'). auth()->user()->name;
            $icon =  getimg(auth()->user()->image);
            $image = getimg(auth()->user()->image);
            $click_action =route('notifications.index');
            $username = auth()->user()->name;


            //Get devices ....
            $ids = User::where('role','super')->orWhere('role','warehouse_admin')
                ->where('id','!=',auth()->id())->get()->pluck('id');
            $devices = Device::whereIn('user_id',$ids)->get()->pluck('device');

//      ann array to collect notification data to store in DB...
            foreach ($ids as $id)
            {
                $row = [
                    'user_id'=>$id,
                    'title' =>$title,
                    'body'=>$body,
                    'icon'=>$icon,
                    'image'=>$image,
                    'click_action'=>$click_action
                ];
                $notification_rows[]= $row;
            }

            $technical_row = [
                'user_id'=>$exchange->technical_id,
                'title' =>$title,
                'body'=> __('system.ex_no_accept'). $exchange->id . __('system.by').auth()->user()->name,
                'icon'=>$icon,
                'image'=>$image,
                'click_action'=>$click_action
            ];


            Notification::insert($notification_rows);
            Notification::insert($technical_row);

            $technical_devices = Device::where('user_id',$exchange->technical_id)->get()->pluck('device');

            $firebase = new firebase();
            $firebase->sendNotify($devices,$title,$body,$icon,$image,$click_action,$username);
            $firebase->sendNotify($technical_devices,$title,__('system.order_no_accept') . $exchange->id . __('system.by').auth()->user()->name,$icon,$image,$click_action,$username);
//--------------- END NOTIFICATIONS ---------------------------------

            session('success',__('system.ex_accept_success'));
            return redirect()->back();
        }

    }



}
