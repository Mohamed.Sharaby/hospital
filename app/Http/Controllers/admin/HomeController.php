<?php

namespace App\Http\Controllers\admin;

use App\Department;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\Specialization;
use App\User;
use Illuminate\Support\Carbon;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [
            'departments' => Department::all()->count(),
            'specials' => Specialization::all()->count(),
            'products' => Product::all()->count(),
            'admins' => User::whereRole('super')->get()->count(),
            'coordinators' => User::whereRole('coordinator')->get()->count(),
            'dept_admins' => User::whereRole('dept_admin')->get()->count(),
            'techs' => User::whereRole('technical')->get()->count(),
            'orders' => Order::all()->count(),
            'new_orders' => Order::whereStatus('new')->get()->count(),
            'refused_orders' => Order::whereStatus('refused')->get()->count(),
            'accepted_orders' => Order::whereStatus('accepted')->get()->count(),
            'completed_orders' => Order::whereStatus('finished')->get()->count(),
        ];

        // orders chart

        $month = now()->month;

        $from =  Carbon::today()->subMonth();
        $to =  Carbon::today();

        $orders = Order::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)
            ->groupBy(['date', 'status'])
            ->selectRaw('count(orders.id) as total, orders.status as status ,Date(created_at) as date')
            ->get()
            ->groupBy('date')
            ->map(static function ($items) {
                $data = ['new' => 0, 'pending' => 0, 'accepted' => 0, 'completed' => 0,
                    'not_completed' => 0, 'finished' => 0, 'refused' => 0];

                foreach ($items as $item) {
                    if (isset($data[$item['status']])) {
                        $data[$item['status']] += (int)$item['total'];
                    }
                }

                return $data;
            })
            ->toArray();
        // orders chart
        return view('admin.home.home', compact('data','orders'));
    }


    public function chart()
    {
//        whereMonth now()->month;
        $from = request('from', Carbon::today()->subMonth()->format('d-m-Y'));
        $to = request('to', Carbon::today()->format('d-m-Y'));

        $orders = Order::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)
            ->groupBy(['date', 'status'])
            ->selectRaw('count(orders.id) as total, orders.status as status ,Date(created_at) as date')
            ->get()
            ->groupBy('date')
            ->map(static function ($items) {
                $data = ['new' => 0, 'pending' => 0, 'accepted' => 0, 'completed' => 0,
                    'not_completed' => 0, 'finished' => 0, 'refused' => 0];

                foreach ($items as $item) {
                    if (isset($data[$item['status']])) {
                        $data[$item['status']] += (int)$item['total'];
                    }
                }

                return $data;
            })
            ->toArray();

        return view('admin.orders.status-chart',compact('from','to','orders'));
    }




}
