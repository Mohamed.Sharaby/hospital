<?php

namespace App\Http\Controllers\admin;

use App\Department;
use App\Device;
use App\notCompleted;
use App\Order;
use App\Refuse;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\firebase;
use App\Notification;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        if(auth()->user()->role == 'technical' || auth()->user()->role == 'dept_admin'){
            return abort(401);
        }
        if($request->has('type') && $request->type == 'all'){
            $orders = Order::where('status','!=','refused')->orderBy('created_at','desc')->get();
            return view('admin.orders.index',compact('orders'));

        }elseif ($request->has('type') && $request->type == 'refused')  {
            $orders = Order::where('status','=','refused')->orderBy('created_at','desc')->get();
            return view('admin.orders.index',compact('orders'));
        }
        elseif ($request->has('type') && $request->type == 'incompleted')  {
            $orders = Order::where('status','=','not_completed')->orderBy('created_at','desc')->get();
            return view('admin.orders.index',compact('orders'));
        }
        else{
            $orders = Order::where('status','!=','refused')->orderBy('created_at','desc')->get();
            return view('admin.orders.index',compact('orders'));
        }

    }

    public function myOrders(){
        $user = auth()->user();
        if($user->role == 'super' || $user->role =='coordinator'){
            return redirect()->route('orders.index');
        }

        if($user->role == 'technical'){
            $orders = Order::where('technical_id',$user->id)->latest()->get();
            return view('admin.orders.my_orders',compact('orders'));
        }

        if($user->role == 'dept_admin'){
            $orders = Order::where('user_id',$user->id)->orderBy('created_at','desc')->get();
            return view('admin.orders.my_orders',compact('orders'));
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->role == 'technical' || auth()->user()->role == 'coordinator'){
            return abort(401);
        }

        $departments = Department::all();
        return view('admin.orders.create',compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'=>'required|string|max:191',
            'dept_id'=>'required|exists:departments,id',
            'image'=>'nullable|image'
        ];
        $this->validate($request,$rules);
        $data = $request->all();
        $data['user_id'] = auth()->id();
        $data['status'] = 'new';
        if($request->has('image') && $request->image !=null){
            $data['image'] = uploader($request,'image');
        }
        Order::create($data);

//-------------- SEND NOTIFICATIONS USING FIREBASE TOKENS
//   notifications data -------------------------------------------------
        $title = __('system.new_order');
        $body = __('system.order_created_by') . auth()->user()->name ;
        $icon =  getimg(auth()->user()->image);
        $image = getimg(auth()->user()->image);
        $click_action =route('notifications.index');
        $username = auth()->user()->name;

        //Get devices  ....
        $ids = User::where('role','super')->orWhere('role','coordinator')->where('id','!=',auth()->id())->get()->pluck('id');
        $devices = Device::whereIn('user_id',$ids)->get()->pluck('device');

//      ann array to collect notification data to store in DB...
            foreach ($ids as $id)
            {
                $row = [
                    'user_id'=>$id,
                    'title' =>$title,
                    'body'=>$body,
                    'icon'=>$icon,
                    'image'=>$image,
                    'click_action'=>$click_action
                ];
                $notification_rows[]= $row;
            }

            Notification::insert($notification_rows);
            $firebase = new firebase();
            $firebase->sendNotify($devices,$title,$body,$icon,$image,$click_action,$username);

//--------------- END NOTIFICATIONS ---------------------------------
            session()->flash('success',__('system.order_sent_succ'));
            return redirect()->route('orders.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function show($id)
    {
        $order = Order::find($id);
        if($order){
            $technicals = User::whereRole('technical')->get();
            return view('admin.orders.details',compact('order','technicals'));
        }
        else{
            return abort(404);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id){}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){}


    public function addTechnical(Request $request){

        $order = Order::find($request->order_id);
        if($order){
            $order->status = 'pending';
            $order->technical_id = $request->tech_id;
            $order->save();

            //-------------- SEND NOTIFICATIONS USING FIREBASE TOKENS
            $title = __('system.new_order');
            $body = __('system.order_assigned_by') . auth()->user()->name .__('system.to_technician') . User::find($request->tech_id)->name . __('system.and_waiting_approval') ;
            $icon =  getimg(auth()->user()->image);
            $image = getimg(auth()->user()->image);
            $click_action =route('notifications.index');
            $username = auth()->user()->name;

            //Get devices ........
             $ids = User::where('role','super')->where('id','!=',auth()->id())->get()->pluck('id');
            $devices = Device::whereIn('user_id',$ids)->get()->pluck('device');

//            ann array to collect notification data to store in DB...
            foreach ($ids as $id)
            {
                $row = [
                    'user_id'=>$id,
                    'title' =>$title,
                    'body'=>$body,
                    'icon'=>$icon,
                    'image'=>$image,
                    'click_action'=>$click_action
                ];
                $notification_rows[]= $row;
            }

            $rowOfTechnical = [
                'user_id'=>$request->tech_id,
                'title' =>$title,
                'body'=>__('system.order_assigned_by'). auth()->user()->name,
                'icon'=>$icon,
                'image'=>$image,
                'click_action'=>$click_action
            ];

            Notification::insert($notification_rows);
            Notification::insert($rowOfTechnical);

            $technical_devices = Device::where('user_id',$request->tech_id)->get()->pluck('device');
            $firebase = new firebase();
            $firebase->sendNotify($devices,$title,$body,$icon,$image,$click_action,$username);
            $firebase->sendNotify($technical_devices,$title,' تم تعيين طلب جديد لديك من المسؤول ' . auth()->user()->name ,$icon,$image,$click_action,$username);

//--------------- END NOTIFICATIONS ---------------------------------
            return response()->json([
                'status'=>true,
                'title'=>__('system.success'),
                'message'=>__('system.order_sent_to_tech_succ')
            ]);
        }
        else{
            return response()->json([
                'status'=>false,
                'title'=>__('system.error'),
                'message'=>__('system.sorry_order_not_found')
            ]);
        }
    }


    public function changeStatus(Request $request){

        $order = Order::find($request->order_id);

        if($order){
            if($request->status == 'accept'){
                $order->status = 'accepted';
                $order->save();

                //-------------- SEND NOTIFICATIONS USING FIREBASE TOKENS
                $title = __('system.tech_accept_order');
                $body = __('system.order_no_accepted') . $order->id . __('system.for_tech') . User::find($order->technical_id)->name .__('system.by') . auth()->user()->name ;
                $icon =  getimg(auth()->user()->image);
                $image = getimg(auth()->user()->image);
                $click_action =route('notifications.index');
                $username = auth()->user()->name;

                //  Get devices
                $ids = User::where('role','super')->orWhere('role','coordinator')->where('id','!=',auth()->id())->get()->pluck('id');
                $devices = Device::whereIn('user_id',$ids)->get()->pluck('device');

//            ann array to collect notification data to store in DB...
                foreach ($ids as $id)
                {
                    $row = [
                        'user_id'=>$id,
                        'title' =>$title,
                        'body'=>$body,
                        'icon'=>$icon,
                        'image'=>$image,
                        'click_action'=>$click_action
                    ];
                    $notification_rows[]= $row;
                }

                $order_user_devices = Device::where('user_id',$order->user_id)->get()->pluck('device');
                $order_user_notify = [
                    'user_id'=>$order->user_id,
                    'title' =>__('system.tech_accept_order') ,
                    'body'=>__('system.order_accepted_by_tech') . User::find($order->technical_id)->name ,
                    'icon'=>$icon,
                    'image'=>$image,
                    'click_action'=>$click_action
                ];

                Notification::insert($notification_rows);
                Notification::insert($order_user_notify);

                $firebase = new firebase();
                $firebase->sendNotify($devices,$title,$body,$icon,$image,$click_action,$username);
                $firebase->sendNotify($order_user_devices,$title,__('system.order_accepted_by_tech'). User::find($order->technical_id)->name ,$icon,$image,$click_action,$username);
//--------------- END NOTIFICATIONS ---------------------------------

                return response()->json([
                    'status'=>true,
                    'title'=>__('system.success'),
                    'message'=>__('system.order_accpeted_succ')
                ]);
            }


            if($request->status == 'refuse'){
               $order->status = 'refused';
               $order->refuse_reason = $request->refuse_reason;
               $order->save();

               // refuse log ..
               $refuse = new Refuse();
               $refuse->user_id = auth()->id();
               $refuse->order_id = $order->id;
               $refuse->refuse_reason = $request->refuse_reason;
               $refuse->save();



                //-------------- SEND NOTIFICATIONS USING FIREBASE TOKENS
                $title = __('system.order_refuse');
                $body = __('system.order_no_refused') . $order->id . __('system.belongs_to_tech') . User::find($order->technical_id)->name .__('system.by') . auth()->user()->name . __('system.refuse_reason') .$request->refuse_reason;
                $icon =  getimg(auth()->user()->image);
                $image = getimg(auth()->user()->image);
                $click_action =route('notifications.index');
                $username = auth()->user()->name;

                //       Get devices
                $ids = User::where('role','super')->orWhere('role','coordinator')->get()->pluck('id');
                $devices = Device::whereIn('user_id',$ids)->get()->pluck('device');

//            ann array to collect notification data to store in DB...
                foreach ($ids as $id)
                {
                    $row = [
                        'user_id'=>$id,
                        'title' =>$title,
                        'body'=>$body,
                        'icon'=>$icon,
                        'image'=>$image,
                        'click_action'=>$click_action
                    ];
                    $notification_rows[]= $row;
                }

                $order_user_devices = Device::where('user_id',$order->user_id)->get()->pluck('device');
                $order_user_notify = [
                    'user_id'=>$order->user_id,
                    'title' =>__('system.refuse_order_by_tech'),
                    'body'=>__('system.your_order_no_refused') . $order->id .__('system.by_tech') . User::find($order->technical_id)->name .__('system.and_refuse_reason') .$request->refuse_reason,
                    'icon'=>$icon,
                    'image'=>$image,
                    'click_action'=>$click_action
                ];

                Notification::insert($notification_rows);
                Notification::insert($order_user_notify);
                $firebase = new firebase();
                $firebase->sendNotify($devices,$title,$body,$icon,$image,$click_action,$username);
                $firebase->sendNotify($order_user_devices,$title,__('system.refuse_order_by_tech') . User::find($order->technical_id)->name .__('system.and_refuse_reason')  .$request->refuse_reason,$click_action,$username);
//--------------- END NOTIFICATIONS ---------------------------------

                return response()->json([
                    'status'=>true,
                    'title'=>__('system.success'),
                    'message'=>__('system.order_refused_with_reason_success')
                ]);
            }



            if($request->status == 'complete'){
                $order->status = 'completed';
                $order->save();

                //-------------- SEND NOTIFICATIONS USING FIREBASE TOKENS

                $title = __('system.order_finish');
                $body = __('system.work_on_order_no_done') . $order->id . __('system.belongs_to_tech') . User::find($order->technical_id)->name .__('system.by') . auth()->user()->name;
                $icon =  getimg(auth()->user()->image);
                $image = getimg(auth()->user()->image);
                $click_action =route('notifications.index');
                $username = auth()->user()->name;

                //            Get devices
                $ids = User::where('role','super')->orWhere('role','coordinator')->where('id','!=',auth()->id())->get()->pluck('id');
                $devices = Device::whereIn('user_id',$ids)->get()->pluck('device');

//            ann array to collect notification data to store in DB...
                foreach ($ids as $id)
                {
                    $row = [
                        'user_id'=>$id,
                        'title' =>$title,
                        'body'=>$body,
                        'icon'=>$icon,
                        'image'=>$image,
                        'click_action'=>$click_action
                    ];
                    $notification_rows[]= $row;
                }

                $order_user_devices = Device::where('user_id',$order->user_id)->get()->pluck('device');
                $order_user_notify = [
                    'user_id'=>$order->user_id,
                    'title' =>__('system.order_finish'),
                    'body'=>__('system.work_on_order_no_done') . $order->id .__('system.by_tech') . User::find($order->technical_id)->name,
                    'icon'=>$icon,
                    'image'=>$image,
                    'click_action'=>$click_action
                ];

                Notification::insert($notification_rows);
                Notification::insert($order_user_notify);
                $firebase = new firebase();
                $firebase->sendNotify($devices,$title,$body,$icon,$image,$click_action,$username);
                $firebase->sendNotify($order_user_devices,$title, __('system.work_on_order_no_done') . $order->id .__('system.by_tech') . User::find($order->technical_id)->name,$click_action,$username);
//--------------- END NOTIFICATIONS ---------------------------------

                return response()->json([
                    'status'=>true,
                    'title'=>__('system.success'),
                    'message'=>__('system.finish_order_wait_dept_admin')
                ]);
            }


            if($request->status == 'finish'){
                $order->status = 'finished';
                $order->save();

                //-------------- SEND NOTIFICATIONS USING FIREBASE TOKENS
                $title = __('system.order_compilation');
                $body = __('system.order_compilation_msg') . auth()->user()->name . __('system.for_tech') . User::find($order->technical_id)->name ;
                $icon =  getimg(auth()->user()->image);
                $image = getimg(auth()->user()->image);
                $click_action =route('notifications.index');
                $username = auth()->user()->name;

                //            Get devices
                $ids = User::where('role','super')->where('id','!=',auth()->id())->get()->pluck('id');
                $devices = Device::whereIn('user_id',$ids)->get()->pluck('device');

//    end notifications data -------------------------------------------------
//            ann array to collect notification data to store in DB...
                foreach ($ids as $id)
                {
                    $row = [
                        'user_id'=>$id,
                        'title' =>$title,
                        'body'=>$body,
                        'icon'=>$icon,
                        'image'=>$image,
                        'click_action'=>$click_action
                    ];
                    $notification_rows[]= $row;
                }

                $rowOfTechnical = [
                    'user_id'=>$order->technical_id,
                    'title' =>$title,
                    'body'=>__('system.order_comp_1'). $order->id .__('system.by_user') .auth()->user()->name,
                    'icon'=>$icon,
                    'image'=>$image,
                    'click_action'=>$click_action
                ];

                Notification::insert($notification_rows);
                Notification::insert($rowOfTechnical);

                $technical_devices = Device::where('user_id',$order->technical_id)->get()->pluck('device');
                $firebase = new firebase();
                $firebase->sendNotify($devices,$title,$body,$icon,$image,$click_action,$username);
                $firebase->sendNotify($technical_devices,$title,__('system.order_comp_1') . $order->id .__('system.order_user') .auth()->user()->name ,$icon,$image,$click_action,$username);
//--------------- END NOTIFICATIONS ---------------------------------

                return response()->json([
                    'status'=>true,
                    'title'=>__('system.success'),
                    'message'=>__('system.order_comp_2')
                ]);
            }

            if($request->status == 'acceptWithExchange'){
                $order->status = 'accepted';
                $order->save();
                return response()->json([
                    'status'=>true,
                    'title'=>__('system.success'),
                    'message'=>__('system.order_accepted_create_ex')
                ]);
            }

            if($request->status == 'not_completed'){
                $order->status = 'not_completed';
                $order->save();

                // refuse log ..
                $reason = new notCompleted();
                $reason->user_id = auth()->id();
                $reason->order_id = $order->id;
                $reason->in_complete_reason = $request->in_complete_reason;
                if($request->has('image') && $request->image !=null){
                    $reason->image =  uploader($request,'image');
                }
                $reason->save();

                //-------------- SEND NOTIFICATIONS USING FIREBASE TOKENS
                $title = __('system.incomplete');
                $body = __('system.incomplete2') . $order->id .__('system.belongs_to_tech') . User::find($order->technical_id)->name .__('system.by') . auth()->user()->name . __('system.and_reason') .$request->in_complete_reason;
                $icon =  getimg(auth()->user()->image);
                $image = getimg(auth()->user()->image);
                $click_action =route('notifications.index');
                $username = auth()->user()->name;

                //    Get devices
                $ids = User::where('role','super')->orWhere('role','coordinator')->get()->pluck('id');
                $devices = Device::whereIn('user_id',$ids)->get()->pluck('device');

//            ann array to collect notification data to store in DB...
                foreach ($ids as $id)
                {
                    $row = [
                        'user_id'=>$id,
                        'title' =>$title,
                        'body'=>$body,
                        'icon'=>$icon,
                        'image'=>$image,
                        'click_action'=>$click_action
                    ];
                    $notification_rows[]= $row;
                }

                $order_user_devices = Device::where('user_id',$order->user_id)->get()->pluck('device');
                $order_user_notify = [
                    'user_id'=>$order->user_id,
                    'title' =>__('system.order_incomplete'),
                    'body'=>__('system.incomplete2') . $order->id .__('system.by_tech') . User::find($order->technical_id)->name . __('system.and_incomplete_reason') .$request->in_complete_reason,
                    'icon'=>$icon,
                    'image'=>$image,
                    'click_action'=>$click_action
                ];

                Notification::insert($notification_rows);
                Notification::insert($order_user_notify);
                $firebase = new firebase();
                $firebase->sendNotify($devices,$title,$body,$icon,$image,$click_action,$username);
                $firebase->sendNotify($order_user_devices,$title,__('system.yr_order_not_comp_by_tech') . User::find($order->technical_id)->name . __('system.and_reason') .$request->in_complete_reason,$click_action,$username);
//--------------- END NOTIFICATIONS ---------------------------------

                return response()->json([
                    'status'=>true,
                    'title'=>__('system.success'),
                    'message'=>__('system.order_not_comp_success')
                ]);
            }
        }
        else{
            return response()->json([
                'status'=>false,
                'title'=>__('system.error'),
                'message'=>__('system.something_went_wrong')
            ]);
        }
    }


    public function techName(Request  $request)
    {
        //dd($request->all());
        Order::where('id',$request->orderId)->update([
            'tech_name'=> $request->tech_name
        ]);
        session()->flash('success','تم الحفظ بنجاح');
        return back();
    }

}
