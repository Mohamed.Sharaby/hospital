<?php

namespace App\Http\Controllers\admin;

use App\Department;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportsController extends Controller
{
    public function Orders(Request $request)
    {

        $technicals = User::whereRole('technical')->get();
        $departments = Department::all();

        $query = Order::query();

        if($request->has('technical_id') && $request->technical_id != null){
             $query = $query->where('technical_id',$request->technical_id);

        }
        if($request->has('dept_id') && $request->dept_id != null){
            $query = $query->where('dept_id',$request->dept_id);
        }

        if($request->has('status') && $request->status != null){
            if($request->status == 'with_techs'){
                $query = $query->where('status','pending')
                    ->orWhere('status','accepted')
                    ->orWhere('status','completed');
            }
            elseif ($request->status == 'all_not_completed'){
                $query = $query->where('status','!=','finished');
            }
            else{
                $query = $query->where('status',$request->status);
            }
        }

        if($request->has('has_exchange') && $request->has_exchange != null){
            if($request->has_exchange == 'yes'){
                $query = $query->whereHas('exchange');
            } elseif ($request->has_exchange == 'no'){
                $query = $query->doesntHave('exchange');
            }

        }

        if($request->has('from') && $request->from != null){
            $from = date('Y-m-d', strtotime($request->from));
            $query = $query->whereDate('created_at','>=',$from);
        }

        if($request->has('to') && $request->to != null){
            $to = date('Y-m-d', strtotime($request->to));
            $query = $query->whereDate('created_at','<=',$to);
        }

        $orders = $query->orderBy('created_at')->get();


        return view('admin.reports.orders',compact('orders','technicals','departments'));
    }
}
