<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use App\User;

class ResetPassword extends ResetPasswordNotification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
//        return (new MailMessage)
//                    ->line('مشروع صيانة المستشفى')
//                    ->action('Notification Action', url('/dashboard/password/reset/'. $this->token))
//                    ->line('شكراً لك!');


        $data = array(
            'url' => $url = url('/dashboard/password/reset/' . $this->token),
            'title' => __('trans.reset_password'),
            'body' => __('trans.reset_body'),
            'logo' => request()->root() . "/public/assets/admin/images/logo.png"

        );
        return (new MailMessage)
            ->line('إعادة تعيين كلمة المرور')
            ->view('admin.emails.email', ['data' => $data])
                ->action('تغيير كلمة المرور', url('/administrator/password/reset/' . $this->token));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
