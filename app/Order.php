<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id','name','image','dept_id','status','refuse_reason','technical_id'];


    public function department(){
        return $this->belongsTo(Department::class,'dept_id')->withDefault(
            [
                'name'=>"غير معرف",
            ]
        );
    }

    public function technical(){
        return $this->belongsTo(User::class,'technical_id')->withDefault(
            [
                'name'=>"غير معرف",

            ]
        );
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function exchange(){
        return $this->hasOne(Exchange::class);
    }

    public function refuses(){
        return $this->hasMany(Refuse::class,'order_id');
    }
    public function notCompleteds(){
        return $this->hasMany(notCompleted::class,'order_id');
    }
}
