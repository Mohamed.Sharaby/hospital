<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable =['name','qty','price','notes','barcode'];

    public function checkStore($qty){
        if($this->qty < $qty)return false;
        else return true;
    }


}
