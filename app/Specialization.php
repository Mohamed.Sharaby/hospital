<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialization extends Model
{
    protected $fillable = ['name','en_name'];

    public function users(){
        return $this->hasMany(User::class);
    }
}
