<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Supply_details;

class Supply extends Model
{
    protected $fillable = ['bill_number','bill_date','user_id','total'];

    public function supply_details(){
        return $this->hasMany(Supply_details::class,'supply_id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }


    public function getTotal(){
        return $this->supply_details()->sum('total');
    }


}
