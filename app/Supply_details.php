<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supply_details extends Model
{
    protected $fillable = ['supply_id','product_id','qty','price','total'];


    public function supply(){
        return $this->belongsTo(Supply::class,'supply_id');
    }

    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }
}
