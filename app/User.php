<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MyAdminResetPassword as ResetPasswordNotification;
use App\Notifications\ResetPassword;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));

    }
    protected $fillable = [
        'name', 'email', 'password','phone','is_active','suspend_reason','job_number','image','role','specialize_id','dept_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

//    protected $primaryKey = 'id';
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
//    protected $casts = [
//        'email_verified_at' => 'datetime',
//    ];


    public function specialize (){
        return $this->belongsTo(Specialization::class,'specialize_id');
    }

    public function department(){
        return $this->belongsTo(Department::class,'dept_id');
    }
}
