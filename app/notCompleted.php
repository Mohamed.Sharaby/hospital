<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notCompleted extends Model
{
    protected $fillable  = ['order_id','in_complete_reason','image'];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function order(){
        return $this->belongsTo(Order::class,'order_id');
    }
}
