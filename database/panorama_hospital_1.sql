-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 27, 2020 at 04:26 PM
-- Server version: 10.3.25-MariaDB-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `panorama_hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `created_at`, `updated_at`, `en_name`) VALUES
(1, 'الباطنة', '2019-03-18 22:00:00', '2020-10-13 13:28:49', 'Internal Medicine'),
(2, 'الجراحة', '2019-03-18 22:00:00', '2020-10-13 13:28:22', 'Surgery'),
(4, 'الأشعة', '2019-03-18 22:00:00', '2020-10-13 13:31:32', 'Radiology'),
(5, 'العيون', '2019-03-18 22:00:00', '2020-10-13 13:32:13', 'Ophthalmologist'),
(6, 'جلدية', '2019-03-18 22:00:00', '2020-10-13 13:31:01', 'Dermatology'),
(7, 'أورام', '2019-03-18 22:00:00', '2020-10-13 13:30:18', 'Oncology'),
(10, 'أطفال', '2019-03-19 07:08:48', '2020-10-13 13:29:54', 'Pediatric'),
(11, 'العظام', '2019-03-19 07:09:22', '2020-10-13 13:29:29', 'Orthopedic'),
(12, 'عناية الاطفال', '2020-09-24 09:44:42', '2020-10-13 13:32:37', 'Pediatric');

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `device` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('web','ios','android') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `user_id`, `device`, `type`, `created_at`, `updated_at`) VALUES
(8, 1, 'cgxlAbJXJqI:APA91bGE-TzpUuEybNYRgYnOQpIEatnh9rfJ3de22tCuGfJ91eixCmQ21m8ciknkhApcFI6_tFRUtPk5v_3oze_gJ_N_xmdjiiRmUhpsq2nBX-SsPHT2erRhRLBtaDZ0NFnAw7Qv9_NC', 'web', '2019-04-20 05:12:04', '2019-04-20 05:12:04'),
(7, 10, 'cQqgVM3RGGE:APA91bGJgxI8Ksq-XCMTfbZrQfdwkOzjZjrZ5bQ1BLLSZLleSOk69cbU_8MjK22izFZzI0j2aofgcXGBhRc3tPuIFMm4hy_lf6yZG7fBrwmwgmk6n7EOQLTShNepMpwurXci0OH2ATHz', 'web', '2019-04-13 16:14:59', '2019-04-13 16:17:28'),
(6, 12, 'cZ1MQGyb9r4:APA91bHpcwZVZEMWl6vuGNthtDJiFQF0LQg3szzvA-USGgS2A2kyCoekbJF2-QdeZ0p_8k4N1029CLEwxehhn0ofl5vtm2kTUQct1uLsz-AxeYjz1nYjyqJLKcLa8MzepL8QFakxgtGi', 'web', '2019-04-13 16:13:15', '2019-04-13 16:16:22'),
(5, 1, 'd-jQ_Yo3MIA:APA91bH9pYrh4FjWv-0YeZxskY-jLUMWbB6INcaBub1g0GMQHQ8dgnHNgjiHZvVFeQlt0I8nIGhmIVQz0_A77SnaWhux2sbmMjYx3jxGTvvBaiIwPhPlK2loICunFEnw7HUt39mSUgo1', 'web', '2019-04-02 11:28:49', '2019-04-02 12:28:17'),
(9, 11, 'f79f3mDo5-g:APA91bHOS_DoZDheOOU69XjF3xDDh7dCTo6qvMAKRRzHA8c4RM6BYs6FM-JCAQTIz1JpuNS0wwdL_aOIjFW3xyglkQcA1Tc235BmiNHyYU4fabq-z2nULA0doE8qsgBz9YTsc4U7K31l', 'web', '2019-04-23 07:35:29', '2019-06-23 05:38:59'),
(14, 1, 'ery4Dz2Wi_g:APA91bH8hzNk1IQcbwB6ocdO7yhvJyY795an-SEhSpBOHKDHAAK3oNUdogN5cvVx5-EcDDFvecmBWZoY7g594Fay5tIi_8bVmBcxxfZFtsDnlQ43zoZbHqK_rP3QwAQ1tzTGVqOLObpr', 'web', '2019-07-31 10:29:21', '2019-07-31 10:29:21'),
(13, 1, 'csbw4o-j8hk:APA91bEyK97iOG426kXCWcMD3omElqFbE_UVNE033d66_mGol4sEEEWRkkaOc_u-MQOW0IONntOrSEzNzuDXAAEgU5FTBC0bwcD3poOQudgSVpyAqeEJgF6zZs1MnU5CsTjAihY67L-q', 'web', '2019-06-30 11:04:15', '2019-06-30 11:04:15'),
(12, 1, 'dLZ5skmdCtQ:APA91bH_M0yiHRJfX6Z_32ou0owB_rjgfdfZMW4qWAolAZU25g5Q0-Imz4OeFzQEpymD_2r8E1t7ID5QsSx01RYJjdKQ8vr28j_HX8S-QHAoDAtmuzhwoHweqXvn1h8RtOnXhTjA8TVk', 'web', '2019-06-30 10:11:31', '2019-06-30 11:03:24'),
(15, 10, 'dDVK7eX6s54:APA91bFpu1RPncMTQps5yGkJB8B_mxG7bmVNI1Mn90EkSg9m7BR-k_yPV-MkruIvY1hpJF-YAB2z0AW6xRbidV8yeBLjlsFAcC_UFNPOHfwddqxUC1R211EY9p1Rq5ECHHNJCGdIzdAD', 'web', '2019-07-31 12:54:16', '2019-07-31 12:54:16'),
(16, 1, 'czYrJYWoVXs:APA91bE2DRF-S3rI-yFtlrL4vyQo_OC9jyh7N2-E2qH9nNjLeB3nIYleEWRXfanb8tvhyZO0VN8Lqjc_WwIukMcZxM4jipT8oglD4UhVtKbkjq2pxy7UazS-pnrU75gVZ7VRTiHbH9bG', 'web', '2019-09-05 12:47:36', '2019-10-02 12:19:40'),
(17, 1, 'dAvaed3C2oc:APA91bGO51iQ8AHzZuHCfs1mRdgvOnSol2tzhgbw-OkIJBJXzy02Yw5SbaQ7vUWPbGnsGPblPGU8wo7Uc08tggMYmP0SzcYC-gx6Dguk9d3N4TYE6bJUOCo2tr_uryEJ2zE2gWi0Cf9E', 'web', '2019-10-08 08:33:34', '2019-10-08 08:33:34'),
(18, 1, 'csnkl5nlofc:APA91bGESCx0RmJBlkAh-XaTVS8X3iLhVOMGL4ZstsEIBYUbBFVFN_fprTIg-LiZeHVzHJVipBU-yOgKoR1ydyf15KMrqsIFyDbY8eyewRpYXTC7cZyxE9LMsq8UOclziVCypNEWSBCj', 'web', '2020-08-17 10:38:15', '2020-08-17 10:38:15'),
(19, 1, 'ccQ29oKy5iQ:APA91bEm15NPB81lRcRInN3f4-gUGnyI3O-ITMJyJMeRTWY6MwN4xObkbeV5JhksRcx9WNBYZcYS26Sj9QG9LshLpYiCNDPjuWO0gI-O9OvC90dg-p8efxUD7IjIf_3YIH3NsXR57-vW', 'web', '2020-08-18 10:35:53', '2020-08-18 11:01:19'),
(20, 1, 'cmJSS9hTvG8:APA91bHZ0Wox8lry3MNtrJ_3sikQWevmLQKvkwmgRlE49yj5IAqn6VoOPkIkNu2G2LZnrjB4tDoIbytMxcm2Nwbonp70gvXA6SvkraFiAFhpzmKFDzon2F5X9ApJAq3IlOxlJjwk099Y', 'web', '2020-10-04 12:08:52', '2020-10-06 12:26:51'),
(21, 17, 'e8Yi_TT9uVo:APA91bEu36YLKsA2Wtak87xTjl959i1PpKCR4g6QHFTJiA8ZZfS5Xk91sTQVtQbFL8Oe6_654OfrIewEDZXDXosrrwkaWE9-cXgkacVDKRd0TaGZttchpuf8cYShAEW2Bzxg8M63DRha', 'web', '2020-10-06 10:35:11', '2020-10-06 10:35:47'),
(22, 17, 'epTNvkWd1zs:APA91bGyNmtZ7UYnNPa2WFLy5kNuR2o73sdNF9Z4K0w0wkCN_b53hgMpyjKUwWGzh5TJH2NJ58M2j0bho6Q0GqMslQ_Mtu2V4lZZSMrGhvEAgI1ysCEsoAhGAi-BIv32aYQzrCmwUPJL', 'web', '2020-10-06 12:39:31', '2020-10-06 12:39:31'),
(23, 10, 'cqY8ipN-Oe0:APA91bHfHyIicgBEHOd1g1IR0BPu_5wCcCAFPWsohvMxFtPia3BTsW4RVCn2gbyzbMAoRofoL3kGDPgbH1zjCHmWWc72wz54bnWoTkDVfIwVYAE-9b-qiHOxK1X9YPbSawFMa4vzHej2', 'web', '2020-10-13 13:56:11', '2020-10-22 11:23:04'),
(24, 1, 'dBtLoLye0ic:APA91bFfcxqjtxnnZcQSOl1zbhUTY-q4Pkb-U9p5MWegH-EFFbdrw0Yd48-5LjZW-u2CwiuB18xqBaGZB3Lt3JBoyhVZiJ1V1Lddl0gdCwWdINRs0zxBRa7MoSAtB_77dniLrr4O53xk', 'web', '2020-10-14 12:50:23', '2020-10-14 13:20:05'),
(25, 1, 'c1x9hxDlCM8:APA91bGbD6xZDwhwaDOjDAeKIfQqROykR1rT3JqcP3IXimmtY6lfgs2MexVbwsI5QXcv4NToDN5Dda2ate8zGn7UoBvjox-b1ASeepJMpjQUESYtFkoEeB-UIhQx0lm-xDDRgFlhoIea', 'web', '2020-10-18 12:29:25', '2020-10-18 12:29:25');

-- --------------------------------------------------------

--
-- Table structure for table `exchanges`
--

CREATE TABLE `exchanges` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `technical_id` int(10) UNSIGNED NOT NULL,
  `status` enum('wait','accepted','refused') COLLATE utf8mb4_unicode_ci NOT NULL,
  `refuse_reason` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exchanges`
--

INSERT INTO `exchanges` (`id`, `order_id`, `technical_id`, `status`, `refuse_reason`, `created_at`, `updated_at`, `description`, `user_id`, `date`) VALUES
(1, 26, 10, 'accepted', NULL, '2019-07-04 11:37:00', '2019-07-04 11:38:39', 'l,hwthjlklllllllllllllllll', 1, '2019-07-04'),
(2, 27, 10, 'accepted', NULL, '2019-07-31 12:56:02', '2019-07-31 12:57:53', 'l,hwthj', 10, '2019-07-31'),
(3, 29, 10, 'accepted', NULL, '2019-08-21 11:47:11', '2019-08-21 11:48:28', 'اصلاح تكييف', 10, '2019-08-21'),
(4, 30, 10, 'accepted', NULL, '2019-09-04 15:13:56', '2019-09-04 15:14:39', 'l,hwthj uhli', 10, '2019-09-04'),
(5, 32, 10, 'accepted', NULL, '2020-03-08 17:13:25', '2020-03-08 17:14:38', 'ghrsg', 1, '2020-03-08'),
(6, 39, 10, 'accepted', NULL, '2020-10-13 14:06:43', '2020-10-13 14:07:34', 'jdtyhdhfhdfggsdf', 10, '2020-10-13'),
(7, 43, 10, 'accepted', NULL, '2020-10-22 11:21:35', '2020-10-22 11:22:39', 'يوجد مشكله في المكيف ويحتاج الي المنتجات التاليه', 10, '2020-10-22');

-- --------------------------------------------------------

--
-- Table structure for table `exchange_details`
--

CREATE TABLE `exchange_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `exchange_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exchange_details`
--

INSERT INTO `exchange_details` (`id`, `exchange_id`, `product_id`, `qty`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 10, '2019-07-04 11:37:00', '2019-07-04 11:37:00'),
(2, 2, 1, 1, '2019-07-31 12:56:02', '2019-07-31 12:56:02'),
(3, 3, 1, 1, '2019-08-21 11:47:11', '2019-08-21 11:47:11'),
(4, 4, 1, 10, '2019-09-04 15:13:56', '2019-09-04 15:13:56'),
(5, 4, 2, 15, '2019-09-04 15:13:56', '2019-09-04 15:13:56'),
(6, 5, 1, 10, '2020-03-08 17:13:25', '2020-03-08 17:13:25'),
(7, 6, 1, 10, '2020-10-13 14:06:43', '2020-10-13 14:06:43'),
(8, 7, 1, 1, '2020-10-22 11:21:35', '2020-10-22 11:21:35'),
(9, 7, 2, 3, '2020-10-22 11:21:35', '2020-10-22 11:21:35');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_03_18_142431_create_departments_table', 1),
(4, '2019_03_18_142507_create_products_table', 1),
(6, '2019_03_19_091147_create_specializations_table', 2),
(7, '2019_03_19_114000_add_job_number_to_users_table', 3),
(8, '2019_03_20_081818_add_remember_token_to_users_table', 4),
(11, '2019_03_20_135115_create_supplies_table', 5),
(12, '2019_03_20_135550_create_supply_details_table', 5),
(13, '2019_03_20_141234_add_price_column_to_products_table', 6),
(14, '2019_03_26_121657_create_orders_table', 7),
(15, '2019_03_26_123845_create_exchanges_table', 7),
(16, '2019_03_26_124106_create_exchange_details_table', 7),
(17, '2019_03_27_090846_create_notifications_table', 7),
(18, '2019_03_27_100133_add_user_id_column_to_orders_table', 7),
(19, '2019_03_28_132708_add_dectiption_column_to_exchanges_table', 7),
(20, '2019_03_28_140630_create_devices_table', 7),
(21, '2019_03_31_081049_add_user_id_to_exchanges_table', 8),
(22, '2019_03_31_084242_add_date_column_to_exchanges_table', 8),
(23, '2019_05_08_125640_create_refuses_table', 9),
(24, '2019_05_09_105812_create_not_completeds_table', 10),
(25, '2019_05_09_111251_add_user_id_to_not_completeds_table', 10),
(26, '2020_10_12_160645_add_barcode_to_products_table', 11),
(27, '2020_10_13_130031_add_en_name_to_departments_table', 11),
(28, '2020_10_13_142129_add_en_name_to_specializations_table', 11),
(29, '2020_10_22_161002_add_tech_name_to_orders_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `click_action` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `title`, `body`, `icon`, `image`, `click_action`, `is_read`, `created_at`, `updated_at`) VALUES
(62, 1, 'قبول طلب من فني', 'تمت الموافقة على الطلب رقم  15الخاص بالفني فني تقني من قبل مدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(61, 11, 'إنتهاء طلب', 'تم الإنتهاء من  طلبك رقم 16 من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(60, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  16 الخاص بالفني  فني تقني من قبل فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(59, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  16 الخاص بالفني  فني تقني من قبل فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(58, 11, 'قبول طلب من فني', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(57, 12, 'قبول طلب من فني', 'تمت الموافقة على الطلب رقم  16الخاص بالفني فني تقني من قبل فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(56, 1, 'قبول طلب من فني', 'تمت الموافقة على الطلب رقم  16الخاص بالفني فني تقني من قبل فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(55, 10, 'طلب جديد', ' تم تعيين طلب جديد لديك من المسؤول منسق أوامر 1', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-06-18 15:58:25'),
(54, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1 للفني فني تقنيو في إنتظار الموافقة', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(53, 10, 'طلب جديد', ' تم تعيين طلب جديد لديك من المسؤول منسق أوامر 1', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-06-18 15:58:25'),
(52, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1 للفني فني تقنيو في إنتظار الموافقة', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(51, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(50, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(49, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(48, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(47, 10, 'طلب جديد', ' تم تعيين طلب جديد لديك من المسؤول منسق أوامر 1', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-06-18 15:58:25'),
(46, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1 للفني فني تقنيو في إنتظار الموافقة', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(45, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(44, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(43, 11, 'إنتهاء طلب', 'تم الإنتهاء من  طلبك رقم 13 من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(42, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  13 الخاص بالفني  فني تقني من قبل فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(41, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  13 الخاص بالفني  فني تقني من قبل فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-04-23 07:35:28'),
(40, 11, 'قبول طلب من فني', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(39, 12, 'قبول طلب من فني', 'تمت الموافقة على الطلب رقم  13الخاص بالفني فني تقني من قبل فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(38, 1, 'قبول طلب من فني', 'تمت الموافقة على الطلب رقم  13الخاص بالفني فني تقني من قبل فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-04-23 07:35:28'),
(37, 10, 'طلب جديد', ' تم تعيين طلب جديد لديك من المسؤول منسق أوامر 1', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-06-18 15:58:25'),
(36, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1 للفني فني تقنيو في إنتظار الموافقة', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-04-23 07:35:28'),
(35, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-04-13 16:16:26'),
(34, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-04-23 07:35:28'),
(63, 12, 'قبول طلب من فني', 'تمت الموافقة على الطلب رقم  15الخاص بالفني فني تقني من قبل مدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(64, 11, 'قبول طلب من فني', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(65, 1, 'رفض طلب', 'تمت رفض الطلب رقم  14 الخاص بالفني  فني تقني من قبل فني تقني و سبب الرفض عدم', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(66, 12, 'رفض طلب', 'تمت رفض الطلب رقم  14 الخاص بالفني  فني تقني من قبل فني تقني و سبب الرفض عدم', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(67, 11, 'رفض طلب من فني', 'تم رفض طلبك رقم 14 من قبل الفني فني تقني و سبب الرفض عدم', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(68, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(69, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(70, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1 للفني فني تقنيو في إنتظار الموافقة', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(71, 10, 'طلب جديد', ' تم تعيين طلب جديد لديك من المسؤول منسق أوامر 1', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-06-18 15:58:25'),
(72, 1, 'قبول طلب من فني', 'تمت الموافقة على الطلب رقم  17الخاص بالفني فني تقني من قبل فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(73, 12, 'قبول طلب من فني', 'تمت الموافقة على الطلب رقم  17الخاص بالفني فني تقني من قبل فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(74, 11, 'قبول طلب من فني', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(75, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  17 الخاص بالفني  فني تقني من قبل فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(76, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  17 الخاص بالفني  فني تقني من قبل فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(77, 11, 'إنتهاء طلب', 'تم الإنتهاء من  طلبك رقم 17 من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(78, 1, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أول للفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(79, 10, 'إكتمال طلب', ' تم إكتمال الطلب الخاص بك رقم  17 من قبل المسؤول  مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-06-18 15:58:25'),
(80, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(81, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(82, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-05-27 14:44:05'),
(83, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(84, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-06-20 11:02:45'),
(85, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(86, 1, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أول للفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-06-30 11:05:33'),
(87, 10, 'إكتمال طلب', ' تم إكتمال الطلب الخاص بك رقم  13 من قبل المسؤول  مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(88, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-06-30 11:05:33'),
(89, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(90, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(91, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(92, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(93, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(94, 1, 'system.new_order', 'system.order_created_byمسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(95, 12, 'system.new_order', 'system.order_created_byمسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(96, 13, 'system.new_order', 'system.order_created_byمسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(97, 1, 'system.new_order', 'system.order_created_byمسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(98, 12, 'system.new_order', 'system.order_created_byمسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(99, 13, 'system.new_order', 'system.order_created_byمسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(100, 1, 'system.new_order', 'system.order_assigned_byمنسق أوامر 1system.to_technicianفني تقنيsystem.and_waiting_approval', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(101, 13, 'system.new_order', 'system.order_assigned_byمنسق أوامر 1system.to_technicianفني تقنيsystem.and_waiting_approval', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(102, 10, 'system.new_order', 'system.order_assigned_byمنسق أوامر 1', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(103, 1, 'system.tech_accept_order', 'system.order_no_accepted24system.for_techفني تقنيsystem.byفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(104, 12, 'system.tech_accept_order', 'system.order_no_accepted24system.for_techفني تقنيsystem.byفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(105, 13, 'system.tech_accept_order', 'system.order_no_accepted24system.for_techفني تقنيsystem.byفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(106, 11, 'system.tech_accept_order', 'system.order_accepted_by_techفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(107, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  24 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(108, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  24 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(109, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  24 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(110, 11, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  24 من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(111, 1, 'system.order_compilation', 'system.order_compilation_msgمسؤول قسم أولsystem.for_techفني تقني', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(112, 13, 'system.order_compilation', 'system.order_compilation_msgمسؤول قسم أولsystem.for_techفني تقني', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(113, 10, 'system.order_compilation', 'system.order_comp_124system.by_userمسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(114, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(115, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(116, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(117, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(118, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(119, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(120, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  25الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(121, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  25الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:07:19'),
(122, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  25الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(123, 11, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(124, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  25 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(125, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  25 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:24:50'),
(126, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  25 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(127, 11, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  25 من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(128, 1, 'لم يكتمل', 'لم يكتمل الطلب رقم  25 الخاص بالفني  فني تقنيمن قبل المسؤولمسؤول قسم أول و السبب لم يكتمل الطلب بنجاح حيث انه هناك مشكله في الكهرباء', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(129, 12, 'لم يكتمل', 'لم يكتمل الطلب رقم  25 الخاص بالفني  فني تقنيمن قبل المسؤولمسؤول قسم أول و السبب لم يكتمل الطلب بنجاح حيث انه هناك مشكله في الكهرباء', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-04 11:24:50'),
(130, 13, 'لم يكتمل', 'لم يكتمل الطلب رقم  25 الخاص بالفني  فني تقنيمن قبل المسؤولمسؤول قسم أول و السبب لم يكتمل الطلب بنجاح حيث انه هناك مشكله في الكهرباء', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(131, 11, 'الطلب لم يكتمل', 'لم يكتمل الطلب رقم  25 من قبل الفني فني تقني و سبب عدم الإكتمال لم يكتمل الطلب بنجاح حيث انه هناك مشكله في الكهرباء', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(132, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(133, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(134, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(135, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  25الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(136, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  25الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-31 12:53:31'),
(137, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  25الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(138, 11, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(139, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  25 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(140, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  25 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-31 12:53:31'),
(141, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  25 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(142, 11, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  25 من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(143, 1, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(144, 13, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(145, 10, 'إكتمال طلب', ' تم إكتمال الطلب الخاص بك رقم   25 من قبل المسؤول  مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(146, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(147, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-31 12:53:31'),
(148, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(149, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(150, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(151, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(152, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  26الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(153, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  26الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-31 12:53:31'),
(154, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  26الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(155, 11, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(156, 1, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول مدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(157, 13, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول مدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(158, 14, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول مدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(159, 1, 'موافقة على أمر صرف', 'system.ex_no_accept1من قبل المسؤولمسئول المستودع', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(160, 13, 'موافقة على أمر صرف', 'system.ex_no_accept1من قبل المسؤولمسئول المستودع', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(161, 10, 'موافقة على أمر صرف', 'system.ex_no_accpet1من قبل المسؤولمسئول المستودع', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(162, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  26 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(163, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  26 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-31 12:53:31'),
(164, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  26 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(165, 11, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  26 من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(166, 1, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-08 16:30:57'),
(167, 13, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(168, 10, 'إكتمال طلب', ' تم إكتمال الطلب الخاص بك رقم   26 من قبل المسؤول  مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(169, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(170, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-07-31 12:53:31'),
(171, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL);
INSERT INTO `notifications` (`id`, `user_id`, `title`, `body`, `icon`, `image`, `click_action`, `is_read`, `created_at`, `updated_at`) VALUES
(172, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(173, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(174, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(175, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  27الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(176, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  27الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(177, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  27الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(178, 11, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(179, 1, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(180, 13, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(181, 14, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(182, 1, 'موافقة على أمر صرف', 'system.ex_no_accept2من قبل المسؤولمسئول المستودع', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(183, 13, 'موافقة على أمر صرف', 'system.ex_no_accept2من قبل المسؤولمسئول المستودع', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(184, 10, 'موافقة على أمر صرف', 'موافقة على أمر صرف2من قبل المسؤولمسئول المستودع', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(185, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(186, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(187, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(188, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(189, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(190, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(191, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  28الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(192, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  28الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(193, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  28الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(194, 11, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(195, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  28 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(196, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  28 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(197, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  28 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(198, 11, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  28 من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(199, 1, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(200, 13, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(201, 10, 'إكتمال طلب', ' تم إكتمال الطلب الخاص بك رقم   28 من قبل المسؤول  مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(202, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(203, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(204, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(205, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(206, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(207, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(208, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  29الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(209, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  29الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(210, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  29الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(211, 11, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(212, 1, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(213, 13, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(214, 14, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(215, 1, 'موافقة على أمر صرف', 'system.ex_no_accept3من قبل المسؤولمسئول المستودع', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(216, 13, 'موافقة على أمر صرف', 'system.ex_no_accept3من قبل المسؤولمسئول المستودع', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(217, 10, 'موافقة على أمر صرف', 'موافقة على أمر صرف3من قبل المسؤولمسئول المستودع', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(218, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  29 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2019-08-21 12:22:51'),
(219, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  29 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(220, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  29 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(221, 11, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  29 من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(222, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-03-04 15:41:18'),
(223, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(224, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(225, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-03-04 15:41:18'),
(226, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(227, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(228, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  30الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-03-04 15:41:18'),
(229, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  30الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(230, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  30الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(231, 11, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(232, 1, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-03-04 15:41:18'),
(233, 13, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(234, 14, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(235, 1, 'موافقة على أمر صرف', 'system.ex_no_accept4من قبل المسؤولمسئول المستودع', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-03-04 15:41:18'),
(236, 13, 'موافقة على أمر صرف', 'system.ex_no_accept4من قبل المسؤولمسئول المستودع', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(237, 10, 'موافقة على أمر صرف', 'موافقة على أمر صرف4من قبل المسؤولمسئول المستودع', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/storage/', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(238, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  30 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-03-04 15:41:18'),
(239, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  30 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(240, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  30 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(241, 11, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  30 من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(242, 1, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-03-04 15:41:18'),
(243, 13, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(244, 10, 'إكتمال طلب', ' تم إكتمال الطلب الخاص بك رقم   30 من قبل المسؤول  مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(245, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/dashboard/notifications', 1, NULL, '2020-03-04 15:41:18'),
(246, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(247, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/dashboard/notifications', 0, NULL, NULL),
(248, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول مدير نظامللفنيفني تقنيو في إنتظار الموافقة ', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/dashboard/notifications', 0, NULL, NULL),
(249, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول مدير نظام', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(250, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  31الخاص بالفني فني تقنيمن قبل المسؤولمدير نظام', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/dashboard/notifications', 1, NULL, '2020-03-04 15:41:18'),
(251, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  31الخاص بالفني فني تقنيمن قبل المسؤولمدير نظام', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(252, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  31الخاص بالفني فني تقنيمن قبل المسؤولمدير نظام', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/dashboard/notifications', 0, NULL, NULL),
(253, 1, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://127.0.0.1:8000/dashboard/notifications', 1, NULL, '2020-03-04 15:41:18'),
(254, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  31 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://127.0.0.1:8000/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://127.0.0.1:8000/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://127.0.0.1:8000/dashboard/notifications', 1, NULL, '2020-03-04 15:41:18'),
(255, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  31 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://127.0.0.1:8000/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://127.0.0.1:8000/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://127.0.0.1:8000/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(256, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  31 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://127.0.0.1:8000/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://127.0.0.1:8000/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://127.0.0.1:8000/dashboard/notifications', 0, NULL, NULL),
(257, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  31 من قبل الفني فني تقني', 'http://127.0.0.1:8000/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://127.0.0.1:8000/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://127.0.0.1:8000/dashboard/notifications', 1, NULL, '2020-03-04 15:41:18'),
(258, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(259, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(260, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(261, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول مدير نظامللفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(262, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول مدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(263, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  32الخاص بالفني فني تقنيمن قبل المسؤولمدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(264, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  32الخاص بالفني فني تقنيمن قبل المسؤولمدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(265, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  32الخاص بالفني فني تقنيمن قبل المسؤولمدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(266, 1, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(267, 1, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول مدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(268, 13, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول مدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(269, 14, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول مدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(270, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  32 الخاص بالفني  فني تقنيمن قبل المسؤولمدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(271, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  32 الخاص بالفني  فني تقنيمن قبل المسؤولمدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(272, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  32 الخاص بالفني  فني تقنيمن قبل المسؤولمدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(273, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  32 من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(274, 13, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مدير نظامالخاص بالفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(275, 10, 'إكتمال طلب', ' تم إكتمال الطلب الخاص بك رقم   32 من قبل المسؤول  مدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(276, 1, 'موافقة على أمر صرف', 'system.ex_no_accept5من قبل المسؤولمدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(277, 13, 'موافقة على أمر صرف', 'system.ex_no_accept5من قبل المسؤولمدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(278, 14, 'موافقة على أمر صرف', 'system.ex_no_accept5من قبل المسؤولمدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(279, 10, 'موافقة على أمر صرف', 'موافقة على أمر صرف5من قبل المسؤولمدير نظام', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(280, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(281, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(282, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(283, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(284, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(285, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(286, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  33الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(287, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  33الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(288, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  33الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(289, 11, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(290, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  33 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(291, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  33 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(292, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  33 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(293, 11, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  33 من قبل الفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(294, 1, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(295, 13, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(296, 10, 'إكتمال طلب', ' تم إكتمال الطلب الخاص بك رقم   33 من قبل المسؤول  مسؤول قسم أول', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'http://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(297, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(298, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:48:03'),
(299, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(300, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(301, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(302, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-08-18 10:49:59'),
(303, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  34الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(304, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  34الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 10:59:35'),
(305, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  34الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(306, 11, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(307, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  34 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(308, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  34 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 10:59:35'),
(309, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  34 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(310, 11, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  34 من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(311, 13, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مدير نظامالخاص بالفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(312, 10, 'إكتمال طلب', ' تم إكتمال الطلب الخاص بك رقم   34 من قبل المسؤول  مدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(313, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41');
INSERT INTO `notifications` (`id`, `user_id`, `title`, `body`, `icon`, `image`, `click_action`, `is_read`, `created_at`, `updated_at`) VALUES
(314, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 10:59:35'),
(315, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(316, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(317, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 10:59:35'),
(318, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(319, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(320, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 10:59:35'),
(321, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(322, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(323, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(324, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(325, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  37الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(326, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  37الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 10:59:35'),
(327, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  37الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(328, 11, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(329, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  37 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(330, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  37 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 10:59:35'),
(331, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  37 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(332, 11, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  37 من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(333, 1, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(334, 13, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(335, 10, 'إكتمال طلب', ' تم إكتمال الطلب الخاص بك رقم   37 من قبل المسؤول  مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(336, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(337, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 10:59:35'),
(338, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(339, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(340, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 10:59:35'),
(341, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(342, 16, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(343, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(344, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(345, 16, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(346, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(347, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  39الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(348, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  39الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 10:59:35'),
(349, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  39الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(350, 16, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  39الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(351, 11, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(352, 1, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(353, 13, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(354, 14, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(355, 15, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(356, 16, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(357, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  39 الخاص بالفني  فني تقنيمن قبل المسؤولمدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(358, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  39 الخاص بالفني  فني تقنيمن قبل المسؤولمدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 10:59:35'),
(359, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  39 الخاص بالفني  فني تقنيمن قبل المسؤولمدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(360, 16, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  39 الخاص بالفني  فني تقنيمن قبل المسؤولمدير نظام', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(361, 11, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  39 من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/storage/photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(362, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(363, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 10:59:35'),
(364, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(365, 16, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(366, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(367, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(368, 16, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(369, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(370, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  40الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 1, NULL, '2020-10-22 11:02:41'),
(371, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  40الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(372, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  40الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(373, 16, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  40الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(374, 11, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(375, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  40 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(376, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  40 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(377, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  40 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(378, 16, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  40 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(379, 11, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  40 من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(380, 1, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(381, 13, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(382, 16, 'إكتمال طلب', 'تم تأكيد إكتمال طلب وغلقه من قبل مسؤول قسم أولالخاص بالفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(383, 10, 'إكتمال طلب', ' تم إكتمال الطلب الخاص بك رقم   40 من قبل المسؤول  مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(384, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(385, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(386, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(387, 16, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(388, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(389, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(390, 16, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(391, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(392, 1, 'system.order_refuse', 'تمت رفض الطلب رقم  41 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقنيسبب الرفضغير مختص', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(393, 12, 'system.order_refuse', 'تمت رفض الطلب رقم  41 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقنيسبب الرفضغير مختص', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(394, 13, 'system.order_refuse', 'تمت رفض الطلب رقم  41 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقنيسبب الرفضغير مختص', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(395, 16, 'system.order_refuse', 'تمت رفض الطلب رقم  41 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقنيسبب الرفضغير مختص', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(396, 11, 'رفض طلب من فني', 'تم رفض طلبك رقم 41 من قبل الفني فني تقني وسبب الرفض  غير مختص', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(397, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(398, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(399, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(400, 16, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(401, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(402, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(403, 16, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(404, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(405, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  42الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(406, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  42الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(407, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  42الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(408, 16, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  42الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(409, 11, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(410, 1, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  42 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(411, 12, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  42 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(412, 13, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  42 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(413, 16, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  42 الخاص بالفني  فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(414, 11, 'إنتهاء طلب', 'تمت إنتهاء العمل على الطلب رقم  42 من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(415, 1, 'لم يكتمل', 'لم يكتمل الطلب رقم  42 الخاص بالفني  فني تقنيمن قبل المسؤولمسؤول قسم أول و السبب لم يكتمل الطلب نظرا لتسبب مشاكل اخري', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(416, 12, 'لم يكتمل', 'لم يكتمل الطلب رقم  42 الخاص بالفني  فني تقنيمن قبل المسؤولمسؤول قسم أول و السبب لم يكتمل الطلب نظرا لتسبب مشاكل اخري', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(417, 13, 'لم يكتمل', 'لم يكتمل الطلب رقم  42 الخاص بالفني  فني تقنيمن قبل المسؤولمسؤول قسم أول و السبب لم يكتمل الطلب نظرا لتسبب مشاكل اخري', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(418, 16, 'لم يكتمل', 'لم يكتمل الطلب رقم  42 الخاص بالفني  فني تقنيمن قبل المسؤولمسؤول قسم أول و السبب لم يكتمل الطلب نظرا لتسبب مشاكل اخري', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(419, 11, 'الطلب لم يكتمل', 'لم يكتمل الطلب رقم  42 من قبل الفني فني تقني و سبب عدم الإكتمال لم يكتمل الطلب نظرا لتسبب مشاكل اخري', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(420, 1, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(421, 12, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(422, 13, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(423, 16, 'طلب جديد', 'تم إنشاء طلب جديد من المسؤول مسؤول قسم أول', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/storage/photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(424, 1, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(425, 13, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(426, 16, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1للفنيفني تقنيو في إنتظار الموافقة ', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(427, 10, 'طلب جديد', 'تم تعيين طلب جديد من المسؤول منسق أوامر 1', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/storage/photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(428, 1, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  43الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(429, 12, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  43الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(430, 13, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  43الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(431, 16, 'قبول طلب من فني ', 'تمت الموافقة على الطلب رقم  43الخاص بالفني فني تقنيمن قبل المسؤولفني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(432, 11, 'قبول طلب من فني ', 'تم الموافقة على طلبك من قبل الفني فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(433, 1, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(434, 13, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(435, 14, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(436, 15, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(437, 16, 'system.exchange', 'تم إنشاء أمر صرف جديد من قبل المسؤول فني تقني', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/storage/photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(438, 1, 'موافقة على أمر صرف', 'موافقة على أمر صرف7من قبل المسؤولمسئول المستودع', 'https://hospital.panorama-q.com/storage/', 'https://hospital.panorama-q.com/storage/', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(439, 13, 'موافقة على أمر صرف', 'موافقة على أمر صرف7من قبل المسؤولمسئول المستودع', 'https://hospital.panorama-q.com/storage/', 'https://hospital.panorama-q.com/storage/', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(440, 15, 'موافقة على أمر صرف', 'موافقة على أمر صرف7من قبل المسؤولمسئول المستودع', 'https://hospital.panorama-q.com/storage/', 'https://hospital.panorama-q.com/storage/', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(441, 16, 'موافقة على أمر صرف', 'موافقة على أمر صرف7من قبل المسؤولمسئول المستودع', 'https://hospital.panorama-q.com/storage/', 'https://hospital.panorama-q.com/storage/', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL),
(442, 10, 'موافقة على أمر صرف', 'موافقة على أمر صرف7من قبل المسؤولمسئول المستودع', 'https://hospital.panorama-q.com/storage/', 'https://hospital.panorama-q.com/storage/', 'https://hospital.panorama-q.com/dashboard/notifications', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `not_completeds`
--

CREATE TABLE `not_completeds` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `in_complete_reason` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `not_completeds`
--

INSERT INTO `not_completeds` (`id`, `order_id`, `in_complete_reason`, `image`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 25, 'لم يكتمل الطلب بنجاح حيث انه هناك مشكله في الكهرباء', NULL, '2019-07-04 11:21:12', '2019-07-04 11:21:12', 11),
(2, 42, 'لم يكتمل الطلب نظرا لتسبب مشاكل اخري', 'photos/CS1JX5FFYCj2ohaHLGsGpme6iNd8p2mjaHr1RJVO.jpeg', '2020-10-22 11:10:32', '2020-10-22 11:10:32', 11);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dept_id` int(10) UNSIGNED NOT NULL,
  `status` enum('new','pending','accepted','completed','finished','refused','not_completed') COLLATE utf8mb4_unicode_ci NOT NULL,
  `refuse_reason` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `technical_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tech_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `name`, `image`, `dept_id`, `status`, `refuse_reason`, `technical_id`, `created_at`, `updated_at`, `tech_name`) VALUES
(17, 11, 'مشكله كهرباء', 'photos/oNI2lc7TwuZekUUo5v9SQ0PN5FZEPIYpLt3HMb4V.jpeg', 6, 'finished', NULL, 10, '2019-04-30 07:50:13', '2019-04-30 07:57:42', NULL),
(16, 11, 'تغيير انارة', 'photos/K8V4etZ4eYVPdqywA2jde18MYJJgnzeWEFeppEXl.jpeg', 6, 'finished', NULL, 10, '2019-04-25 08:01:06', '2019-04-25 08:38:14', NULL),
(15, 11, 'اصلاح المغسلة', 'photos/54377tEb4UpBeuwSBvrKoHdfuRVoXJgEAKEDXTuL.jpeg', 6, 'accepted', NULL, 10, '2019-04-23 08:39:56', '2019-04-25 08:41:07', NULL),
(14, 11, 'اصلاح الانارة', 'photos/FSBwebfPj0XdAgoaQFfRgQripvcF1p3bZSwWqb4L.jpeg', 6, 'refused', 'عدم', 10, '2019-04-23 08:18:41', '2019-04-25 08:49:07', NULL),
(13, 11, 'تصليح مكيف بالقسم', 'photos/jdEoPx55kDdE1h8dZ0ZNf1sNJwbfiqmKIrbK24Kj.jpeg', 6, 'finished', NULL, 10, '2019-04-13 16:15:32', '2019-06-23 05:29:33', NULL),
(34, 11, 'اصلاح تكييف', NULL, 6, 'finished', NULL, 10, '2020-08-18 10:46:51', '2020-08-18 11:05:51', NULL),
(32, 1, 'test', NULL, 1, 'finished', NULL, 10, '2020-03-08 17:12:18', '2020-03-08 17:14:02', NULL),
(33, 11, 'hjlkhgjfjfghfhcplghxdf', NULL, 6, 'finished', NULL, 10, '2020-03-11 13:21:29', '2020-03-11 13:26:48', NULL),
(22, 13, 'helllapsox asxjk', 'photos/KLifCQiCuTTrgWhhSocvUXEW1EqW8emqGcvXyZHN.jpeg', 5, 'new', NULL, NULL, '2019-06-30 11:06:27', '2019-06-30 11:06:27', NULL),
(23, 11, 'Gif', NULL, 6, 'new', NULL, NULL, '2019-07-02 18:58:48', '2019-07-02 18:58:48', NULL),
(24, 11, 'fix lamp', NULL, 6, 'finished', NULL, 10, '2019-07-04 06:05:54', '2019-07-04 06:18:21', NULL),
(25, 11, 'طلب جديد من قسم الجلدية', NULL, 6, 'finished', NULL, 10, '2019-07-04 10:31:05', '2019-07-04 11:26:58', NULL),
(26, 11, 'تصليح باب المكتب', NULL, 6, 'finished', NULL, 10, '2019-07-04 11:28:13', '2019-07-04 11:44:24', NULL),
(27, 11, 'تصليح كهرباء', NULL, 6, 'accepted', NULL, 10, '2019-07-31 12:53:00', '2019-07-31 12:54:27', NULL),
(28, 11, 'اصلاح تكييف', NULL, 6, 'finished', NULL, 10, '2019-08-21 11:13:43', '2019-08-21 11:31:35', NULL),
(29, 11, 'اصلاح تكييف', NULL, 6, 'completed', NULL, 10, '2019-08-21 11:44:42', '2019-08-21 11:50:28', NULL),
(30, 11, 'طلب مخصوص بالكفته', NULL, 6, 'finished', NULL, 10, '2019-09-04 15:11:34', '2019-09-04 15:15:51', NULL),
(35, 11, 'لمبة لاتعمل', 'photos/6KYwIlDEYnCrxI3ZwZF6bFSvkrZiRnuyTdfXCd6F.png', 6, 'new', NULL, NULL, '2020-09-15 10:42:16', '2020-09-15 10:42:16', NULL),
(36, 11, 'عبدالله\\', NULL, 6, 'new', NULL, NULL, '2020-09-20 10:32:03', '2020-09-20 10:32:03', NULL),
(37, 11, 'تغيير شطاف', NULL, 6, 'finished', NULL, 10, '2020-09-20 10:32:48', '2020-09-20 10:36:51', NULL),
(38, 11, 'نلات', NULL, 6, 'new', NULL, NULL, '2020-10-04 12:06:54', '2020-10-04 12:06:54', NULL),
(39, 11, 'طلب جديد', 'photos/PIXVbspxHydWR0SyWfZNxzTnq0iXTHCG4qQiPsuv.jpeg', 6, 'completed', NULL, 10, '2020-10-13 14:05:11', '2020-10-14 15:35:41', NULL),
(40, 11, 'مشكله في التكييف', 'photos/B9NZN26GZkTJ5Rfb3SwT02Cc2JbbyS3fIDBEpXPC.jpeg', 6, 'finished', NULL, 10, '2020-10-22 10:58:56', '2020-10-22 11:03:46', NULL),
(41, 11, 'اصلاح كمبيوتر', NULL, 6, 'refused', 'غير مختص', 10, '2020-10-22 11:05:05', '2020-10-22 11:06:00', NULL),
(42, 11, 'طلب جديد في قسم الجلدية', 'photos/Wq4PzVgu1eFov3W4LSQ0MIo5ydtYroLnKwUqZjON.jpeg', 6, 'not_completed', NULL, 10, '2020-10-22 11:08:54', '2020-10-22 11:10:32', NULL),
(43, 11, 'محمود صبحي', NULL, 6, 'accepted', NULL, 10, '2020-10-22 11:19:35', '2020-10-22 11:20:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('omarzain012@gmail.com', '$2y$10$ZA6tZ.vR7XJUKPtbqjUf8unTfA86WgFCOnW4sPytqwR2YCaDJJlka', '2019-03-24 10:48:08'),
('admin@admin.com', '$2y$10$YNBHx/I0/NSH6vygTqOFUeAxzwQRGMZFdZhbDLdYt5RkJdT7gm1Rm', '2019-04-02 12:28:00');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `barcode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `qty`, `notes`, `created_at`, `updated_at`, `price`, `barcode`) VALUES
(1, 'منتج جديد', 9, NULL, '2019-04-30 08:10:22', '2020-10-22 11:22:39', 100.00, NULL),
(2, 'منتج جديد2', 2, NULL, '2019-05-18 19:36:27', '2020-10-22 11:22:39', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `refuses`
--

CREATE TABLE `refuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `refuse_reason` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `refuses`
--

INSERT INTO `refuses` (`id`, `order_id`, `user_id`, `refuse_reason`, `created_at`, `updated_at`) VALUES
(1, 41, 10, 'غير مختص', '2020-10-22 11:06:00', '2020-10-22 11:06:00');

-- --------------------------------------------------------

--
-- Table structure for table `specializations`
--

CREATE TABLE `specializations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `specializations`
--

INSERT INTO `specializations` (`id`, `name`, `created_at`, `updated_at`, `en_name`) VALUES
(4, 'تصليح', '2019-04-02 11:30:02', '2020-10-13 13:34:03', 'Repairing'),
(5, 'نقل', '2019-07-04 11:15:21', '2020-10-13 13:33:31', 'Move');

-- --------------------------------------------------------

--
-- Table structure for table `supplies`
--

CREATE TABLE `supplies` (
  `id` int(10) UNSIGNED NOT NULL,
  `bill_number` int(11) NOT NULL,
  `bill_date` date NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supplies`
--

INSERT INTO `supplies` (`id`, `bill_number`, `bill_date`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 101, '2019-04-30', 1, '2019-04-30 08:11:07', '2019-04-30 08:11:07'),
(2, 111, '2019-08-21', 14, '2019-08-21 11:49:11', '2019-08-21 11:49:11');

-- --------------------------------------------------------

--
-- Table structure for table `supply_details`
--

CREATE TABLE `supply_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `supply_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double(8,2) DEFAULT NULL,
  `total` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supply_details`
--

INSERT INTO `supply_details` (`id`, `supply_id`, `product_id`, `qty`, `price`, `total`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 10, NULL, 1000.00, '2019-04-30 08:11:07', '2019-04-30 08:11:07'),
(2, 2, 1, 20, 100.00, 2000.00, '2019-08-21 11:49:11', '2019-08-21 11:49:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `suspend_reason` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` enum('super','technical','dept_admin','coordinator','warehouse_admin','reporter') COLLATE utf8mb4_unicode_ci NOT NULL,
  `specialize_id` int(11) DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `job_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `email`, `password`, `is_active`, `suspend_reason`, `image`, `role`, `specialize_id`, `dept_id`, `created_at`, `updated_at`, `job_number`, `remember_token`) VALUES
(1, 'مدير نظام', '01111111111', 'admin@admin.com', '$2y$10$NrvyG.UtMdBqRctrpt6Pg.LHAOJY85st0OZYWBkBoA6ypz45VVQp.', 1, NULL, 'photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'super', NULL, NULL, '2019-03-19 12:31:40', '2020-10-26 17:06:50', '1234', 'PuFPXqwGoCkibpq5dngoiyYlj3we0WdyLWDg8hU3h8OALddlw9ngZQkTKqAu'),
(10, 'فني تقني', '01111111116', 'tech@admin.com', '$2y$10$TYCKHi49fLyA3OBr0DuaNOTPzFqKaTYr90Am4U3uVn1KJRjL8ewEe', 1, NULL, 'photos/AFmndkDFgIAQHVknovEtFu4iYYtlShuE1rbHqz5p.png', 'technical', 4, NULL, '2019-04-02 12:24:39', '2020-10-22 11:23:03', '01478', 'sTsSvnF3U2tTkuqJR8NF1OUKAACwupxXDPevNURmpY0Ezjy7X8SrilUogBuT'),
(11, 'مسؤول قسم أول', '01111114795', 'dept@admin.com', '$2y$10$on7/hRhe/PhhnqeQFTZskO7PNZP89uMIdN17dqcioqnJVA1dFJrWC', 1, NULL, 'photos/aW26BXGlzdJQeTH5VShrzGZcwJvLgrK5TopQEjC2.jpeg', 'dept_admin', NULL, 6, '2019-04-02 12:26:18', '2020-10-22 11:19:22', '01452', 'VDEzw2PqHPtfqix1vZzh4PqYQLd8BPz44ylCsntDEzMI2lGTzmrKHw1pWhfy'),
(12, 'منسق أوامر 1', '01117896522', 'coordinator@admin.com', '$2y$10$Ziwk.BSAIMDwoYdiuqd99OolOAEzoVFo8pJRVqAmv3Ot4q.MEANFq', 1, NULL, 'photos/gLxSZipT61koMKRA1hN4DgW4u6pwOW5rsdJesvm5.jpeg', 'coordinator', NULL, NULL, '2019-04-02 12:27:27', '2020-10-22 11:19:59', '012322', 'uHl3izVri30NOPyOZEiQ1hsMVqKAP881ygEjKFTEvjGMPey43ydbQ3RDyGQW'),
(13, 'مدير نظام', '01111111117', 'admin2@admin.com', '$2y$10$oeyTxKT6ucoBPxG6JYAVeuIV.duiDeyFEXnnApp6ndNzlsqH8Mkri', 1, NULL, 'photos/X6bZAIv54Moc9lcG92xpiQzu2bh7dQAvoxxjTInR.png', 'super', NULL, NULL, '2019-03-19 12:31:40', '2019-06-30 11:03:40', '1234', 'jwzYfbe1CQEmjhfEIkw5RG85Vq6aV4uomqG1Wv6CgK0b8YkP6Uqu34BNdDWN'),
(14, 'مسئول المستودع', '123456789', 'store@admin.com', '$2y$10$eLjnpUM2cA.ydRYM6SEyAO92n17ZG/DDUMft8/FzmUWp6Jj.00igq', 1, NULL, NULL, 'warehouse_admin', NULL, NULL, '2019-07-04 06:04:20', '2020-10-22 11:22:13', '123454454545', 'cxUKRHKeLHJnnpIXiRUUd6QFkFiP3XtkGdSAyeI7RdJLGCZUVBqTEuv4x0FT'),
(15, 'مدير  المستودع', '0583987220', 'ali009etf3@gmail.com', '$2y$10$uIUXuwQKcID5XgqYC85LreM4YyqxnPr.PvoHEhOZn8nGEjw9EtSka', 1, NULL, NULL, 'warehouse_admin', NULL, NULL, '2020-09-15 10:57:38', '2020-09-15 11:23:43', '1', '49VWM2EBzUAK6cC1TMZvqZ6REHnTgvxu0nJZzk4d4Shc9hsBR0mrW5AClx2k'),
(16, 'مصطفى', '0126448997', 'm@dsasd.aa', '$2y$10$XERqK4mljP8UOoTsiEQKeu9LurgxCG3CI4DDYNBls3ixZFNpEYR2q', 1, NULL, NULL, 'super', NULL, NULL, '2020-10-06 09:46:35', '2020-10-06 09:50:17', '20', 'G9BJxqi5GxvyGGeD7DyoCqCdKaSm10VbiNSCeHiNMkinotGd3G4odqyru52w'),
(17, 'Reporter', '456879465', 'reporter@reporter.com', '$2y$10$GIk4qvo4knw.sU0Sh1gYSevB37vCl6DKGpGednKwYwoTXkIjqMqfq', 1, NULL, NULL, 'reporter', NULL, NULL, '2020-10-06 09:47:45', '2020-10-22 10:54:51', '465', '3zNe6rO0xmmxIWwQuo1zsHWTuArPmADdI2KpL3gPy0fAUd89ujKXSDjekahA');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exchanges`
--
ALTER TABLE `exchanges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exchange_details`
--
ALTER TABLE `exchange_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `not_completeds`
--
ALTER TABLE `not_completeds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `not_completeds_user_id_foreign` (`user_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refuses`
--
ALTER TABLE `refuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `refuses_order_id_foreign` (`order_id`),
  ADD KEY `refuses_user_id_foreign` (`user_id`);

--
-- Indexes for table `specializations`
--
ALTER TABLE `specializations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplies`
--
ALTER TABLE `supplies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supply_details`
--
ALTER TABLE `supply_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supply_details_supply_id_foreign` (`supply_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `exchanges`
--
ALTER TABLE `exchanges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `exchange_details`
--
ALTER TABLE `exchange_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=443;

--
-- AUTO_INCREMENT for table `not_completeds`
--
ALTER TABLE `not_completeds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `refuses`
--
ALTER TABLE `refuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `specializations`
--
ALTER TABLE `specializations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `supplies`
--
ALTER TABLE `supplies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `supply_details`
--
ALTER TABLE `supply_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `supply_details`
--
ALTER TABLE `supply_details`
  ADD CONSTRAINT `supply_details_supply_id_foreign` FOREIGN KEY (`supply_id`) REFERENCES `supplies` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
