@extends('admin.layout.master')
@section('title',__('system.edit_dept'))



@section('content')

    <!-- Page Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">
                <a href="{{route('departments.index')}}" class="btn btn-custom dropdown-toggle waves-effect waves-light" >{{__('Back To Departments')}} <span class="m-l-5"><i class="fa fa-reply"></i></span></a>
            </div>
            <h4 class="page-title">@lang('system.edit_dept') - {{$department->name}}</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30">@lang('system.department_date')</h4>

                <div class="row">
                    <form method="post" action="{{route('departments.update',$department->id)}}" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{method_field('PUT')}}
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">{{__('Arabic Name')}}</label>
                                <div class="col-md-10">
                                    <input type="text" required value="{{ $department->name}}"
                                           autocomplete="off"
                                           data-parsley-required-message="@lang('system.field_required')"
                                           data-parsley-trigger="keyup"
                                           data-parsley-maxlength="60"
                                           data-parsley-maxlength-message="@lang('system.max_char_60')"
                                           name="name" class="form-control" placeholder="{{__('Arabic Name')}}">

                                    @if($errors->has('name'))
                                        <p class="help-block">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">{{__('English Name')}}</label>
                                <div class="col-md-10">
                                    <input type="text" required value="{{ $department->en_name}}"
                                           autocomplete="off"
                                           data-parsley-required-message="@lang('system.field_required')"
                                           data-parsley-trigger="keyup"
                                           data-parsley-maxlength="60"
                                           data-parsley-maxlength-message="@lang('system.max_char_60')"
                                           name="en_name" class="form-control" placeholder="{{__('English Name')}}">

                                    @if($errors->has('en_name'))
                                        <p class="help-block">
                                            {{ $errors->first('en_name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>



                        {{-- buttons --}}
                        <div class="col-lg-12">
                            <div class="form-group text-right m-t-20">
                                <button class="btn btn-primary waves-effect waves-light m-t-20" id="btnSubmit" type="submit">
                                    @lang('system.edit')
                                </button>
                            </div>
                        </div>
                    </form>


                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>



@endsection

@section('scripts')


@endsection
