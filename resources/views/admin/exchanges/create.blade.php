@extends('admin.layout.master')
@section('title', __('system.exchange_order'))

@section('styles')
<style>
    .help-block {
        color:red;
    }
</style>
@endsection
@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">
                <a href="{{route('exchanges.index')}}" class="btn btn-custom dropdown-toggle waves-effect waves-light" >@lang('system.return_to_exchanges')<span class="m-l-5"><i class="fa fa-reply"></i></span></a>
            </div>
            <h4 class="page-title">@lang('system.add_new_exchange')</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">


                <h4 class="header-title m-t-0 m-b-30">@lang('system.exchange_data')</h4>

                <div class="row">

                    <form method="post" action="{{route('exchanges.store')}}" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="col-md-4">
                                <label class=" control-label">@lang('system.order_NO')*</label>
                                <input type="number" required value="{{old('order_id')}}"
                                       data-parsley-required-message="@lang('system.field_required')"
                                       data-parsley-trigger="keyup"
                                       data-parsley-maxlength="20"
                                       data-parsley-maxlength-message="@lang('system.max_char_20')"
                                       {{--oninput="this.value = Math.abs(this.value)"--}}
                                       name="order_id" class="form-control" placeholder="@lang('system.order_NO')">
                                @if($errors->has('order_id'))
                                    <p class="help-block" style="color: #FF0000;">
                                        {{ $errors->first('order_id') }}
                                    </p>
                                @endif
                            </div>


                            <div class="col-md-4">
                                <label class=" control-label">@lang('system.technical_name')*</label>

                                @if(auth()->user()->role == 'technical')
                                    <select class="form-control"
                                            id="technical"
                                            data-parsley-trigger="select" name="technical_id"
                                            data-parsley-required-message="@lang('system.field_required')">
                                            <option value="{{auth()->id()}}" selected >{{auth()->user()->name}}</option>
                                    </select>
                                @else
                                    <select class="form-control"
                                            id="technical"
                                            data-parsley-trigger="select" name="technical_id"
                                            data-parsley-required-message="@lang('system.field_required')">
                                        <option value="" disabled>@lang('system.choose_technical')</option>
                                        @foreach ($technicals  as $tech)
                                            <option value="{{$tech->id}}" @if(auth()->id() == $tech->id) selected @endif >{{$tech->name}}</option>
                                        @endforeach
                                    </select>
                                @endif


                                @if($errors->has('technical_id'))
                                <p class="help-block" style="color: #FF0000;">
                                {{ $errors->first('technical_id') }}
                                </p>
                                @endif
                            </div>


                            <div class="col-md-4">
                                <label class=" control-label">@lang('system.exchange_date')*</label>
                                <input name="date"  data-parsley-maxlength="20"
                                       data-parsley-maxlength-message="@lang('system.max_char_20')"
                                       required autocomplete="false"
                                       data-parsley-required-message="@lang('system.field_required')"
                                       type="date" class="form-control"
                                >
                                @if($errors->has('date'))
                                    <p class="help-block">
                                        {{ $errors->first('date') }}
                                    </p>
                                @endif
                            </div>


                            <div class="col-md-12">
                                <label class=" control-label">@lang('system.exchange_speci'):*</label>
                              <textarea name="description" required data-parsley-required-message="@lang('system.field-required')" class="form-control" placeholder=" .... "></textarea>
                                @if($errors->has('bill_date'))
                                    <p class="help-block">
                                        {{ $errors->first('bill_date') }}
                                    </p>
                                @endif
                            </div>

                        </div>


                        <div class="form-group">
                            <div class="col-md-5">
                                <label class="control-label">  @lang('system.choose_product')*</label>
                                <select class="form-control"
                                        id="product"
                                        data-parsley-trigger="select"
                                        data-parsley-required-message="@lang('system.field_required')">
                                    <option value="" selected disabled>@lang('system.choose_product')</option>
                                    @foreach ($products  as $product)
                                        <option data-name="{{$product->name}}" data-price="{{$product->price}}" value="{{$product->id}}">{{$product->name}}</option>
                                    @endforeach
                                </select>

                                @if($errors->has('products'))
                                    <p class="help-block" style="color: #FF0000;">
                                        {{ $errors->first('products') }}
                                    </p>
                                @endif
                            </div>


                            <div class="col-md-5">
                                <label class="control-label">@lang('system.qty')*</label>
                                <input id="qty" type="number" value="{{old('qty')}}"
                                       data-parsley-required-message="@lang('system.field_required')"
                                       data-parsley-trigger="keyup"

                                       oninput="this.value = Math.abs(this.value)"
                                       class="form-control" placeholder="@lang('system.qty')">
                                @if($errors->has('qty'))
                                    <p class="help-block">
                                        {{ $errors->first('qty') }}
                                    </p>
                                @endif
                            </div>

                            <div class="col-md-2">
                                <button id="addProduct" class="btn btn-primary waves-effect waves-light m-t-20"  type="button">
                                    @lang('system.add')
                                </button>

                            </div>

                        </div>

                         {{--products table--}}

                        <hr style="color:#ACACAC;background-color:#ACACAC; height: 3px; border: none;">

                        <h4 class="header-title m-t-0 m-b-30">@lang('system.products_list')</h4>
                        <div class="table-responsive">
                            <table id="productsTable" class="table m-0">
                                <thead>
                                <tr>
                                    <th>@lang('system.product_name')</th>
                                    <th>@lang('system.qty')</th>
                                    <th>@lang('system.delete')</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--<tr>--}}
                                {{--<td>Table cell</td>--}}
                                {{--<td>Table cell</td>--}}
                                {{--<td>Table cell</td>--}}
                                {{--<td>--}}
                                {{--<a href="javascript:;" class="btn btn-danger waves-effect waves-light btn-xs m-b-5">حذف</a>--}}
                                {{--</td>--}}
                                {{--<input type="hidden" name="products[]" value="id" />--}}
                                {{--</tr>--}}

                                </tbody>
                            </table>
                        </div>


                        {{-- buttons --}}
                        <div class="col-lg-12">
                            <div class="form-group text-right m-t-20">
                                <button class="btn btn-primary waves-effect waves-light m-t-20" id="btnSubmit" type="submit">
                                    @lang('system.register')
                                </button>
                                <button onclick="window.history.back();return false;" type="reset"
                                        class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                                    @lang('system.cancel')
                                </button>
                            </div>
                        </div>
                    </form>


                </div><!-- end row -->


            </div>
        </div><!-- end col -->
    </div>
@endsection
@section('scripts')
    <script>
        // Date Picker
        jQuery('#datepicker').datepicker();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });




    </script>



    <script>
        $('body').delegate('#addProduct','click', function(){


            var product_name   = $('#product').find("option:selected").attr('data-name');
            var product_id = $('#product').find("option:selected").val();
            var deleteId = "removeProduct"+product_id;
            var trId = "tr"+product_id;

            var qty = $('#qty').val();
            var price = $('#product').find("option:selected").attr('data-price');

            if(product_name == null || qty <= 0 || qty == "" ){
                $('#addProduct').attr('disabled')==true;
            }else {
                $('#addProduct').attr('disabled')==false;
                $('#productsTable > tbody:last-child').append(
                    '<tr id="'+trId+'">' +
                    '<td>' + product_name + '</td>'+
                    '<td>' + qty + '</td>'+
                    '<td>' +
                    '<a href="javascript:;" id="' +deleteId +'" data-id="'+product_id+'"  class="removeProduct btn btn-danger waves-effect waves-light btn-xs m-b-5">'+'@lang('system.delete')'+'</a>' + '</td>'+
                    '<input type="hidden" name="products[]" value="' + product_id + '" />' +
                    '<input type="hidden" name="qtys[]" value="' + qty + '" />' +
                    '</tr>');

                $('#product').prop('selectedIndex',0);
                $('#qty').val('');
            }


//
        });


        $('body').on('click', '.removeProduct', function () {
            var id = $(this).attr('data-id');
            var tr = $(this).closest($('#removeProduct' + id).parent().parent());

            tr.find('td').fadeOut(500, function () {
                tr.remove();
            });
        });

        $('#product').change(function () {
            var id = $(this).val();
            $.ajax({
                type: 'POST',
                url: '{{ route('getAjaxProductQty') }}',
                data: {id: id},
                dataType: 'json',

                success: function (data) {
//                    $.each(data.data, function (element, ele) {
//                        console.log(ele);
//                        $('#students').append("<option value='"+ele.id+"'>" + ele.name + "</option>");
//                    });
//                    $('#full_degree').html("الدرجة الكلية "+data.data);
                    $('#qty').attr("data-parsley-max",data.data);
                    $('#qty').attr("data-parsley-max-message","@lang('system.allowed_qty') "+data.data);
                }
            });
        });



    </script>



@endsection
