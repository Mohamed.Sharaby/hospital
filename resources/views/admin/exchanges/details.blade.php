@extends('admin.layout.master')
@section('title',__('system.exchange_details'))
@section('styles')
<link href="{{asset('admin/assets/css/print_bill.css')}}" rel="stylesheet" type="text/css" />
<!-- <style>
.dataTables_filter, .dataTables_info, .pagination { display: none; }
</style>
<style type="text/css" media="print">
.prevent, .no-print,.dt-buttons,.dataTables_filter,.dataTables_info,.pagination { display: none; }
</style> -->
@endsection

@section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-xs-12">

        <div class="btn-group pull-right m-t-15">
            <button onclick="window.history.back();" class="no-print btn btn-custom dropdown-toggle waves-effect waves-light">
                @lang('system.back')
                <span class="m-l-5"><i class="fa fa-arrow-left"></i></span>
            </button>

        </div>

        <h4 class="page-title"> @lang('system.exchange_no'): {{$exchange->id}}</h4>
    </div>
</div>
<!--End Page-Title -->


<div class="row">
    <button type="button" id="print-all">طباعة</button>
    <div class="col-xs-12">
        <div>
            <div class="card-box table-responsive" id="myDivToPrint">

                <h3 class="header-title m-t-0 m-b-30">@lang('system.exchange_data') : </h3>
                {{--<button onclick="window.print();"  class="no-print btn btn-custom dropdown-toggle waves-effect waves-light">--}}
                {{--طباعة تقرير كامل--}}
                {{--<span class="m-l-5"><i class="fa fa-print"></i></span>--}}
                {{--</button>--}}
                <div class="row">
                    <div class="col-xs-12">

                        <div class="col-md-4">
                            <h4>@lang('system.ex_no'):</h4>
                            <h5 style="font-weight: 600;">{{$exchange->id}}</h5>
                        </div>
                        <div class="col-md-4">
                            <h4>@lang('system.order_NO')</h4>
                            <h5 style="font-weight: 600;">{{$exchange->order_id}}</h5>
                        </div>
                        <div class="col-md-4">
                            <h4>@lang('system.technical_name')</h4>
                            <h5 style="font-weight: 600;">{{$exchange->technical->name}}</h5>
                        </div>
                        <div class="col-md-4 hide-print">
                            <h4>@lang('system.ex_user')</h4>
                            <h5 style="font-weight: 600;">{{$exchange->user->name}}</h5>
                        </div>
                        <div class="col-md-4">
                            <h4>@lang('system.exchange_date')</h4>
                            <h5 style="font-weight: 600;">{{$exchange->date}}</h5>
                        </div>
                        <div class="col-md-4 hide-print">
                            <h4>@lang('system.ex_status')</h4>
                            <h5 style="font-weight: 600;">
                                @switch($exchange->status)
                                @case('wait') <label class="label label-warning">@lang('system.wait')</label> @break
                                @case('accepted') <label class="label label-success">@lang('system.accepted')</label> @break
                                @case('refused') <label class="label label-danger">@lang('system.refused')</label> @break
                                @endswitch
                            </h5>
                        </div>
                        <div class="col-md-8">
                            <h4>@lang('system.ex_details')</h4>
                            <h5 style="font-weight: 600;">{{$exchange->description}}</h5>
                        </div>

                        @if($exchange->status == 'refused')
                        <div class="col-md-8">
                            <h4 style="color: #ff0000;">@lang('system.ex_refuse_reason'):</h4>
                            <h5 style="font-weight: 600; color: #ff0000;">{{$exchange->refuse_reason}}</h5>
                        </div>
                        @endif

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="card-box table-responsive">

                                    <h4 class="header-title m-t-0 m-b-30">@lang('system.ex_required_products')</h4>

                                    @if($exchange->status != 'wait')
                                    <table id="" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>@lang('system.No')</th>
                                                <th>@lang('system.product_name')</th>
                                                <th>@lang('system.qty')</th>
                                                {{--<th style="width: 250px;" >خيارات</th>--}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i = 1; @endphp
                                            @foreach($exchange->exchange_details as $row)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$row->product->name}}</td>
                                                <td>{{$row->qty}}</td>
                                                {{--<td>--}}
                                                {{--<a href="{{route('supplies.show',$row->id)}}" class="label label-primary">تفاصيل</a>--}}
                                                {{--<a href="{{route('supplies.edit',$row->id)}}" class="label label-warning">تعديل</a>--}}
                                                {{--<a id="elementRow{{$row->id}}" href="javascript:;" data-id="{{$row->id}}" data-url="{{route('supplies.destroy',$row->id)}}" class="removeElement label label-danger">حذف</a>--}}
                                                {{--</td>--}}
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                    @else
                                    <form method="post" action="{{route('exchanges.accept')}}" class="form-inline">
                                        {{csrf_field()}}
                                        <input type="hidden" name="exchange_id" value="{{$exchange->id}}">
                                        <table id="" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>@lang('system.No')</th>
                                                    <th>@lang('system.product_name')</th>
                                                    <th>@lang('system.required_qty')</th>
                                                    <th>@lang('system.product_qty')</th>
                                                    @if(auth()->user()->role == 'warehouse_admin' || auth()->user()->role == 'super')
                                                    <th>@lang('system.discharged_qty')</th>
                                                    @endif

                                                    {{--<th style="width: 250px;" >خيارات</th>--}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php $i = 1; @endphp
                                                @foreach($exchange->exchange_details as $row)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{$row->product->name}}</td>
                                                    <td>{{$row->qty}}</td>
                                                    <td>{{$row->product->qty}}</td>
                                                    @if(auth()->user()->role == 'warehouse_admin' || auth()->user()->role == 'super')
                                                    <td>

                                                        <input type="hidden" name="rowsId[]" value="{{$row->id}}">
                                                        <input type="hidden" name="productsIds[]" value="{{$row->product_id}}">
                                                        <input type="number" class="form-control" name="qtys[]" data-parsley-trigger="keyup" oninput="this.value = Math.abs(this.value)" required data-parsley-required-message="@lang('system.field_required')" data-parsley-max="{{$row->product->qty}}" data-parsley-max-message="@lang('system.qty_is_over')">
                                                    </td>
                                                    @endif
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>


                                        {{-- buttons --}}
                                        @if( auth()->user()->role == 'super' || auth()->user()->role == 'warehouse_admin')
                                        <div class="col-lg-12">
                                            <div class="form-group text-right m-t-20">
                                                <button class="btn btn-primary waves-effect waves-light m-t-20" id="btnSubmit" type="submit">
                                                    @lang('system.accept')
                                                </button>
                                                <a id="elementRow{{$exchange->id}}" href="javascript:;" data-id="{{$exchange->id}}" data-action="suspend" data-url="{{route('exchange.refuse')}}" class="statusWithReason btn btn-danger m-t-20">@lang('system.refuse')</a>
                                            </div>
                                        </div>
                                        @endif

                                    </form>



                                    <!-- Refuse Modal -->
                                    <div class="modal fade" id="myModal_suspend" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 id="modal_header" class="modal-title">@lang('system.ex_refuse')</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <label for="reason">@lang('system.refuse_reason')</label>
                                                    <textarea id="suspend_reason" placeholder="@lang('system.enter_refuse_reason')" name="reason" class="form-control"></textarea>
                                                </div>
                                                <div class="modal-footer">
                                                    <button id="suspendButton" type="button" class="btn btn-danger">@lang('system.refuse')</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('system.cancel')</button>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!--End of row-->

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!----- start print ---->
<script>
   $("#print-all").click(function() {
	let t = document.getElementById("myDivToPrint").innerHTML;
	let style = `<style></style>`;
	let win = window.open('', '', );
	win.document.write(`<html><head><title>الفاتورة</title><link href="{{asset('admin/assets/css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css"/><link href="{{asset('admin/assets/css/responsive.css')}}" rel="stylesheet" type="text/css"/><link href="{{asset('admin/assets/css/print_bill.css')}}" rel="stylesheet" type="text/css"/></head><body>${t}</body></html>`);
	win.document.close();
	win.print();
});
</script>
<!----- end print ---->

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('body').on('click', '.statusWithReason', function() {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        var $tr = $(this).closest($('#elementRow' + id).parent().parent());
        var action = $(this).attr('data-action');
        var text = '';
        var type = '';
        var confirmButtonClass = '';
        var redirectionRoute = '';

        //  Modal data ....
        if (action === 'suspend') {
            text = '@lang('
            system.sure_refuse ')';
            type = 'error';
            confirmButtonClass = 'btn-danger waves-effect waves-light';


        }
        if (action === 'activate') {
            text = "@lang('system.sure_activate_user')";
            type = 'success';
            confirmButtonClass = 'btn-success waves-effect waves-light';

        }

        swal({
                title: "@lang('system.r_u_sure')",
                text: text,
                type: type,
                showCancelButton: true,
                confirmButtonColor: "#27dd24",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: confirmButtonClass,
                closeOnConfirm: true,
                closeOnCancel: true,
            },
            function(isConfirm) {
                if (isConfirm) {
                    if (action === 'activate') {
                        $('#myModal_active').modal('show');

                        $("#activeButton").click(function(e) {

                            var reason = $('#activate_reason').val();

                            $.ajax({
                                type: 'post',
                                url: url,
                                data: {
                                    id: id,
                                    action: action,
                                    reason: reason
                                },
                                dataType: 'json',
                                success: function(data) {
                                    if (data.status == true) {
                                        var title = data.title;
                                        var msg = data.message;
                                        toastr.options = {
                                            positionClass: 'toast-top-left',
                                            onclick: null
                                        };

                                        $('.modal').modal('hide');
                                        var $toast = toastr['success'](msg, title);
                                        $toastlast = $toast;

                                        function pageRedirect() {
                                            location.reload();
                                        }
                                        setTimeout(pageRedirect(), 2500);
                                    } else {
                                        var title = data.title;
                                        var msg = data.message;
                                        toastr.options = {
                                            positionClass: 'toast-top-left',
                                            onclick: null
                                        };

                                        var $toast = toastr['error'](msg, title);
                                        $toastlast = $toast
                                    }
                                }
                            });
                        });
                    }
                    if (action === 'suspend') {
                        $('#myModal_suspend').modal('show');

                        $("#suspendButton").click(function(e) {

                            var reason = $('#suspend_reason').val();

                            $.ajax({
                                type: 'post',
                                url: url,
                                data: {
                                    id: id,
                                    action: action,
                                    reason: reason
                                },
                                dataType: 'json',
                                success: function(data) {
                                    if (data.status == true) {
                                        var title = data.title;
                                        var msg = data.message;
                                        toastr.options = {
                                            positionClass: 'toast-top-left',
                                            onclick: null
                                        };

                                        $('.modal').modal('hide');
                                        var $toast = toastr['success'](msg, title);
                                        $toastlast = $toast;

                                        //                                            $tr.find('td').fadeOut(100,function () {
                                        //                                                $tr.remove();
                                        //                                            });

                                        function pageRedirect() {
                                            location.reload();
                                        }
                                        setTimeout(pageRedirect(), 2500);
                                    } else {
                                        var title = data.title;
                                        var msg = data.message;
                                        toastr.options = {
                                            positionClass: 'toast-top-left',
                                            onclick: null
                                        };

                                        var $toast = toastr['error'](msg, title);
                                        $toastlast = $toast
                                    }
                                }
                            });
                        });
                    }

                }

            }
        );
    })
</script>

@endsection
