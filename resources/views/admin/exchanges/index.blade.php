@extends('admin.layout.master')
@section('title',__('system.exchanges'))

@section('styles')

@endsection


@section('content')
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">

            <div class="btn-group pull-right m-t-15">
                @if(auth()->user()->role !='warehouse_admin')
                <a href="{{route('exchanges.create')}}" class="btn btn-custom dropdown-toggle waves-effect waves-light">
                    @lang('system.create_exchange')
                    <span class="m-l-5"><i class="fa fa-plus"></i></span>
                </a>
                @endif
            </div>

            <h4 class="page-title">@lang('system.exchanges')</h4>
        </div>
    </div><!--End Page-Title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <h4 class="header-title m-t-0 m-b-30">@lang('system.exchanges')</h4>

                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>@lang('system.ex_No')</th>
                        <th>@lang('system.order_No')</th>
                        <th>@lang('system.technical_name')</th>
                        <th>@lang('system.ex_user')</th>
                        <th>@lang('system.ex_date')</th>
                        <th>@lang('system.status')</th>
                        <th style="width: 250px;" >@lang('system.options')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $i = 1; @endphp
                    @foreach($exchanges as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->order_id}}</td>
                            <td>{{$row->technical->name}}</td>
                            <td>{{$row->user->name}}</td>
                            <td>
                                {{$row->date}}
                            </td>
                            <td>
                                @switch($row->status)
                                @case('wait') <label class="label label-warning">@lang('system.wait')</label> @break
                                @case('accepted') <label class="label label-success">@lang('system.accepted')</label> @break
                                @case('refused') <label class="label label-danger">@lang('system.refused')</label> @break
                                @endswitch</td>
                            <td>
                            <a href="{{route('exchanges.show',$row->id)}}" class="label label-primary">@lang('system.details')</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

@endsection

