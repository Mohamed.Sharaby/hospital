@extends('admin.layout.master')
@section('title',__('system.control_panel_title'))

@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">

            <h4 class="page-title">@lang('system.main_page')</h4>
        </div>
    </div>
{{--///////////////////////// Orders Chart ///////////////////////////////////--}}
    <div class="card" >
        <div class="card-header header-elements-inline">
            <h5 class="card-title" style="color: #eae4e4;">{{__('Orders Status Chart of Last 30 Days')}}</h5>
        </div>

        <div class="card-body">
            <div id="chart"></div>
        </div>
    </div>
    <hr>
{{--///////////////////////// Orders Chart ///////////////////////////////////--}}

    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <a href="{{route('departments.index')}}">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">@lang('system.dept_count')</h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#188AE2"
                                   data-bgColor="#D0E8F9" value="{{$data['departments']}}"
                                   data-skin="tron" data-angleOffset="180" data-readOnly=true
                                   data-thickness=".15"/>
                        </div>

                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0"> {{$data['departments']}} <i style="color: #188AE2;" class="fa fa-object-ungroup"></i></h2>
                            <p class="text-muted">@lang('system.dept')</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <a href="{{route('specializations.index')}}">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">@lang('system.specialties_count')</h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#FD5D93"
                                   data-bgColor="rgba(255, 188, 211, .8)" value="{{$data['specials']}}"
                                   data-skin="tron" data-angleOffset="180" data-readOnly=true
                                   data-thickness=".15"/>
                        </div>
                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0" data-plugin="counterup"> {{$data['specials']}}  <i style="color: #FD5D93;" class="fa fa-object-group"></i> </h2>
                            <p class="text-muted">@lang('system.specialty')</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <a href="{{route('products.index')}}">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">@lang('system.available_products')</h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#35b8e0"
                                   data-bgColor="#B8E6F4" value="{{$data['products']}}"
                                   data-skin="tron" data-angleOffset="180" data-readOnly=true
                                   data-thickness=".15"/>
                        </div>
                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0" data-plugin="counterup"> {{$data['products']}} <i style="color: #35b8e0;" class="fa fa-font-awesome"></i> </h2>
                            <p class="text-muted">@lang('system.product')</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <a href="{{route('users.index')}}">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">@lang('system.admins_count')</h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#10c469"
                                   data-bgColor="#AAE2C6" value="{{$data['admins']}}"
                                   data-skin="tron" data-angleOffset="180" data-readOnly=true
                                   data-thickness=".15"/>
                        </div>
                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0" data-plugin="counterup"> {{$data['admins']}} <i style="color: #10c469;" class="fa fa-address-card"></i> </h2>
                            <p class="text-muted">@lang('system.admin')</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <a href="{{route('users.index')}}">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">@lang('system.coordinators_count')</h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#5B69BC"
                                   data-bgColor="#DEE1F2" value="{{$data['coordinators']}}"
                                   data-skin="tron" data-angleOffset="180" data-readOnly=true
                                   data-thickness=".15"/>
                        </div>
                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0"> {{$data['coordinators']}} <i style="color: #5B69BC;" class="fa fa-users"></i></h2>
                            <p class="text-muted">@lang('system.coordinator')</p>
                        </div>
                    </div>
                </div>
            </a>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <a href="{{route('users.index')}}">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">@lang('system.dept_admins_count')</h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#188AE2"
                                   data-bgColor="#D0E8F9" value="{{$data['dept_admins']}}"
                                   data-skin="tron" data-angleOffset="180" data-readOnly=true
                                   data-thickness=".15"/>
                        </div>
                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0"> {{$data['dept_admins']}} <i style="color: #188AE2;" class="fa fa-user-md"></i></h2>
                            <p class="text-muted">@lang('system.dept_admin')</p>
                        </div>
                    </div>
                </div>
            </a>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <a href="{{route('users.index')}}">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">@lang('system.technicals_count')</h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#ffbd4a"
                                   data-bgColor="#FFE6BA" value="{{$data['techs']}}"
                                   data-skin="tron" data-angleOffset="180" data-readOnly=true
                                   data-thickness=".15"/>
                        </div>
                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0" data-plugin="counterup"> {{$data['techs']}}  <i style="color: #ffbd4a;" class="fa fa-hand-paper-o"></i> </h2>
                            <p class="text-muted">@lang('system.technical')</p>
                        </div>
                    </div>
                </div>
            </a>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <a href="{{route('orders.index')}}">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">@lang('system.orders_count')</h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#FF8ACC"
                                   data-bgColor="#FFE8F5" value="{{$data['orders']}}"
                                   data-skin="tron" data-angleOffset="180" data-readOnly=true
                                   data-thickness=".15"/>
                        </div>
                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0"> {{$data['orders']}} <i style="color: #ff282d;" class="fa fa-first-order"></i></h2>
                            <p class="text-muted">@lang('system.order')</p>
                        </div>
                    </div>
                </div>
            </a>
        </div><!-- end col -->


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <a href="{{route('orders.index')}}">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">@lang('system.new_orders')</h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#5B69BC"
                                   data-bgColor="#DEE1F2" value="{{$data['new_orders']}}"
                                   data-skin="tron" data-angleOffset="180" data-readOnly=true
                                   data-thickness=".15"/>
                        </div>
                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0"> {{$data['new_orders']}} <i style="color: #5B69BC;" class="fa fa-bullhorn"></i></h2>
                            <p class="text-muted">@lang('system.order')</p>
                        </div>
                    </div>
                </div>
            </a>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <a href="{{route('orders.index')}}">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">@lang('system.refused_orders')</h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#ff282d"
                                   data-bgColor="#ffd8bf" value="{{$data['refused_orders']}}"
                                   data-skin="tron" data-angleOffset="180" data-readOnly=true
                                   data-thickness=".15"/>
                        </div>
                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0"> {{$data['refused_orders']}} <i style="color: #ff282d;" class="fa fa-window-close"></i></h2>
                            <p class="text-muted">@lang('system.order')</p>
                        </div>
                    </div>
                </div>
            </a>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <a href="{{route('orders.index')}}">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">@lang('system.opened_orders')</h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#5B69BC"
                                   data-bgColor="#DEE1F2" value="{{$data['accepted_orders']}}"
                                   data-skin="tron" data-angleOffset="180" data-readOnly=true
                                   data-thickness=".15"/>
                        </div>
                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0"> {{$data['accepted_orders']}} <i style="color: #5B69BC;" class="fa fa-opencart"></i></h2>
                            <p class="text-muted">@lang('system.order')</p>
                        </div>
                    </div>
                </div>
            </a>
        </div><!-- end col -->


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <a href="{{route('orders.index')}}">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">@lang('system.completed_orders')</h4>
                    <div class="widget-chart-1">
                        <div class="widget-chart-box-1">
                            <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#10c469"
                                   data-bgColor="#AAE2C6" value="{{$data['completed_orders']}}"
                                   data-skin="tron" data-angleOffset="180" data-readOnly=true
                                   data-thickness=".15"/>
                        </div>
                        <div class="widget-detail-1">
                            <h2 class="p-t-10 m-b-0" data-plugin="counterup"> {{$data['completed_orders']}} <i style="color: #10c469;" class="fa fa-check"></i> </h2>
                            <p class="text-muted">@lang('system.order')</p>
                        </div>
                    </div>
                </div>
            </a>
        </div><!-- end col -->


    </div>
    <!-- end row -->


@endsection

@section('scripts')
    <script src="//www.gstatic.com/charts/loader.js"></script>
    <script>
        var GoogleComboChart = function() {


            //
            // Setup module components
            //

            // Combo chart
            var _googleComboChart = function() {
                if (typeof google == 'undefined') {
                    console.warn('Warning - Google Charts library is not loaded.');
                    return;
                }

                // Initialize chart
                google.charts.load('current', {
                    callback: function () {

                        // Draw chart
                        drawCombo();

                        // Resize on sidebar width change
                        $(document).on('click', '.sidebar-control', drawCombo);

                        // Resize on window resize
                        var resizeCombo;
                        $(window).on('resize', function() {
                            clearTimeout(resizeCombo);
                            resizeCombo = setTimeout(function () {
                                drawCombo();
                            }, 200);
                        });
                    },
                    packages: ['corechart']
                });

                // Chart settings
                function drawCombo() {

                    // Define charts element
                    var combo_chart_element = document.getElementById('chart');

                    // Data
                    var data = google.visualization.arrayToDataTable([
                        ['Day',     '{{__('New')}}',   '{{__('Pending')}}',  '{{__('Accepted')}}', '{{__('Completed')}}', '{{__('Not Completed')}}','{{__('Finished')}}','{{__('Refused')}}'],
                            @foreach($orders as $chart => $item)
                        [
                            '{{ $chart }}', {{ $item['new'] }}, {{ $item['pending'] }}, {{ $item['accepted'] }},
                            {{ $item['completed'] }}, {{ $item['not_completed'] }}, {{ $item['finished'] }},{{ $item['refused'] }},
                        ],
                        @endforeach
                    ]);


                    // Options
                    var options_combo = {
                        fontName: 'Roboto',
                        height: 400,
                        fontSize: 12,
                        chartArea: {
                            left: '5%',
                            width: '94%',
                            height: 350
                        },
                        seriesType: "bars",
                        // series: {
                        //     5: {
                        //         type: "line",
                        //         pointSize: 5
                        //     }
                        // },
                        tooltip: {
                            textStyle: {
                                fontName: 'Roboto',
                                fontSize: 13
                            }
                        },
                        vAxis: {
                            gridlines:{
                                color: '#e5e5e5',
                                count: 10
                            },
                            minValue: 0
                        },
                        legend: {
                            position: 'top',
                            alignment: 'center',
                            textStyle: {
                                fontSize: 16
                            }
                        }
                    };

                    // Draw chart
                    var combo = new google.visualization.ComboChart(combo_chart_element);
                    combo.draw(data, options_combo);
                }
            };


            //
            // Return objects assigned to module
            //

            return {
                init: function() {
                    _googleComboChart();
                }
            }
        }();


        // Initialize module
        // ------------------------------

        $(document).ready(function (){
            @if(count($orders) > 0)
            GoogleComboChart.init();
            @endif
        });

    </script>
@endsection
