<footer class="footer text-right">
    {{date('Y')}} <a href="https://panorama-q.com/" target="_blank"> © @lang('system.panorama_q')</a>
    @lang('system.for_p_solutions')
</footer>
