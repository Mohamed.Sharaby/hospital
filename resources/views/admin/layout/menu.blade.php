<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <div class="">

            <a href="{{route('homePage')}}" class="logo-wrapper">
                <img src="{{asset('admin/assets/images/logo2.png')}}" alt="@lang('system.system_image')">
            </a>
        </div>
        <!-- User -->
        <div class="user-box">
            <div class="user-img">
                @php $image = auth()->user()->image; @endphp
                @if($image != null or $image != "")
                    <img src="{{getimg($image)}}" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                    @else
                    <img src="{{asset('admin/assets/images/noimage.png')}}" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                    @endif

                {{--<div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>--}}
            </div>
            <h5 style="margin-top: 40px;"><a href="#">{{auth()->user()->name}}</a> </h5>
            <ul class="list-inline">
                <li>
                    <a href="{{route('user.get.profile')}}" >
                        <i class="zmdi zmdi-settings"></i>
                    </a>
                </li>

                <li>
                    <a href="#" class="text-custom" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="zmdi zmdi-power"></i>
                    </a>
                </li>
            </ul>
        </div>
        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST"
              style="display: none;">
            {{ csrf_field() }}
        </form>
        <!-- End User -->

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
                <li class="text-muted menu-title">@lang('system.menu')</li>

                @if(auth()->user()->role == 'super')

                    <li><a href="{{route('homePage')}}" class="waves-effect"><i class="zmdi zmdi-view-dashboard"></i><span>@lang('system.main')</span></a></li>
                    <li><a href="{{route('users.index')}}" class="waves-effect"><i class="zmdi zmdi-account-box-mail"></i><span>@lang('system.admins_menu')</span></a></li>
                    <li><a href="{{route('departments.index')}}" class="waves-effect"><i class="zmdi zmdi-view-agenda"></i><span>@lang('system.depts_menu')</span></a></li>
                    <li><a href="{{route('specializations.index')}}" class="waves-effect"><i class="zmdi zmdi-view-list-alt"></i><span>@lang('system.speci_menu')</span></a></li>
                    <li><a href="{{route('products.index')}}" class="waves-effect"><i class="zmdi zmdi-label"></i><span>@lang('system.products_menu')</span></a></li>
                    <li><a href="{{route('supplies.index')}}" class="waves-effect"><i class="zmdi zmdi-format-underlined"></i><span>@lang('system.supply_menu')</span></a></li>
                    <li><a href="{{route('exchanges.index')}}" class="waves-effect"><i class="zmdi zmdi-format-list-bulleted"></i><span>@lang('system.exchanges_menu')</span></a></li>
                    <li><a href="{{route('inventory.index')}}" class="waves-effect"><i class="zmdi zmdi-search-in-page"></i> <span>@lang('system.inventory_menu')</span></a></li>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-check-circle"></i><span>@lang('system.orders_menu')</span><span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{route('orders.chart')}}">{{__('Orders Status Chart')}}</a></li>
                            <li><a href="{{route('orders.create')}}">@lang('system.create_new_order')</a></li>
                            <li><a href="{{route('orders.index')}}?type=all">@lang('system.all_orders')</a></li>
                            <li><a href="{{route('orders.myOrders')}}">@lang('system.my_orders')</a></li>
                            <li><a href="{{route('orders.index')}}?type=refused">@lang('system.refused_orders')</a></li>
                            <li><a href="{{route('orders.index')}}?type=incompleted">@lang('system.not_completed_orders')</a></li>
                        </ul>
                    </li>
                    <li><a href="{{route('reports.orders')}}" class="waves-effect"><i class="zmdi zmdi-book"></i> <span>@lang('system.orders_reports')</span></a></li>
                    {{--<li class="has_sub">--}}
                        {{--<a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-book"></i><span>تقارير الطلبات</span><span class="menu-arrow"></span></a>--}}
                        {{--<ul class="list-unstyled">--}}
                            {{--<li><a href="{{route('reports.orders')}}">تقارير الطلبات(فلترة شاملة)</a></li>--}}
                            {{--<li><a href="#">تقرير طلبات فني معين</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}

                    @elseif(auth()->user()->role == 'coordinator')
                    <li><a href="{{route('homePage')}}" class="waves-effect"><i class="zmdi zmdi-view-dashboard"></i><span>@lang('system.main')</span></a></li>
                    {{--<li><a href="{{route('exchanges.index')}}" class="waves-effect"><i class="zmdi zmdi-format-list-bulleted"></i><span>أوامر الصرف</span></a></li>--}}
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-check-circle"></i><span>@lang('system.orders_menu')</span><span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
{{--                            <li><a href="{{route('orders.create')}}">@lang('system.create_new_order')</a></li>--}}
                            <li><a href="{{route('orders.index')}}?type=all">@lang('system.all_orders')</a></li>
{{--                            <li><a href="{{route('orders.myOrders')}}">@lang('system.my_orders')</a></li>--}}
                            <li><a href="{{route('orders.index')}}?type=refused">@lang('system.refused_orders')</a></li>
                            <li><a href="{{route('orders.index')}}?type=incompleted">@lang('system.not_completed_orders')</a></li>
                        </ul>
                    </li>
                    <li><a href="{{route('reports.orders')}}" class="waves-effect"><i class="zmdi zmdi-book"></i> <span>@lang('system.orders_reports')</span></a></li>


                @elseif(auth()->user()->role == 'dept_admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-check-circle"></i><span>@lang('system.orders_menu')</span><span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{route('orders.create')}}">@lang('system.create_new_order')</a></li>
                            {{--<li><a href="{{route('orders.index')}}?type=all">كل الطلبات</a></li>--}}
                            <li><a href="{{route('orders.myOrders')}}">@lang('system.my_orders')</a></li>
                            {{--<li><a href="{{route('orders.index')}}?type=refused">الطلبات المرفوضه</a></li>--}}
                        </ul>
                    </li>

                    @elseif(auth()->user()->role == 'technical')

                    <li><a href="{{route('exchanges.index')}}" class="waves-effect"><i class="zmdi zmdi-format-list-bulleted"></i><span>@lang('system.exchanges_menu')</span></a></li>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-check-circle"></i><span>@lang('system.orders_menu')</span><span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            {{--<li><a href="{{route('orders.create')}}">إنشاء طلب جديد</a></li>--}}
                            {{--<li><a href="{{route('orders.index')}}?type=all">كل الطلبات</a></li>--}}
                            <li><a href="{{route('orders.myOrders')}}">@lang('system.orders_menu')</a></li>
                            {{--<li><a href="{{route('orders.index')}}?type=refused">الطلبات المرفوضه</a></li>--}}
                        </ul>
                    </li>
                    <li><a href="{{route('reports.orders')}}" class="waves-effect"><i class="zmdi zmdi-book"></i> <span>@lang('system.orders_reports')</span></a></li>


                @elseif(auth()->user()->role == 'warehouse_admin')
                    <li><a href="{{route('products.index')}}" class="waves-effect"><i class="zmdi zmdi-label"></i><span>@lang('system.products_menu')</span></a></li>
                    <li><a href="{{route('supplies.index')}}" class="waves-effect"><i class="zmdi zmdi-format-underlined"></i><span>@lang('system.supply_menu')</span></a></li>
                    <li><a href="{{route('exchanges.index')}}" class="waves-effect"><i class="zmdi zmdi-format-list-bulleted"></i><span>@lang('system.exchanges_menu')</span></a></li>
                    <li><a href="{{route('reports.orders')}}" class="waves-effect"><i class="zmdi zmdi-book"></i> <span>@lang('system.orders_reports')</span></a></li>

                @endif



            </ul>
            <div class="clearfix"></div>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>

</div>
