@extends('admin.layout.master')
@section('title',__('system.notifications'))

@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">
                {{--<a href="{{route('orders.create')}}" class="btn btn-custom dropdown-toggle waves-effect waves-light">--}}
                {{--إنشاء طلب جديد--}}
                {{--<span class="m-l-5"><i class="fa fa-plus"></i></span>--}}
                {{--</a>--}}
            </div>

            <h4 class="page-title">@lang('system.notifications')</h4>
        </div>
    </div><!--End Page-Title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <h4 class="header-title m-t-0 m-b-30">@lang('system.all_notifications')</h4>

                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="width: 50px;">@lang('system.No')</th>
                        <th style="width: 150px;">@lang('system.address')</th>
                        <th style="width: 800px;">@lang('system.notify_body')</th>
                        <th>@lang('system.options')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $i = 1; @endphp
                        @forelse($notifications as $notify)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$notify->title}}</td>
                            <td>{{$notify->body}}</td>
                            <td>
                                <a  id="elementRow{{$notify->id}}" href="javascript:;" data-id="{{$notify->id}}" data-url="{{route('notification.delete')}}" class="removeElement label label-danger">حذف</a>
                            </td>
                        </tr>
                    @empty
                            <tr>
                                <td></td>
                                <td>@lang('system.no_notifications')</td>
                                <td></td>
                                <td></td>
                            </tr>
                    @endforelse
                    </tbody>
                </table>

            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>

        $('body').on('click', '.removeElement', function () {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var tr = $(this).closest($('#elementRow' + id).parent().parent());

            swal({
                    title: "@lang('system.r_u_sure')",
                    text: '@lang('system.sure_delete_notify')',
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#27dd24",
                    confirmButtonText: "@lang('system.accept')",
                    cancelButtonText: "@lang('system.cancel')",
                    confirmButtonClass:"btn-danger waves-effect waves-light",
                    closeOnConfirm: true,
                    closeOnCancel: true,
                },
                function (isConfirm) {
                    if(isConfirm){
                        $.ajax({
                            type:'post',
                            url :url,
                            data:{id:id},
                            dataType:'json',
                            success:function(data){
                                if(data.status == true){
                                    var title = data.title;
                                    var msg = data.message;
                                    toastr.options = {
                                        positionClass : 'toast-top-left',
                                        onclick:null
                                    };

                                    var $toast = toastr['success'](msg,title);
                                    $toastlast = $toast;

                                    tr.find('td').fadeOut(1000, function () {
                                        tr.remove();
                                    });
                                }else {
                                    var title = data.title;
                                    var msg = data.message;
                                    toastr.options = {
                                        positionClass : 'toast-top-left',
                                        onclick:null
                                    };

                                    var $toast = toastr['error'](msg,title);
                                    $toastlast = $toast
                                }
                            }
                        });
                    }
                }
            );
        });

    </script>

@endsection
