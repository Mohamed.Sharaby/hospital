@extends('admin.layout.master')
@section('title',__('system.create_new_order'))

@section('styles')
    <style>
        .help-block{
            color: red;
        }
    </style>
@endsection



@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">
                {{--<a href="{{route('orders.index')}}" class="btn btn-custom dropdown-toggle waves-effect waves-light" >رجوع لقائمة الطلبات<span class="m-l-5"><i class="fa fa-reply"></i></span></a>--}}
            </div>
            <h4 class="page-title">@lang('system.create_new_order')</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30">@lang('system.order_data')</h4>

                <div class="row">
                    <form method="post" action="{{route('orders.store')}}" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('system.order_name')</label>
                                <div class="col-md-10">
                                    <input type="text" required value="{{old('name')}}"
                                           autocomplete="off"
                                           data-parsley-required-message="@lang('system.field_required')"
                                           data-parsley-trigger="keyup"
                                           data-parsley-maxlength="191"
                                           data-parsley-maxlength-message="@lang('system.max_char_191')"
                                           name="name" class="form-control" placeholder="@lang('system.order_name')">

                                    @if($errors->has('name'))
                                        <p class="help-block">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('system.department')</label>
                                <div class="col-md-10">

                                    @if(auth()->user()->role !='dept_admin')
                                    <select  required data-parsley-required-message="@lang('system.should_choose_dept')"
                                            class="form-control" name="dept_id">
                                            <option value="">@lang('system.choose_dept')</option>
                                        @foreach($departments as $dept)
                                            <option value="{{$dept->id}}" > {{$dept->value}}</option>
                                        @endforeach
                                    </select>
                                    @else
                                        <select  required data-parsley-required-message="@lang('system.should_choose_dept')"
                                                 class="form-control" name="dept_id">
                                                <option value="{{auth()->user()->dept_id}}" > {{auth()->user()->department->name}}</option>
                                        </select>
                                        {{--@if(auth()->user()->dept_id == $dept->id) selected @endif--}}
                                        @endif
                                    {{--<input type="number" required value="{{old('qty')}}"--}}
                                           {{--data-parsley-required-message="هذا الحقل مطلوب"--}}
                                           {{--autocomplete="off"--}}
                                           {{--data-parsley-trigger="keyup"--}}
                                           {{--data-parsley-min="1"--}}
                                           {{--data-parsley-min-message="أقل كمية هي 1"--}}
                                           {{--name="qty" class="form-control" placeholder="الكمية">--}}

                                    @if($errors->has('dept_id'))
                                        <p class="help-block">
                                            {{ $errors->first('dept_id') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('system.order_image')</label>
                                <div class="col-md-10">
                                    <input  name="image" type="file" class="dropify" data-max-file-size="6M"
                                           data-allowed-file-extensions="png gif jpg jpeg"
                                           data-errors-position="inside"
                                    />
                                </div>
                            </div>
                        </div>
                        @if($errors->has('image'))
                            <p class="help-block">
                                {{ $errors->first('image') }}
                            </p>
                        @endif



                        {{-- buttons --}}
                        <div class="col-lg-12">
                            <div class="form-group text-right m-t-20">
                                <button class="btn btn-primary waves-effect waves-light m-t-20" id="btnSubmit" type="submit">
                                    @lang('system.create')
                                </button>
                            </div>
                        </div>
                    </form>


                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>
@endsection

@section('scripts')

    <script>
        $('.dropify').dropify({
            messages: {
                'default': "@lang('system.dropify_1')",
                'replace': "@lang('system.dropify_2')",
                'remove': "@lang('system.delete')",
                'error': "@lang('system.somethingWentWrong')"
            },
            error: {
                'fileSize': "@lang('system.max_size_image')",
                'fileExtension': "@lang('system.image_type')",
            }
        });
    </script>
@endsection

