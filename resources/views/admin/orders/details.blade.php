@extends('admin.layout.master')
@section('styles')
    <!-- Custom box css -->
    <link href="{{asset('admin/assets/plugins/custombox/dist/custombox.min.css')}}" rel="stylesheet">

@endsection
@section('content')
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">
                @if(auth()->user()->role == 'super' || auth()->user()->role == 'coordinator')
                    <a href="{{route('orders.index')}}"
                       class="no-print btn btn-custom dropdown-toggle waves-effect waves-light">
                        @lang('system.back')
                        <span class="m-l-5"><i class="fa fa-arrow-left"></i></span>
                    </a>

                @else
                    <a href="{{route('orders.myOrders')}}"
                       class="no-print btn btn-custom dropdown-toggle waves-effect waves-light">
                        @lang('system.back')
                        <span class="m-l-5"><i class="fa fa-arrow-left"></i></span>
                    </a>
                @endif
            </div>

            <h4 class="page-title"> @lang('system.order_details') : {{$order->name}}</h4>
        </div>
    </div><!--End Page-Title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div class="row">
                    <div class="btn-group">
                        <form>
                            <input type="button" class="btn btn-warning" value="{{__('Print')}}"
                                   onclick="window.print()"/>
                        </form>
                    </div>
                    <div class="col-sm-12">

                        <div class="col-md-4">
                            <h4>@lang('system.order_name')</h4>
                            <h5 style="font-weight: 600;">{{$order->name}}</h5>
                        </div>

                        <div class="col-md-4">
                            <h4>@lang('system.order_number')</h4>
                            <h5 style="font-weight: 600;">{{$order->id}}</h5>
                        </div>

                        <div class="col-md-4">
                            <h4>@lang('system.order_date')</h4>
                            <h5 style="font-weight: 600;">{{$order->created_at}}</h5>
                        </div>

                        <div class="col-md-4">
                            <h4>@lang('system.followed_dept'):</h4>
                            <h5 style="font-weight: 600;">{{$order->department->value}}</h5>
                        </div>

                        <div class="col-md-4">
                            <h4>@lang('system.order_owner'):</h4>
                            <h5 style="font-weight: 600;">{{$order->user->name}}</h5>
                        </div>

                        <div class="col-md-4">
                            <h4>@lang('system.order_status')</h4>
                            <h5 style="font-weight: 600;">
                                @switch($order->status)
                                    @case('new')
                                    <button type="button"
                                            class="btn btn-info btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.new')</button>                        @break
                                    @case('pending')
                                    <button type="button"
                                            class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.wait_technical_approval')</button>   @break
                                    @case('accepted')
                                    <button type="button"
                                            class="btn btn-warning btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.technical_approval')</button>
                                    @if(isset($order->exchange))
                                        <button type="button"
                                                class="btn btn-purple btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.exist_exchange')</button>
                                    @endif
                                    @break
                                    @case('completed')
                                    <button type="button"
                                            class="btn btn-purple  btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.completed_with_note')</button>              @break
                                    @case('finished')
                                    <button type="button"
                                            class="btn btn-success btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.completed')</button>              @break
                                    @case('not_completed')
                                    <button type="button"
                                            class="btn btn-danger btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.not_completed')</button>       @break
                                    @case('refused')
                                    <button type="button"
                                            class="btn btn-danger btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.refused')</button>                  @break
                                @endswitch
                            </h5>
                        </div>

                        {{--@if($order->status == 'refused')--}}
                        {{--<div class="col-md-4">--}}
                        {{--<h4>سبب الرفض</h4>--}}
                        {{--<h5 style="font-weight: 600;">{{$order->refuse_reason}}</h5>--}}
                        {{--</div>--}}
                        {{--@endif--}}

                        <div class="col-md-4">
                            <h4>@lang('system.owner_technical')</h4>
                            @if($order->technical != null) <h5
                                style="font-weight: 600;">{{optional($order->technical)->name}}</h5>
                            @else <h5 style="font-weight: 600;">@lang('system.no_technical_rightnow')</h5>
                            @endif

                        </div>

                        @if(auth()->user()->role == 'technical')
                            <div class="col-md-4">
                                <h4>اسم الفنى الذى يقوم بحل المشكلة</h4>
                                @if($order->tech_name == null)
                                    <div class="row">
                                        <form action="{{route('orders.techName')}}" method="post">
                                            @csrf
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input type="text" name="tech_name" class="form-control"
                                                           placeholder="ادخل اسم الفنى ">
                                                    <input type="hidden" name="orderId" value="{{$order->id}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-info">حفظ</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                @else
                                    <h5
                                        style="font-weight: 600;">{{$order->tech_name}}</h5>
                                @endif
                            </div>
                        @endif

                        <div class="col-md-4">
                            <h4>@lang('system.order_image')</h4>
                            <div style="width: 200px; height: 150px;">
                                @if($order->image)
                                    <a href="{{getimg($order->image)}}" class="image-popup" title="Screenshot-1">
                                        <img width="200" height="150" src="{{getimg($order->image)}}" class="thumb-img"
                                             alt="work-thumbnail">
                                    </a>
                                @else
                                    <a href="{{asset('admin/assets/images/noimage.png')}}" class=" image-popup"
                                       title="Screenshot-1">
                                        <img width="200" height="150" src="{{asset('admin/assets/images/noimage.png')}}"
                                             class="thumb-img" alt="work-thumbnail">
                                    </a>
                                @endif
                            </div>
                        </div>


                        <div class="col-md-4">
                            <h4>@lang('system.create_exchange')</h4>
                            <a href="{{route('exchange.fast.create',$order->id)}}" target="_blank"
                               class="btn btn-primary waves-effect waves-light btn-lg m-b-5">@lang('system.create')</a>
                        </div>


                        @php $roleOfUser =auth()->user()->role;  @endphp
                        <div class="col-md-6">
                            <h4>@lang('system.change_order_status')</h4>
                            <h5 style="font-weight: 600;">

                                @if(in_array($order->status,['new']))
                                    @if(auth()->user()->role == 'super' || auth()->user()->role == 'coordinator')
                                        <a href="javascript:;" data-action="addTech"
                                           class="changeOrderStatus btn btn-info btn-bordred w-md waves-effect waves-light m-b-5">@lang('system.change_to_technical')</a>
                                    @else
                                        <button type="button"
                                                class="btn btn-inverse btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.wait_technical_approval')</button>
                                    @endif

                                @endif

                                @if(in_array($order->status,['pending']))
                                    @if($order->technical_id == auth()->id() || $roleOfUser == 'super')
                                        <a href="javascript:;" data-url="{{route('orders.change.status')}}"
                                           data-value="{{$order->id}}" data-action="accept"
                                           class="changeOrderStatus btn btn-success btn-bordred w-md waves-effect waves-light m-b-5">@lang('system.accept_order')</a>
                                        <a href="javascript:;" data-url="{{route('orders.change.status')}}"
                                           data-value="{{$order->id}}" data-action="refuse"
                                           class="changeOrderStatus btn btn-danger btn-bordred w-md waves-effect waves-light m-b-5">@lang('system.refuse_with_reason')</a>
                                    @else
                                        <button type="button"
                                                class="btn btn-inverse btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.has_no_permission_to_change_order_status')</button>
                                    @endif
                                @endif

                                @if(in_array($order->status,['accepted']))
                                    @if($order->technical_id == auth()->id() || $roleOfUser == 'super')
                                        <a href="javascript:;" data-url="{{route('orders.change.status')}}"
                                           data-value="{{$order->id}}" data-action="complete"
                                           class="changeOrderStatus btn btn-purple btn-bordred w-md waves-effect waves-light m-b-5">@lang('system.finish_order_work')</a>
                                    @else
                                        <button type="button"
                                                class="btn btn-inverse btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.has_no_permission_to_change_order_status')</button>
                                    @endif
                                @endif

                                @if(in_array($order->status,['completed']))
                                    {{--@if(auth()->user()->role == 'super')--}}
                                    {{--<a href="javascript:;" data-url="{{route('orders.change.status')}}" data-value="{{$order->id}}" data-action="finish"  class="changeOrderStatus btn btn-success btn-bordred w-md waves-effect waves-light m-b-5">إكتمل الطلب بنجاح</a>--}}
                                    {{--@endif--}}
                                    @if( ( auth()->user()->role == 'super' ) || (auth()->user()->role == 'dept_admin' && $order->dept_id == auth()->user()->dept_id)  )
                                        <a href="javascript:;" data-url="{{route('orders.change.status')}}"
                                           data-value="{{$order->id}}" data-action="finish"
                                           class="changeOrderStatus btn btn-success btn-bordred w-md waves-effect waves-light m-b-5">@lang('system.order_completed_successfully')</a>
                                        <a href="javascript:;" data-url="{{route('orders.change.status')}}"
                                           data-value="{{$order->id}}" data-action="not_completed"
                                           class="changeOrderStatus btn btn-danger btn-bordred w-md waves-effect waves-light m-b-5">@lang('system.no_approval')</a>
                                    @else
                                        <button type="button"
                                                class="btn btn-inverse btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.has_no_permission_to_change_order_status')</button>
                                    @endif
                                @endif

                                @if(in_array($order->status,['finished']))
                                    <a href="javascript:;"
                                       class="btn btn-success btn-bordred w-md waves-effect waves-light m-b-5">@lang('system.order_finishing_done')</a>
                                @endif

                                @if(in_array($order->status,['not_completed']))
                                    {{--<a href="javascript:;"  class="btn btn-danger btn-bordred w-md waves-effect waves-light m-b-5">الطلب لم يكتمل</a>--}}

                                    @if(auth()->user()->role == 'super' || auth()->user()->role == 'coordinator')
                                        <a href="javascript:;" data-action="addTech"
                                           class="changeOrderStatus btn btn-info btn-bordred w-md waves-effect waves-light m-b-5">@lang('system.reversal_to_technical')</a>
                                    @else
                                        <button type="button"
                                                class="btn btn-inverse btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.wait_for_reversal_to_technical')</button>
                                    @endif
                                @endif

                                @if(in_array($order->status,['refused']))

                                    @if(auth()->user()->role == 'super' || auth()->user()->role == 'coordinator')
                                        <a href="javascript:;" data-action="addTech"
                                           class="changeOrderStatus btn btn-info btn-bordred w-md waves-effect waves-light m-b-5">@lang('system.reversal_to_technical')</a>
                                    @else
                                        <button type="button"
                                                class="btn btn-inverse btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.wait_for_reversal_to_technical')</button>
                                    @endif

                                @endif
                            </h5>
                        </div>
                    </div>
                </div> <!--End of row-->

            {{--****************************           Modals                ************************************ --}}
            <!-- Add Technical to Order ...  Modal -->
                <div class="modal fade" id="technical_modal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 id="modal_header" class="modal-title">@lang('system.assign_order_to_technical')</h4>
                            </div>
                            <form id="addTechForm" data-parsley-validate novalidate method="post"
                                  action="{{route('orders.add.technical')}}">
                                <div class="modal-body">
                                    <label for="reason">@lang('system.choose_technical')</label>

                                    {{csrf_field()}}
                                    <select required data-parsley-required-message="@lang('system.technical_required')"
                                            class="form-control" name="tech_id">
                                        @foreach($technicals as $tech)
                                            <option value="{{$tech->id}}">{{$tech->name}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="order_id" value="{{$order->id}}">

                                </div>
                                <div class="modal-footer">
                                    <button id="technical_ok" type="submit"
                                            class="btn btn-success">@lang('system.send')</button>
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">@lang('system.cancel')</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>

                <!-- Reason of Refuse  ...  Modal -->
                <div class="modal fade" id="refuse_modal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 id="modal_header" class="modal-title">@lang('system.refuse_reason')</h4>
                            </div>
                            <form id="refuseReasonForm" data-parsley-validate novalidate method="post"
                                  action="{{route('orders.change.status')}}">
                                <div class="modal-body">
                                    <label for="reason">@lang('system.enter_refuse_reason')</label>
                                    {{csrf_field()}}
                                    <textarea class="form-control" data-parsely-max="191"
                                              data-parsley-max-message="@lang('system.max_char_191')"
                                              name="refuse_reason" placeholder="@lang('system.enter_refuse_reason')"
                                              required
                                              data-parsley-required-message="@lang('system.refuse_required')"></textarea>
                                    <input type="hidden" name="order_id" value="{{$order->id}}">
                                    <input type="hidden" name="status" value="refuse">

                                </div>
                                <div class="modal-footer">
                                    <button id="refuse_ok" type="submit"
                                            class="btn btn-success">@lang('system.send')</button>
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">@lang('system.cancel')</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>

                <!-- Reason of Non-Compilation  ...  Modal -->
                <div class="modal fade" id="not_completed_modal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 id="modal_header"
                                    class="modal-title">@lang('system.order_not_completed_request')</h4>
                            </div>
                            <form id="notCompletedForm" data-parsley-validate novalidate method="post"
                                  action="{{route('orders.change.status')}}">
                                <div class="modal-body">
                                    <label for="reason">@lang('system.enter_not_complete_reason')</label>
                                    {{csrf_field()}}
                                    <textarea class="form-control" data-parsely-max="191"
                                              data-parsley-max-message="@lang('system.max_char_191')"
                                              name="in_complete_reason"
                                              placeholder="@lang('system.enter_not_complete_reason')" required
                                              data-parsley-required-message="@lang('system.field_required')"></textarea>

                                    <input name="image" type="file" class="dropify" data-max-file-size="6M"
                                           data-allowed-file-extensions="png gif jpg jpeg"
                                           data-errors-position="inside"
                                    />

                                    <input type="hidden" name="order_id" value="{{$order->id}}">
                                    <input type="hidden" name="status" value="not_completed">

                                </div>
                                <div class="modal-footer">
                                    <button id="not_completed_ok" type="submit" class="btn btn-success">إرسال</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">إلغاء</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>

                <!-- Exchange Orders -->
                {{--                <!-- Modal -->--}}
                {{--                <div id="custom-modal" class="modal-demo">--}}
                {{--                    <button type="button" class="close" onclick="Custombox.close();">--}}
                {{--                        <span>&times;</span><span class="sr-only">Close</span>--}}
                {{--                    </button>--}}
                {{--                    <h4 class="custom-modal-title">Modal title</h4>--}}
                {{--                    <div class="custom-modal-text">--}}
                {{--                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.--}}
                {{--                    </div>--}}
                {{--                </div>--}}


            </div>
        </div>
    </div>

    {{--****************************         End Modals         ************************************ --}}


    {{--********************** Refuses Section ************************* --}}

    @if($order->refuses->count() >0)
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <h4>@lang('order_refuse_reasons')</h4>

                    <br>
                    <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>@lang('system.No')</th>
                            <th>@lang('system.technical_name')</th>
                            <th>@lang('system.refuse_reason')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $i = 1; @endphp
                        @foreach($order->refuses as $refuse)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$refuse->user->name}}</td>
                                <td>{{$refuse->refuse_reason}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>

        {{--@else--}}
        {{--<div class="row">--}}
        {{--<div class="col-sm-12">--}}
        {{--<div class="card-box table-responsive">--}}
        {{--<h3>لم يتم تعيين أمر صرف</h3>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
    @endif

    {{--********************** End  Refuses Section ************************* --}}


    {{--********************** InCompleted Section ************************* --}}

    @if($order->notCompleteds->count() >0)
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <h4>@lang('system.order_refuse_reasons')</h4>

                    <br>
                    <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>@lang('system.No')</th>
                            <th>@lang('system.not_complete_request_user')</th>
                            <th>@lang('system.not_complete_reason')</th>
                            <th>@lang('system.the_image')</th>
                        </tr>
                        </thead>
                        <tbody>

                        @php $i = 1; @endphp
                        @foreach($order->notCompleteds as $reason)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$reason->user->name}}</td>
                                <td>{{$reason->in_complete_reason}}</td>
                                <td style="width: 20%;">
                                    @if($reason->image != "")
                                        <a data-fancybox="gallery"

                                           href="{{ getimg($reason->image) }}">
                                            <img style="width: 50%; border-radius: 50%; height: 49px;"
                                                 src="{{ getimg($reason->image) }}"/>
                                        </a>

                                    @else

                                    @endif

                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>

        {{--@else--}}
        {{--<div class="row">--}}
        {{--<div class="col-sm-12">--}}
        {{--<div class="card-box table-responsive">--}}
        {{--<h3>لم يتم تعيين أمر صرف</h3>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
    @endif

    {{--********************** End  InCompleted Section ************************* --}}




    {{--********************** Exchanges Section ************************* --}}

    @if(isset($order->exchange))
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <h4>@lang('system.ex_details')</h4>

                    <div class="col-md-4">
                        <h4>@lang('system.ex_no'):</h4>
                        <h5 style="font-weight: 600;">{{$order->exchange->id}}</h5>
                    </div>

                    <div class="col-md-4">
                        <h4>@lang('system.technical_name')</h4>
                        <h5 style="font-weight: 600;">{{$order->exchange->technical->name}}</h5>
                    </div>

                    <div class="col-md-4">
                        <h4>@lang('system.ex_status')</h4>
                        <h5 style="font-weight: 600;">
                            @if($order->exchange->status == 'wait')
                                <button type="button"
                                        class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.wait')</button>
                            @elseif($order->exchange->status == 'accepted')
                                <button type="button"
                                        class="btn btn-success btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.accepted')</button>
                            @else
                                <button type="button"
                                        class="btn btn-danger btn-rounded w-md waves-effect waves-light m-b-5">@lang('system.refused')</button>
                            @endif
                        </h5>
                    </div>

                    {{--@if($order->exchange->status == 'refused')--}}
                    {{--<div class="col-md-4">--}}
                    {{--<h4 style="color: red;"> سبب الرفض:</h4>--}}
                    {{--<h5 style="font-weight: 600; color: red;">{{$order->exchange->refuse_reason}}</h5>--}}
                    {{--</div>--}}
                    {{--@endif--}}

                    <div class="col-md-8">
                        <h4>@lang('system.ex_details')</h4>
                        <h5 style="font-weight: 600;">{{$order->exchange->description}}</h5>
                    </div>

                    <div class="col-md-4">
                        <h4> @lang('system.exchange_date')</h4>
                        <h5 style="font-weight: 600;">{{$order->created_at}}</h5>
                    </div>

                    <br>
                    <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>@lang('system.No')</th>
                            <th>@lang('system.product')</th>
                            <th>@lang('system.qty')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $i = 1; @endphp
                        @foreach($order->exchange->exchange_details as $row)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$row->product->name}}</td>
                                <td>{{$row->qty}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    @else
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <h3>@lang('system.no_ex_made')</h3>
                </div>
            </div>
        </div>
    @endif

    {{--**********************End  Exchanges Section ************************* --}}



@endsection

@section('scripts')
    <!-- Modal-Effect -->
    <script src="{{asset('admin/assets/plugins/custombox/dist/custombox.min.js')}}"></script>
    <script src="{{asset('admin/assets/plugins/custombox/dist/legacy.min.js')}}"></script>

    <script>
        $('.dropify').dropify({
            messages: {
                'default': "@lang('system.dropify_1')",
                'replace': "@lang('system.dropify_2')",
                'remove': "@lang('system.delete')",
                'error': "@lang('system.somethingWentWrong')"
            },
            error: {
                'fileSize': "@lang('system.max_size_image')",
                'fileExtension': "@lang('system.image_type')",
            }
        });
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $('body').on('click', '.changeOrderStatus', function () {
            var action = $(this).attr('data-action');
            console.log(action);

            var order_id = $(this).attr('data-value');
            console.log(order_id);
            var url = $(this).attr('data-url');
            console.log(url);

            var text = '';
            var type = ''; // error or success
            var confirmButtonClass = ''; //'btn-success waves-effect waves-light'

            if (action === 'addTech') {
                text = '@lang('system.assign_to_technical')';
                type = 'success';
                confirmButtonClass = 'btn-success waves-effect waves-light';
            }
            if (action === 'accept') {
                text = '@lang('system.accept_the_order')';
                type = 'success';
                confirmButtonClass = 'btn-success waves-effect waves-light';
            }
            if (action === 'refuse') {
                text = '@lang('system.sure_refuse_order')';
                type = 'error';
                confirmButtonClass = 'btn-danger waves-effect waves-light';
            }
            if (action === 'acceptWithExchange') {
                text = '@lang('system.accept_the_ex')';
                type = 'success';
                confirmButtonClass = 'btn-success waves-effect waves-light';
            }
            if (action === 'complete') {
                text = '@lang('system.finish_the_work')';
                type = 'success';
                confirmButtonClass = 'btn-success waves-effect waves-light';
            }

            if (action === 'not_completed') {
                text = '@lang('system.work_not_completed')';
                type = 'error';
                confirmButtonClass = 'btn-danger waves-effect waves-light';
            }

            if (action === 'finish') {
                text = '@lang('system.work_is_finished')';
                type = 'success';
                confirmButtonClass = 'btn-success waves-effect waves-light';
            }
//* ***********************************************************
            swal({
                    title: "@lang('system.r_u_sure')",
                    text: text,
                    type: type,
                    showCancelButton: true,
                    confirmButtonColor: "#27dd24",
                    confirmButtonText: "@lang('system.accept')",
                    cancelButtonText: "@lang('system.cancel')",
                    confirmButtonClass: confirmButtonClass,
                    closeOnConfirm: true,
                    closeOnCancel: true,
                },
                function (isConfirm) {
                    if (isConfirm) {

                        if (action === 'addTech') {
                            $('#technical_modal').modal('show');
                        }

                        if (action === 'accept') {
                            $.ajax({
                                type: 'post',
                                url: url,
                                data: {order_id: order_id, status: action},
                                dataType: 'json',

                                success: function (data) {
                                    if (data.status === true) {
                                        var title = data.title;
                                        var msg = data.message;
                                        toastr.options = {
                                            positionClass: 'toast-top-left',
                                            onclick: null
                                        };
                                        var $toast = toastr['success'](msg, title);
                                        $toastlast = $toast;

                                        setTimeout(function () {
                                            window.location.href = "";
                                        }, 3000);

                                    } else {
                                        var title = data.title;
                                        var msg = data.message;
                                        toastr.options = {
                                            positionClass: 'toast-top-left',
                                            onclick: null
                                        };
                                        var $toast = toastr['error'](msg, title);
                                        $toastlast = $toast;

                                    }
                                },
                                error: function (data) {

                                }
                            });
                        }

                        if (action === 'refuse') {
                            $('#refuse_modal').modal('show');
                        }

                        if (action === 'not_completed') {
                            $('#not_completed_modal').modal('show');
                        }

                        if (action == 'acceptWithExchange') {

                            $.ajax({
                                type: 'post',
                                url: url,
                                data: {order_id: order_id, status: action},
                                dataType: 'json',

                                success: function (data) {
                                    if (data.status === true) {
                                        var title = data.title;
                                        var msg = data.message;
                                        toastr.options = {
                                            positionClass: 'toast-top-left',
                                            onclick: null
                                        };
                                        var $toast = toastr['success'](msg, title);
                                        $toastlast = $toast;

                                        setTimeout(function () {
                                            window.location.href = "";
                                        }, 3000);

                                    } else {
                                        var title = data.title;
                                        var msg = data.message;
                                        toastr.options = {
                                            positionClass: 'toast-top-left',
                                            onclick: null
                                        };
                                        var $toast = toastr['error'](msg, title);
                                        $toastlast = $toast;

                                    }
                                },
                                error: function (data) {

                                }
                            });

                        }

                        if (action == 'complete') {

                            $.ajax({
                                type: 'post',
                                url: url,
                                data: {order_id: order_id, status: action},
                                dataType: 'json',

                                success: function (data) {
                                    if (data.status === true) {
                                        var title = data.title;
                                        var msg = data.message;
                                        toastr.options = {
                                            positionClass: 'toast-top-left',
                                            onclick: null
                                        };
                                        var $toast = toastr['success'](msg, title);
                                        $toastlast = $toast;

                                        setTimeout(function () {
                                            window.location.href = "";
                                        }, 3000);

                                    } else {
                                        var title = data.title;
                                        var msg = data.message;
                                        toastr.options = {
                                            positionClass: 'toast-top-left',
                                            onclick: null
                                        };
                                        var $toast = toastr['error'](msg, title);
                                        $toastlast = $toast;

                                    }
                                },
                                error: function (data) {

                                }
                            });
                        }

                        if (action == 'finish') {
                            $.ajax({
                                type: 'post',
                                url: url,
                                data: {order_id: order_id, status: action},
                                dataType: 'json',

                                success: function (data) {
                                    if (data.status === true) {
                                        var title = data.title;
                                        var msg = data.message;
                                        toastr.options = {
                                            positionClass: 'toast-top-left',
                                            onclick: null
                                        };
                                        var $toast = toastr['success'](msg, title);
                                        $toastlast = $toast;

                                        setTimeout(function () {
                                            window.location.href = "";
                                        }, 3000);

                                    } else {
                                        var title = data.title;
                                        var msg = data.message;
                                        toastr.options = {
                                            positionClass: 'toast-top-left',
                                            onclick: null
                                        };
                                        var $toast = toastr['error'](msg, title);
                                        $toastlast = $toast;

                                    }
                                },
                                error: function (data) {

                                }
                            });

                        }


                    }
                }
            );
        });
    </script>


    <script>

        $('#addTechForm').on('submit', function (e) {
            e.preventDefault();
            var form = $(this);
            form.parsley().validate();
            if (form.parsley().isValid()) {
                var formData = new FormData(this);

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        if (data.status === true) {
                            var title = data.title;
                            var msg = data.message;
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr['success'](msg, title);
                            $toastlast = $toast;

                            setTimeout(function () {
                                window.location.href = "";
                            }, 3000);

                            $('#technical_modal').modal('hide');
                        } else {
                            var title = data.title;
                            var msg = data.message;
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr['error'](msg, title);
                            $toastlast = $toast;

                            $('#technical_modal').modal('hide');

                        }
                    },
                    error: function (data) {

                    }
                });
            }
        });


        $('#refuseReasonForm').on('submit', function (e) {
            e.preventDefault();
            var form = $(this);
            form.parsley().validate();
            if (form.parsley().isValid()) {
                var formData = new FormData(this);

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        if (data.status === true) {
                            var title = data.title;
                            var msg = data.message;
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr['success'](msg, title);
                            $toastlast = $toast;

                            setTimeout(function () {
                                window.location.href = "";
                            }, 3000);

                            $('#refuse_modal').modal('hide');
                        } else {
                            var title = data.title;
                            var msg = data.message;
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr['error'](msg, title);
                            $toastlast = $toast;

                            $('#refuse_modal').modal('hide');

                        }
                    },
                    error: function (data) {

                    }
                });
            }
        });

        $('#notCompletedForm').on('submit', function (e) {
            e.preventDefault();
            var form = $(this);
            form.parsley().validate();
            if (form.parsley().isValid()) {
                var formData = new FormData(this);

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        if (data.status === true) {
                            var title = data.title;
                            var msg = data.message;
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr['success'](msg, title);
                            $toastlast = $toast;

                            setTimeout(function () {
                                window.location.href = "";
                            }, 3000);

                            $('#not_completed_modal').modal('hide');
                        } else {
                            var title = data.title;
                            var msg = data.message;
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr['error'](msg, title);
                            $toastlast = $toast;

                            $('#not_completed_modal').modal('hide');

                        }
                    },
                    error: function (data) {

                    }
                });
            }
        });


    </script>
@endsection
