@extends('admin.layout.master')
@section('title',__('system.my_orders'))



@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">

            <div class="btn-group pull-right m-t-15">
                {{--<a href="{{route('orders.create')}}" class="btn btn-custom dropdown-toggle waves-effect waves-light">--}}
                {{--إنشاء طلب جديد--}}
                {{--<span class="m-l-5"><i class="fa fa-plus"></i></span>--}}
                {{--</a>--}}

            </div>

            <h4 class="page-title">@lang('system.your_own_orders')</h4>
        </div>
    </div><!--End Page-Title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <h4 class="header-title m-t-0 m-b-30">@lang('system.all_orders')</h4>

                <table id="datatable-responsive2" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>@lang('system.order_NO')</th>
                        <th>@lang('system.order_date')</th>
                        <th>@lang('system.order_name')</th>
                        <th>@lang('system.followed_dept')</th>
                        <th>@lang('system.status')</th>
                        <th style="width: 250px;" >@lang('system.options')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $i = 1; @endphp
                    @foreach($orders as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->created_at}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->department->value}}</td>
                            <td>
                                @switch($row->status)
                                @case('new')        <label  class="label label-info label-rounded w-md waves-effect waves-light m-b-5">@lang('system.new')</label>         @break
                                @case('pending')    <label  class="label label-primary label-rounded w-md waves-effect waves-light m-b-5">@lang('system.wait_technical_approval')</label>   @break
                                @case('accepted')
                                <label  class="label label-warning label-rounded w-md waves-effect waves-light m-b-5">@lang('system.technical_approval')</label>
                                @if(isset($row->exchange))
                                    <label  class="label label-purple label-rounded w-md waves-effect waves-light m-b-5">@lang('system.exist_exchange')</label>
                                @endif
                                @break
                                @case('completed')  <label  class="label label-purple  label-rounded w-md waves-effect waves-light m-b-5">@lang('system.completed_with_note')</label>               @break
                                @case('finished')   <label  class="label label-success label-rounded w-md waves-effect waves-light m-b-5">@lang('system.completed')</label>              @break
                                @case('refused')    <label  class="label label-danger label-rounded w-md waves-effect waves-light m-b-5">@lang('system.refused')</label>                  @break
                                @endswitch
                            </td>
                            <td>
                                <a href="{{route('orders.show',$row->id)}}" class="label label-primary">@lang('system.details')</a>
                            </td>
                            {{--<td>--}}

                            {{--<a href="{{route('products.edit',$row->id)}}" class="label label-warning">تعديل</a>--}}
                            {{--<a  id="elementRow{{$row->id}}" href="javascript:;" data-id="{{$row->id}}" data-url="{{route('products.destroy',$row->id)}}" class="removeElement label label-danger">حذف</a>--}}
                            {{--</td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function (){
            $('#datatable-responsive2').DataTable({
                "order": [[ 1, "desc" ]],
                columnDefs: [{orderable: false, targets: [0]}],
                "language": {
                    "lengthMenu": "@lang('system.show') _MENU_ @lang('system.perpage')",
                    "info": "@lang('system.show') @lang('system.perpage') _PAGE_ @lang('system.from')_PAGES_",
                    "infoEmpty": "@lang('system.no_data_available')",
                    "infoFiltered": "(@lang('system.filter_from_max_total') _MAX_)",
                    "paginate": {
                        "first": "@lang('system.first')",
                        "last": "@lang('system.last')",
                        "next": "@lang('system.next')",
                        "previous": "@lang('system.previous')"
                    },
                    "search": "@lang('system.search'):",
                    "zeroRecords": "@lang('system.no_search_result')",
                },

            });
        });


        $('body').on('click', '.removeElement', function () {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var tr = $(this).closest($('#elementRow' + id).parent().parent());

            swal({
                    title: "@lang('system.r_u_sure')",
                    text: "@lang('system.sure_delete_order')",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#27dd24",
                    confirmButtonText: "@lang('system.accept')",
                    cancelButtonText: "@lang('system.cancel')",
                    confirmButtonClass:"btn-danger waves-effect waves-light",
                    closeOnConfirm: true,
                    closeOnCancel: true,
                },
                function (isConfirm) {
                    if(isConfirm){
                        $.ajax({
                            type:'delete',
                            url :url,
                            data:{id:id},
                            dataType:'json',
                            success:function(data){
                                if(data.status == true){
                                    var title = data.title;
                                    var msg = data.message;
                                    toastr.options = {
                                        positionClass : 'toast-top-left',
                                        onclick:null
                                    };

                                    var $toast = toastr['success'](msg,title);
                                    $toastlast = $toast;

                                    tr.find('td').fadeOut(1000, function () {
                                        tr.remove();
                                    });
                                }else {
                                    var title = data.title;
                                    var msg = data.message;
                                    toastr.options = {
                                        positionClass : 'toast-top-left',
                                        onclick:null
                                    };

                                    var $toast = toastr['error'](msg,title);
                                    $toastlast = $toast
                                }
                            }
                        });
                    }

                }
            );
        });

    </script>

@endsection
