@extends('admin.layout.master')
@section('title',__('Orders Status Chart'))


{{--@section('styles')--}}
{{--    <style>--}}
{{--        g rect{--}}
{{--            width: 30px;--}}
{{--        }--}}
{{--    </style>--}}
{{--@endsection--}}
@section('content')
    <div class="card" >
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{__('Orders Status Chart')}}</h5>


            <form class="form-inline"  method="get" action="">

                <div class="row" style="margin-bottom: 50px">
                    <div class="col-md-4">
                        <label class=" control-label">{{__('From')}}</label>
                        <div class="input-group">
                            <input name="from" type="date" class="form-control datepicker"  value="{{$from}}" placeholder="mm/dd/yyyy" >
                            <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <label class=" control-label">{{__('To')}}</label>
                        <div class="input-group">
                            <input name="to" type="date" class="form-control datepicker"  value="{{$to}}" placeholder="mm/dd/yyyy" >
                            <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary form-control">عرض</button>
                    </div>
                </div>

            </form>


        </div>

        <div class="card-body">


            <div id="chart"></div>
        </div>
    </div>

@endsection


@section('scripts')
    <script src="//www.gstatic.com/charts/loader.js"></script>
    <script>
        var GoogleComboChart = function() {


            //
            // Setup module components
            //

            // Combo chart
            var _googleComboChart = function() {
                if (typeof google == 'undefined') {
                    console.warn('Warning - Google Charts library is not loaded.');
                    return;
                }

                // Initialize chart
                google.charts.load('current', {
                    callback: function () {

                        // Draw chart
                        drawCombo();

                        // Resize on sidebar width change
                        $(document).on('click', '.sidebar-control', drawCombo);

                        // Resize on window resize
                        var resizeCombo;
                        $(window).on('resize', function() {
                            clearTimeout(resizeCombo);
                            resizeCombo = setTimeout(function () {
                                drawCombo();
                            }, 200);
                        });
                    },
                    packages: ['corechart']
                });

                // Chart settings
                function drawCombo() {

                    // Define charts element
                    var combo_chart_element = document.getElementById('chart');

                    // Data
                    var data = google.visualization.arrayToDataTable([
                        ['Day',     '{{__('New')}}',   '{{__('Pending')}}',  '{{__('Accepted')}}', '{{__('Completed')}}', '{{__('Not Completed')}}','{{__('Finished')}}','{{__('Refused')}}'],
                            @foreach($orders as $chart => $item)
                        [
                            '{{ $chart }}', {{ $item['new'] }}, {{ $item['pending'] }}, {{ $item['accepted'] }},
                             {{ $item['completed'] }}, {{ $item['not_completed'] }}, {{ $item['finished'] }},{{ $item['refused'] }},
                        ],
                        @endforeach
                    ]);


                    // Options
                    var options_combo = {
                        fontName: 'Roboto',
                        height: 400,
                        fontSize: 12,
                        chartArea: {
                            left: '5%',
                            width: '94%',
                            height: 350
                        },
                        seriesType: "bars",
                        // series: {
                        //     5: {
                        //         type: "line",
                        //         pointSize: 5
                        //     }
                        // },
                        tooltip: {
                            textStyle: {
                                fontName: 'Roboto',
                                fontSize: 13
                            }
                        },
                        vAxis: {
                            gridlines:{
                                color: '#e5e5e5',
                                count: 10
                            },
                            minValue: 0
                        },
                        legend: {
                            position: 'top',
                            alignment: 'center',
                            textStyle: {
                                fontSize: 16
                            }
                        }
                    };

                    // Draw chart
                    var combo = new google.visualization.ComboChart(combo_chart_element);
                    combo.draw(data, options_combo);
                }
            };


            //
            // Return objects assigned to module
            //

            return {
                init: function() {
                    _googleComboChart();
                }
            }
        }();


        // Initialize module
        // ------------------------------

       $(document).ready(function (){
          @if(count($orders) > 0)
          GoogleComboChart.init();
          @endif
       });

    </script>
@endsection

