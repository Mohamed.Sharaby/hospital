@extends('admin.layout.master')
@section('title',__('system.edit_product'))

@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">
                <a href="{{route('products.index')}}" class="btn btn-custom dropdown-toggle waves-effect waves-light" >@lang('system.back')<span class="m-l-5"><i class="fa fa-reply"></i></span></a>
            </div>
            <h4 class="page-title">@lang('system.edit_product')</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30">@lang('system.edit_product')</h4>

                <div class="row">
                    <form method="post" action="{{route('products.update',$product->id)}}" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{method_field('PUT')}}
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('system.product_name')</label>
                                <div class="col-md-10">
                                    <input type="text" required value="{{$product->name}}"
                                           autocomplete="off"
                                           data-parsley-required-message="@lang('system.field_required')"
                                           data-parsley-trigger="keyup"
                                           data-parsley-maxlength="60"
                                           data-parsley-maxlength-message="@lang('system.max_char_60')"
                                           name="name" class="form-control" placeholder="@lang('system.product_name')">

                                    @if($errors->has('name'))
                                        <p class="help-block">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>



                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('system.quantity')</label>
                                <div class="col-md-10">
                                    <input type="number" required value="{{$product->qty}}"
                                           data-parsley-required-message="@lang('system.field_required')"
                                           autocomplete="off"
                                           data-parsley-trigger="keyup"
                                           data-parsley-min="1"
                                           data-parsley-min-message="@lang('system.min_qty_1')1"
                                           name="qty" class="form-control" placeholder="@lang('system.qty')">

                                    @if($errors->has('qty'))
                                        <p class="help-block">
                                            {{ $errors->first('qty') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('system.price')</label>
                                <div class="col-md-10">
                                    <input type="number" value="{{$product->price}}"
                                           autocomplete="off"
                                           data-parsley-trigger="keyup"
                                           {{--data-parsley-min="1"--}}
                                           {{--data-parsley-min-message="أقل كمية هي 1"--}}
                                           name="price" class="form-control" placeholder="@lang('system.price')">

                                    @if($errors->has('price'))
                                        <p class="help-block">
                                            {{ $errors->first('price') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('system.barcode')</label>
                                <div class="col-md-10">
                                    <input type="string" value="{{$product->barcode}}"
                                           autocomplete="off"
                                           data-parsley-trigger="keyup"
                                           {{--data-parsley-min="1"--}}
                                           {{--data-parsley-min-message="أقل كمية هي 1"--}}
                                           name="barcode" class="form-control" placeholder="@lang('system.barcode')">

                                    @if($errors->has('barcode'))
                                        <p class="help-block">
                                            {{ $errors->first('barcode') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('system.notes_optional')</label>
                                <div class="col-md-10">
                                    <textarea name="notes"  class="form-control"
                                              data-parsley-trigger="keyup"
                                              data-parsley-maxlength="200"
                                              data-parsley-maxlength-message="@lang('system.max_char_200')"
                                              rows="5">{{$product->notes}}</textarea>
                                </div>
                            </div>
                        </div>



                        {{-- buttons --}}
                        <div class="col-lg-12">
                            <div class="form-group text-right m-t-20">
                                <button class="btn btn-primary waves-effect waves-light m-t-20" id="btnSubmit" type="submit">
                                    @lang('system.edit')
                                </button>
                            </div>
                        </div>
                    </form>


                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>
@endsection

