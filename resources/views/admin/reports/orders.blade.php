@extends('admin.layout.master')
@section('title',__('system.orders_report'))



@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">

            <div class="btn-group pull-right m-t-15">
                {{--<a href="{{route('orders.create')}}" class="btn btn-custom dropdown-toggle waves-effect waves-light">--}}
                {{--إنشاء طلب جديد--}}
                {{--<span class="m-l-5"><i class="fa fa-plus"></i></span>--}}
                {{--</a>--}}

            </div>

            <h4 class="page-title">@lang('system.orders_report')</h4>
        </div>
    </div><!--End Page-Title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <h4 class="header-title m-t-0 m-b-30">@lang('system.orders_filter')</h4>
                <div class="row">
                    <form class="form-inline" enctype="multipart/form-data" method="get" action="">

                        <div class="row" style="margin-bottom: 10px">


                        <div class="col-md-4">
                                <label class=" control-label">@lang('system.technician_choose')</label>
                                <select class="form-control" name="technical_id">
                                    <option value="">@lang('system.all')</option>
                                    @foreach($technicals as $technical)
                                        <option value="{{$technical->id}}" @if(request('technical_id') == $technical->id) selected @endif >{{$technical->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label class=" control-label">@lang('system.select_dept')</label>
                                <select class="form-control" name="dept_id">
                                    <option value="">@lang('system.all')</option>
                                    @foreach($departments as $dept)
                                        <option value="{{$dept->id}}" @if(request('dept_id') == $dept->id) selected @endif >{{$dept->value}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="col-md-4">
                                <label class=" control-label">@lang('system.order_status')</label>
                                <select class="form-control" name="status">
                                    <option value="" @if(request('status') == "") selected @endif >                             @lang('system.all')                 </option>
                                    <option value="all_not_completed" @if(request('status') == "all_not_completed") selected @endif >                             @lang('system.all_not_completed')                 </option>
                                    <option value="new" @if(request('status') == "new") selected @endif >                       @lang('system.new')                 </option>
                                    <option value="pending" @if(request('status') == "pending") selected @endif >               @lang('system.wait_technical_approval')     </option>
                                    <option value="accepted" @if(request('status') == "accepted") selected @endif >             @lang('system.technical_approval')    </option>
                                    <option value="completed" @if(request('status') == "completed") selected @endif >           @lang('system.finished_by_tech')            </option>
                                    <option value="finished" @if(request('status') == "finished") selected @endif >             @lang('system.completed')         </option>
                                    <option value="with_techs" @if(request('status') == "with_techs") selected @endif >         @lang('system.orders_with_techs')     </option>
{{--                                    <option value="not_completed" @if(request('status') == "not_completed") selected @endif >   @lang('system.not_approved_by_dept_admin')     </option>--}}
                                    <option value="refused" @if(request('status') == "refused") selected @endif >               @lang('system.refused')           </option>
                                </select>
                            </div>


                            <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-4">
                                <label class=" control-label">{{__('Exchange Order Status')}}  </label>

                                <select class="form-control" name="has_exchange">
                                    <option value="" @if(request('has_exchange') == "") selected @endif >@lang('system.all')</option>
                                    <option value="yes" @if(request('has_exchange') == "yes") selected @endif >@lang('system.has_exchange')</option>
                                    <option value="no" @if(request('has_exchange') == "no") selected @endif >@lang('system.has_no_exchange')</option>
                                </select>
                            </div>
                            </div>


                        </div>

                        <div class="row" style="margin-bottom: 50px">
                            <div class="col-md-4">
                                <label class=" control-label">{{__('From')}}</label>
                                <div class="input-group">
                                    <input name="from" type="text" class="form-control" value="{{request('from')}}" placeholder="mm/dd/yyyy" id="datepicker1">
                                    <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <label class=" control-label">{{__('To')}}</label>
                                <div class="input-group">
                                    <input name="to" type="text" class="form-control" value="{{request('to')}}" placeholder="mm/dd/yyyy" id="datepicker2">
                                    <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary form-control">@lang('system.search_filter')</button>
                            </div>
                        </div>

                    </form>
                </div>

                <table id="datatable-responsiveOrders" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="width: 20px;">@lang('system.order_NO')</th>
                        <th style="width: 160px;">@lang('system.order_date')</th>
                        <th>@lang('system.order_name')</th>
                        <th>@lang('system.technician')</th>
                        <th>الفنى الذى قام بحل المشكلة</th>
                        <th style="width: 150px;">@lang('system.followed_dept')</th>
                        <th style="width: 150px;">@lang('system.techs_refused_order')</th>
                        <th>@lang('system.status')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $i = 1; @endphp
                    @forelse($orders as $row)

                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->created_at}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->technical->name}}</td>
                            <td>{{$row->tech_name ?? 'غير معروف'}}</td>
                            <td>{{$row->department->value}}</td>
                            <td>
                                @foreach($row->refuses as $refuse)
                                    <h4 style="color: #FFF; font-weight: bold;">{{$refuse->user->name}}</h4>
                                    <li>{{$refuse->refuse_reason}}</li>
                                @endforeach
                            </td>
                            <td>
                                @switch($row->status)
                                @case('new')        <label  class="label label-info label-rounded w-md waves-effect waves-light m-b-5">@lang('system.new')</label>                        @break
                                @case('pending')    <label  class="label label-primary label-rounded w-md waves-effect waves-light m-b-5">@lang('system.wait_technical_approval')</label>   @break
                                @case('accepted')
                                <label  class="label label-warning label-rounded w-md waves-effect waves-light m-b-5">@lang('system.technical_approval')</label>
                                @break
                                @case('completed')  <label  class="label label-purple  label-rounded w-md waves-effect waves-light m-b-5">@lang('system.finished_by_tech_2') </label>               @break
                                @case('not_completed')  <label  class="label label-danger  label-rounded w-md waves-effect waves-light m-b-5">@lang('system.not_approved_by_dept_admin')</label>               @break
                                @case('finished')   <label  class="label label-success label-rounded w-md waves-effect waves-light m-b-5">@lang('system.completed')</label>              @break
                                @case('refused')    <label  class="label label-danger label-rounded w-md waves-effect waves-light m-b-5">@lang('system.refused')</label>                  @break
                                @endswitch

                                @if(isset($row->exchange))
                                    <label  class="label label-purple label-rounded w-md waves-effect waves-light m-b-5">@lang('system.exist_exchange')</label>
                                @endif
                            </td>

                            {{--<td>--}}

                            {{--<a href="{{route('products.edit',$row->id)}}" class="label label-warning">تعديل</a>--}}
                            {{--<a  id="elementRow{{$row->id}}" href="javascript:;" data-id="{{$row->id}}" data-url="{{route('products.destroy',$row->id)}}" class="removeElement label label-danger">حذف</a>--}}
                            {{--</td>--}}
                        </tr>
                        @empty

                    @endforelse
                    </tbody>
                </table>

            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script>
        // Date Picker
        jQuery('#datepicker1').datepicker();
        jQuery('#datepicker2').datepicker();

        $('#datatable-responsiveOrders').DataTable({
            "language": {
                "lengthMenu": "@lang('system.show') _MENU_ @lang('system.perpage')",
                "info": "@lang('system.show') @lang('system.perpage') _PAGE_ @lang('system.from')_PAGES_",
                "infoEmpty": "@lang('system.no_data_available')",
                "infoFiltered": "(@lang('system.filter_from_max_total') _MAX_)",
                "paginate": {
                    "first": "@lang('system.first')",
                    "last": "@lang('system.last')",
                    "next": "@lang('system.next')",
                    "previous": "@lang('system.previous')"
                },
                "search": "@lang('system.search'):",
                "zeroRecords": "@lang('system.no_search_result')",
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    text: 'Excel',
                    exportOptions: {
                        columns: [6,4, 3, 2, 1, 0]
                    },
                    className:"btn btn-success"
                },
                {
                    extend: 'print',
                    text: 'PDF',
                    exportOptions: {
                        columns: [6,4,3,2,1,0]
                    } ,
                    className:"btn btn-inverse"

                },
            ],
        });

    </script>
@endsection

