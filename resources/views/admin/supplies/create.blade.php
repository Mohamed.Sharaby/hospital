@extends('admin.layout.master')
@section('title',__('system.add_new_bill'))

@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">
                <a href="{{route('supplies.index')}}" class="btn btn-custom dropdown-toggle waves-effect waves-light" >@lang('system.back')<span class="m-l-5"><i class="fa fa-reply"></i></span></a>
            </div>
            <h4 class="page-title">@lang('system.add_new_bill')</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">


                <h4 class="header-title m-t-0 m-b-30">@lang('system.bill_data')</h4>
<br>
                <div class="row">

                    <form method="post" action="{{route('supplies.store')}}" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}


<br>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class=" control-label">@lang('system.bill_number')*</label>
                                    <input type="number" required value="{{old('bill_number')}}"
                                           data-parsley-required-message="@lang('system.field_required')"
                                           data-parsley-trigger="keyup"
                                           data-parsley-maxlength="20"
                                           data-parsley-maxlength-message="@lang('system.max_num_20')"
                                           {{--oninput="this.value = Math.abs(this.value)"--}}
                                           name="bill_number" class="form-control" placeholder="@lang('system.bill_number')">
                                    @if($errors->has('bill_number'))
                                        <p class="help-block" style="color: #FF0000;">
                                            {{ $errors->first('bill_number') }}
                                        </p>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <label class=" control-label"> @lang('system.bill_date')*</label>
                                     <input name="bill_date"  data-parsley-maxlength="20"
                                        data-parsley-maxlength-message="@lang('system.max_char_20')"
                                            required autocomplete="false"
                                            data-parsley-required-message="@lang('system.field_required')"
                                            type="date" class="form-control"
                                             >
                                    @if($errors->has('bill_date'))
                                        <p class="help-block">
                                            {{ $errors->first('bill_date') }}
                                        </p>
                                     @endif
                                </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-5">
                                <label class="control-label">  @lang('system.choose_product')*</label>
                                <select class="form-control"
                                         id="product"
                                        data-parsley-trigger="select"
                                        data-parsley-required-message="@lang('system.field_required')">
                                    <option value="" selected disabled>@lang('system.choose_product')</option>
                                    @foreach ($products  as $product)
                                        <option data-name="{{$product->name}}" data-price="{{$product->price}}" value="{{$product->id}}">{{$product->name}}</option>
                                    @endforeach
                                </select>

                                @if($errors->has('products'))
                                    <p class="help-block" style="color: #FF0000;">
                                        {{ $errors->first('products') }}
                                    </p>
                                @endif
                            </div>


                            <div class="col-md-5">
                                <label class="control-label">*@lang('system.qty')</label>
                                <input id="qty" type="number" value="{{old('qty')}}"
                                       data-parsley-required-message="@lang('system.field_required')"
                                       data-parsley-trigger="keyup"

                                       {{--oninput="this.value = Math.abs(this.value)"--}}
                                        class="form-control" placeholder="@lang('system.qty')">
                                @if($errors->has('qty'))
                                    <p class="help-block">
                                        {{ $errors->first('qty') }}
                                    </p>
                                @endif
                            </div>

                            <div class="col-md-2">
                                <button id="addProduct" class="btn btn-primary waves-effect waves-light m-t-20"  type="button">
                                    @lang('system.add')
                                </button>

                            </div>

                        </div>


                        {{-- products table --}}

        <br>
    <hr style="color:#ACACAC;background-color:#ACACAC; height: 3px; border: none;">

                        <h4 class="header-title m-t-0 m-b-30">@lang('system.products_list')</h4>
                        <div class="table-responsive">
                            <table id="productsTable" class="table m-0">
                                <thead>
                                <tr>
                                    <th>@lang('system.product_name')</th>
                                    <th>@lang('system.qty')</th>
                                    <th>@lang('system.price')</th>
                                    <th>@lang('system.delete')</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--<tr>--}}
                                    {{--<td>Table cell</td>--}}
                                    {{--<td>Table cell</td>--}}
                                    {{--<td>Table cell</td>--}}
                                    {{--<td>--}}
                                        {{--<a href="javascript:;" class="btn btn-danger waves-effect waves-light btn-xs m-b-5">حذف</a>--}}
                                    {{--</td>--}}
                                    {{--<input type="hidden" name="products[]" value="id" />--}}
                                {{--</tr>--}}

                                </tbody>
                            </table>
                        </div>









                        {{-- buttons --}}
                        <div class="col-lg-12">
                            <div class="form-group text-right m-t-20">
                                <button class="btn btn-primary waves-effect waves-light m-t-20" id="btnSubmit" type="submit">
                                    @lang('system.register')
                                </button>
                                <button onclick="window.history.back();return false;" type="reset"
                                        class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                                    @lang('system.cancel')
                                </button>
                            </div>
                        </div>
                    </form>


                </div><!-- end row -->


            </div>
        </div><!-- end col -->
    </div>
@endsection
@section('scripts')
    <script>
        // Date Picker
        jQuery('#datepicker').datepicker();
        $('#addProduct').attr('disabled')==true;
    </script>



    <script>
        $('body').delegate('#addProduct','click', function(){


            var product_name   = $('#product').find("option:selected").attr('data-name');
            var product_id = $('#product').find("option:selected").val();
            var deleteId = "removeProduct"+product_id;
            var trId = "tr"+product_id;

            var qty = $('#qty').val();
            var price = $('#product').find("option:selected").attr('data-price');

            if(product_name == null || qty <= 0 || qty == "" ){
                $('#addProduct').attr('disabled')==true;
            }else {
                $('#addProduct').attr('disabled')==false;
                $('#productsTable > tbody:last-child').append(
                    '<tr id="'+trId+'">' +
                    '<td>' + product_name + '</td>'+
                    '<td>' + qty + '</td>'+
                    '<td>' + price + '</td>'+
                    '<td>' +
                    '<a href="javascript:;" id="' +deleteId +'" data-id="'+product_id+'"  class="removeProduct btn btn-danger waves-effect waves-light btn-xs m-b-5">'+'@lang("system.delete")'+'</a>' + '</td>'+
                    '<input type="hidden" name="products[]" value="' + product_id + '" />' +
                    '<input type="hidden" name="qtys[]" value="' + qty + '" />' +
                    '</tr>');

                $('#product').prop('selectedIndex',0);
                $('#qty').val('');
            }


//
        });


        $('body').on('click', '.removeProduct', function () {
            var id = $(this).attr('data-id');
            var tr = $(this).closest($('#removeProduct' + id).parent().parent());

            tr.find('td').fadeOut(500, function () {
                tr.remove();
            });


        });
    </script>

@endsection
