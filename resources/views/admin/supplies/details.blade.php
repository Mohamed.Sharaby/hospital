@extends('admin.layout.master')
@section('title',__('system.bill_details'))


{{--@section('styles')--}}
{{--<style>--}}
{{--.dataTables_filter, .dataTables_info, .pagination { display: none; }--}}
{{--</style>--}}
{{--<style type="text/css" media="print">--}}
{{--.prevent, .no-print,.dt-buttons,.dataTables_filter,.dataTables_info,.pagination { display: none; }--}}
{{--</style>--}}
{{--@endsection--}}
@section('content')
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">

            <div class="btn-group pull-right m-t-15">
                <button onclick="window.history.back();"  class="no-print btn btn-custom dropdown-toggle waves-effect waves-light">
                    @lang('system.back')
                    <span class="m-l-5"><i class="fa fa-arrow-left"></i></span>
                </button>

            </div>

            <h4 class="page-title">@lang('system.bill_details') {{$supply->bill_number}}</h4>
        </div>
    </div><!--End Page-Title -->


    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <h3 class="header-title m-t-0 m-b-30">@lang('system.bill_data') </h3>
                {{--<button onclick="window.print();"  class="no-print btn btn-custom dropdown-toggle waves-effect waves-light">--}}
                {{--طباعة تقرير كامل--}}
                {{--<span class="m-l-5"><i class="fa fa-print"></i></span>--}}
                {{--</button>--}}
                <div class="row">
                    <div class="col-sm-12">

                        <div class="col-md-4">
                            <h4>@lang('system.bill_number')</h4>
                            <h5 style="font-weight: 600;">{{$supply->bill_number}}</h5>
                        </div>




                        <div class="col-md-4">
                            <h4>@lang('system.bill_supplier'):</h4>
                            <h5 style="font-weight: 600;">{{\App\User::find($supply->user_id)->name}}</h5>
                        </div>




                        <div class="col-md-4">
                            <h4>@lang('system.total'):</h4>
                            <h5 style="font-weight: 600;">{{$supply->getTotal()}}</h5>
                        </div>


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="header-title m-t-0 m-b-30">@lang('system.bill_data')</h4>


                                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>@lang('system.No')</th>
                                            <th>@lang('system.product_name')</th>
                                            <th>@lang('system.qty')</th>
                                            <th>@lang('system.price')</th>
                                            <th>@lang('system.barcode')</th>
                                            <th style="width: 250px;" >@lang('system.total')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i = 1; @endphp
                                        @foreach($supply->supply_details as $row)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$row->product->name}}</td>
                                                <td>{{$row->qty}}</td>
                                                <td>{{$row->price}}</td>
                                                <td>{{$row->barcode ?? __('not_found')}}</td>
                                                <td>{{$row->total}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                    </div>
                </div> <!--End of row-->

            </div>
        </div>
    </div>
@endsection
