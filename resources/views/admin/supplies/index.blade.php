@extends('admin.layout.master')
@section('title',__('system.supply_menu'))



@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">

            <div class="btn-group pull-right m-t-15">
                <a href="{{route('supplies.create')}}" class="btn btn-custom dropdown-toggle waves-effect waves-light">
                    @lang('system.register_buy_bill')
                    <span class="m-l-5"><i class="fa fa-plus"></i></span>
                </a>

            </div>

            <h4 class="page-title">@lang('system.supplies')</h4>
        </div>
    </div><!--End Page-Title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <h4 class="header-title m-t-0 m-b-30">@lang('system.all_buy_bills')</h4>


                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>@lang('system.No')</th>
                        <th>@lang('system.bill_number')</th>
                        <th>@lang('system.bill_supplier')</th>
                        <th>@lang('system.total')</th>
                        <th style="width: 250px;" >@lang('system.options')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $i = 1; @endphp
                    @foreach($supplies as $row)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$row->bill_number}}</td>
                            <td>{{$row->user->name}}</td>
                            <td>{{$row->getTotal()}}</td>
                            <td>
                                <a href="{{route('supplies.show',$row->id)}}" class="label label-primary">@lang('system.details')</a>
                                {{--<a href="{{route('supplies.edit',$row->id)}}" class="label label-warning">تعديل</a>--}}
                                <a  id="elementRow{{$row->id}}" href="javascript:;" data-id="{{$row->id}}" data-url="{{route('supplies.destroy',$row->id)}}" class="removeElement label label-danger">@lang('system.delete')</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>

        $('body').on('click', '.removeElement', function () {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var tr = $(this).closest($('#elementRow' + id).parent().parent());

            swal({
                    title: "@lang('system.r_u_sure')",
                    text: "@lang('system.really_delete_bill')",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#27dd24",
                    confirmButtonText: "@lang('system.accept')",
                    cancelButtonText: "@lang('system.cancel')",
                    confirmButtonClass:"btn-danger waves-effect waves-light",
                    closeOnConfirm: true,
                    closeOnCancel: true,
                },
                function (isConfirm) {
                    if(isConfirm){
                        $.ajax({
                            type:'delete',
                            url :url,
                            data:{id:id},
                            dataType:'json',
                            success:function(data){
                                if(data.status == true){
                                    var title = data.title;
                                    var msg = data.message;
                                    toastr.options = {
                                        positionClass : 'toast-top-left',
                                        onclick:null
                                    };

                                    var $toast = toastr['success'](msg,title);
                                    $toastlast = $toast;

                                    tr.find('td').fadeOut(1000, function () {
                                        tr.remove();
                                    });
                                }else {
                                    var title = data.title;
                                    var msg = data.message;
                                    toastr.options = {
                                        positionClass : 'toast-top-left',
                                        onclick:null
                                    };

                                    var $toast = toastr['error'](msg,title);
                                    $toastlast = $toast
                                }
                            }
                        });
                    }

                }
            );
        });

    </script>

@endsection
