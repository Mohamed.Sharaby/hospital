@extends('admin.layout.master')
@section('title',$user->name)


{{--@section('styles')--}}
    {{--<style>--}}
        {{--.dataTables_filter, .dataTables_info, .pagination { display: none; }--}}
    {{--</style>--}}
    {{--<style type="text/css" media="print">--}}
        {{--.prevent, .no-print,.dt-buttons,.dataTables_filter,.dataTables_info,.pagination { display: none; }--}}
    {{--</style>--}}
{{--@endsection--}}
@section('content')
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">

            <div class="btn-group pull-right m-t-15">
                <button onclick="window.history.back();"  class="no-print btn btn-custom dropdown-toggle waves-effect waves-light">
                    @lang('system.back')
                    <span class="m-l-5"><i class="fa fa-arrow-left"></i></span>
                </button>

            </div>

            <h4 class="page-title">@lang('system.user_details') {{$user->name}}</h4>
        </div>
    </div><!--End Page-Title -->


    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <h3 class="header-title m-t-0 m-b-30">@lang('system.user_data'){{$user->name}}</h3>
                {{--<button onclick="window.print();"  class="no-print btn btn-custom dropdown-toggle waves-effect waves-light">--}}
                    {{--طباعة تقرير كامل--}}
                    {{--<span class="m-l-5"><i class="fa fa-print"></i></span>--}}
                {{--</button>--}}
                <div class="row">
                    <div class="col-sm-12">


                        <div class="col-md-4">
                            <h4>@lang('system.name'):</h4>
                            <h5 style="font-weight: 600;">{{$user->name}}</h5>
                        </div>

                        <div class="col-md-4">
                            <h4>@lang('system.email')</h4>
                            <h5 style="font-weight: 600;">{{$user->email}}</h5>
                        </div>


                        <div class="col-md-4">
                            <h4>@lang('system.phone')</h4>
                            <h5 style="font-weight: 600;">{{$user->phone}}</h5>
                        </div>

                        <div class="col-md-4">
                            <h4>@lang('system.job_number')</h4>
                            <h5 style="font-weight: 600;">{{$user->job_number}}</h5>
                        </div>


                        <div class="col-md-4">
                            <h4>@lang('system.task_permission')</h4>
                            <h5 style="font-weight: 600;">
                                @switch($user->role)
                                @case('technical') @lang('system.technician') @break
                                @case('coordinator')@lang('system.coordinator')@break
                                @case('dept_admin') @lang('system.dept_admin') @break
                                @case('super') @lang('system.role_admin') @break
                                @endswitch
                            </h5>
                        </div>

                        @if($user->role == 'technical')
                        <div class="col-md-4">
                            <h4>@lang('system.speciality')</h4>
                            <h5 style="font-weight: 600;">{{$user->specialize->name}}</h5>
                        </div>
                        @endif

                        @if($user->role == 'dept_admin')
                            <div class="col-md-4">
                                <h4>@lang('system.his_dept')</h4>
                                <h5 style="font-weight: 600;">{{$user->department->name}}</h5>
                            </div>
                        @endif


                        <div class="col-md-4">
                            <h4>@lang('system.status')</h4>
                            @if($user->is_active ==1)
                                <h5 style="font-size: 15px;" class="label label-success">@lang('system.active')</h5>
                            @else
                                <h5 style="font-size: 15px;"  class="label label-danger">@lang('system.not_active')</h5>
                            @endif
                        </div>


                        @if($user->is_active == 0)
                            <div class="col-md-4">
                                <h4>@lang('system.suspend_reason')</h4>
                                <h5 style="font-weight: 600;">{{$user->suspend_reason}}</h5>
                            </div>
                        @endif



                        <div class="col-md-4">
                            <h4>@lang('system.profile_pic')</h4>
                            <div  style="width: 200px; height: 150px;">
                                @if($user->image)
                                    <a href="{{getimg($user->image)}}" class="image-popup" title="Screenshot-1">
                                        <img width="200" height="150" src="{{getimg($user->image)}}" class="thumb-img" alt="work-thumbnail">
                                    </a>
                                @else
                                    <a href="{{asset('admin/assets/images/noimage.png')}}" class=" image-popup" title="Screenshot-1">
                                        <img width="200" height="150" src="{{asset('admin/assets/images/noimage.png')}}" class="thumb-img" alt="work-thumbnail">
                                    </a>
                                @endif
                            </div>
                        </div>

                    </div>
                </div> <!--End of row-->

            </div>
        </div>
    </div>
@endsection
