@extends('admin.layout.master')
@section('title',__('system.profile'))

@section('styles')
    <style>
        .erro{
            color: red;
        }
    </style>
@endsection

@section('content')
    <!-- Page Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">
                {{--<a href="{{route('users.index')}}" class="btn btn-custom dropdown-toggle waves-effect waves-light" >رجوع لمستخدمي النظام<span class="m-l-5"><i class="fa fa-reply"></i></span></a>--}}
            </div>
            <h4 class="page-title">@lang('system.profile')</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30">@lang('system.user_data'){{$user->name}}</h4>

                <div class="row">

                    <form method="post" action="{{route('user.update.profile',$user->id)}}" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
{{--                        {{method_field('post')}}--}}
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('system.user_name')</label>
                                <div class="col-md-10">
                                    <input type="text" required value="{{$user->name}}"
                                           data-parsley-required-message="@lang('system.field_required')"
                                           data-parsley-trigger="keyup"
                                           data-parsley-maxlength="60"
                                           data-parsley-maxlength-message="@lang('system.max_char_60')"
                                           name="name" class="form-control" placeholder="@lang('system.username')">

                                    @if($errors->has('name'))
                                        <p class="help-block">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('system.phone')</label>
                                <div class="col-md-10">
                                    <input type="number" required value="{{$user->phone}}"
                                           data-parsley-required-message="@lang('system.field_required')"
                                           data-parsley-trigger="keyup"
                                           data-parsley-maxlength="11"
                                           data-parsley-maxlength-message="@lang('system.max_numbers_11')"
                                           {{--data-parsley-pattern="^01[0-2]{1}[0-9]{8}"--}}
                                           {{--data-parsley-pattern-message="برجاء إدخال رقم موبايل بصيغة صحيحة"--}}
                                           {{--oninput="this.value = Math.abs(this.value)"--}}
                                           name="phone" class="form-control" placeholder="@lang('system.phone')">
                                    @if($errors->has('phone'))
                                        <p class="help-block" style="color: #FF0000;">
                                            {{ $errors->first('phone') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>



                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('system.email')</label>
                                <div class="col-md-10">
                                    <input type="email" required value="{{$user->email}}"
                                           data-parsley-type="email"
                                           data-parsley-type-message = "@lang('system.enter_valid_email')"
                                           data-parsley-required-message="@lang('system.field-required')"
                                           data-parsley-trigger="keyup"
                                           data-parsley-maxlength="60"
                                           data-parsley-maxlength-message="@lang('system.max_char_60')"
                                           name="email" class="form-control" placeholder="@lang('system.email')">

                                    @if($errors->has('email'))
                                        <p class="help-block">
                                            {{ $errors->first('email') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>



                        {{--******************************************************************--}}
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('system.password')</label>
                                <div class="col-md-10">
                                    <input type="password" name="password" id="pass1" value="{{ old('password') }}"
                                           class="form-control"
                                           autocomplete="off"
                                           placeholder="@lang('system.password')"
                                           data-parsley-trigger="keyup"
                                           data-parsley-maxlength="60"
                                           data-parsley-minlength="6"
                                           data-parsley-maxlength-message=" @lang('system.max_char_60')"
                                           data-parsley-minlength-message="@lang('system.min_char_6')"
                                    />
                                    @if($errors->has('password'))
                                        <p class="help-block">
                                            {{ $errors->first('password') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('system.password_confirmation')*</label>
                                <div class="col-md-10">
                                    <input data-parsley-equalto="#pass1" name="password_confirmation" type="password" data-parsley-trigger="keyup"
                                           placeholder="@lang('system.password_confirmation') ..." class="form-control"
                                           autocomplete="off"
                                           id="passWord2"
                                           data-parsley-equalto-message="@lang('system.pass_conf_not_equal')"
                                           data-parsley-maxlength="55"
                                           data-parsley-minlength="6"
                                           data-parsley-maxlength-message=" @lang('system.max_char_55')"
                                           data-parsley-minlength-message="@lang('system.min_char_6')">

                                    @if($errors->has('password_confirmation'))
                                        <p class="help-block erro">
                                            {{ $errors->first('password_confirmation') }}
                                        </p>
                                    @endif

                                </div>
                            </div>
                        </div>

                        {{--*******************************************************************--}}

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">@lang('system.user_image')</label>
                                <div class="col-md-10">
                                    <input name="image" type="file" class="dropify" data-max-file-size="1M"
                                           data-allowed-file-extensions="png gif jpg jpeg"
                                           data-errors-position="inside"
                                           data-show-remove="false"
                                           data-default-file="{{getimg($user->image)}}"

                                    />
                                </div>
                            </div>
                        </div>


                        {{-- buttons --}}
                        <div class="col-lg-12">
                            <div class="form-group text-right m-t-20">
                                <button class="btn btn-primary waves-effect waves-light m-t-20" id="btnSubmit" type="submit">
                                    @lang('system.edit')
                                </button>
                                <button onclick="window.history.back();return false;" type="reset"
                                        class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                                    @lang('system.cancel')
                                </button>
                            </div>
                        </div>
                    </form>


                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>
@endsection
@section('scripts')
    <script>
        // Date Picker
        jQuery('#datepicker').datepicker();


        $('.dropify').dropify({
            messages: {
                'default': "@lang('system.dropify_1')",
                'replace': "@lang('system.dropify_2')",
                'remove': "@lang('system.delete')",
                'error': "@lang('system.somethingWentWrong')"
            },
            error: {
                'fileSize': "@lang('system.max_size_image')",
                'fileExtension': "@lang('system.image_type')",
            }
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        //        $("#spec").hide();
        //        $("#spec_label").hide();
        //
        //        $("#dept_select_label").hide();
        //        $("#dept_select").hide();


    </script>

    <script>
        $('#role_select').on('change', function () {

            var role_select = $('#role_select').val();


            console.log(role_select);

            if(role_select === 'technical'){
                $("#spec").show();
                $("#spec_label").show();
                $("#dept_select_label").hide();
                $("#dept_select").hide();

            }

            else if(role_select === 'dept_admin'){
                $("#dept_select_label").show();
                $("#dept_select").show();

                $("#spec").hide();
                $("#spec_label").hide();


            }

            else {
                $("#spec").hide();
                $("#spec_label").hide();

                $("#dept_select_label").hide();
                $("#dept_select").hide();
            }

        });

    </script>

@endsection
