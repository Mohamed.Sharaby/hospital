<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::redirect('/', '/dashboard');

Route::get('set-locale/{locale}', function ($lang) {
    if (array_key_exists($lang, \Config::get('language'))) {
        \Session::put('locale', $lang);
    }
    return back();
})->name('lang');



/* Login Routes ..*/
Route::group(['prefix'=>"dashboard",'namespace'=>'admin'], function () {
    route::get('/', 'LoginController@getAdminLogin')->name('admin.login');
    Route::post('/', 'LoginController@login')->name('admin.postLogin');


    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('administrator.password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('administrator.password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('administrator.password.reset.token');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('administrator.password.reset');

});




Route::group(['prefix'=>"dashboard",'namespace'=>'admin','middleware'=>'admin'], function (){

    route::get('/home','HomeController@index')->name('homePage');

    route::resource('/departments','DepartmentsController');

    route::resource('/specializations','SpecializationsController');

    route::resource('/products','ProductsController');

    route::resource('/users','UsersController');
    route::get('/user/profile','UsersController@getProfile')->name('user.get.profile');
    route::post('/user/profile/{id}','UsersController@updateProfile')->name('user.update.profile');
    route::post('users/activeOrSuspend','UsersController@activeOrSuspend')->name('users.suspendOrActivate');

    route::resource('supplies','SuppliesController');

    route::get('inventory','ProductsController@inventory')->name('inventory.index');
    route::post('inventory/editqty','ProductsController@editQuantity')->name('products.edit.qty');


// *************************      routes of orders Cycle     *******************************************
    route::resource('orders','OrdersController');
    route::get('order/myOrders','OrdersController@myOrders')->name('orders.myOrders');

    route::post('orders/add/technical','OrdersController@addTechnical')->name('orders.add.technical');
    route::post('orders/change/status','OrdersController@changeStatus')->name('orders.change.status');


    route::get('orders-chart','HomeController@chart')->name('orders.chart');
    route::post('orders-techName','OrdersController@techName')->name('orders.techName');



// *********************  ==============================  *******************************************
    route::get('reports/orders','ReportsController@Orders')->name('reports.orders');


//******************************* Exchanges Routes  ***********************************

    route::resource('exchanges','ExchangeController');
    Route::get('exchange/fast/{order_id}','ExchangeController@getFastExchange')->name('exchange.fast.create');
    route::post('exchange/refuse','ExchangeController@refuseWithReason')->name('exchange.refuse');
    route::post('exchange/accept','ExchangeController@accept')->name('exchanges.accept');
    Route::post('/product/qty','ExchangeController@getAjaxProductQty')->name('getAjaxProductQty');



    route::get('notifications','NotificationsController@index')->name('notifications.index');
    route::post('notification/delete','NotificationsController@delete')->name('notification.delete');


    Route::post('user/update/token', function (Request $request) {

        $user = \App\User::whereId($request->id)->first();

        if ($request->token) {
            $data = \App\Device::where('device', $request->token)->first();
            if ($data) {
                $data->user_id = $user->id;
                $data->save();
            } else {

                $data = new \App\Device;
                $data->device = $request->token;
                $data->user_id = $user->id;
                $data->type = 'web';
                $data->save();
            }
        }

    })->name('user.update.token');


    route::post('/logout','LoginController@logout')->name('admin.logout');

});

//Auth::routes();
Route::redirect('login','/dashboard')->name('login');

Route::get('/home', 'HomeController@index')->name('home');


